WARNING: Cannot load 'transpose-tests-config.properties' configuration file!!

ASSERTION ERROR: false; static bool rev::ConfigReader::load(const string&)
	D:\Revers\NetBeansGraphicsProjects\RevCommon\src\rev\RevConfigReader.cpp @ line 36
Initialization of CL context succeed.
  CL_DEVICE_NAME: 			Cypress
  CL_DEVICE_VENDOR: 			Advanced Micro Devices, Inc.
  CL_DRIVER_VERSION: 			CAL 1.4.1741 (VM)
  CL_DEVICE_TYPE:			CL_DEVICE_TYPE_GPU
  CL_DEVICE_MAX_COMPUTE_UNITS:		18
  CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS:	3
  CL_DEVICE_MAX_WORK_ITEM_SIZES:	256 / 256 / 256 
  CL_DEVICE_MAX_WORK_GROUP_SIZE:	256
  CL_DEVICE_MAX_CLOCK_FREQUENCY:	775 MHz
  CL_DEVICE_ADDRESS_BITS:		32
  CL_DEVICE_MAX_MEM_ALLOC_SIZE:		512 MByte
  CL_DEVICE_GLOBAL_MEM_SIZE:		1024 MByte
  CL_DEVICE_ERROR_CORRECTION_SUPPORT:	no
  CL_DEVICE_LOCAL_MEM_TYPE:		local
  CL_DEVICE_LOCAL_MEM_SIZE:		32 KByte
  CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE:	64 KByte
  CL_DEVICE_QUEUE_PROPERTIES:		CL_QUEUE_PROFILING_ENABLE
  CL_DEVICE_IMAGE_SUPPORT:		1
  CL_DEVICE_MAX_READ_IMAGE_ARGS:	128
  CL_DEVICE_MAX_WRITE_IMAGE_ARGS:	8

  CL_DEVICE_IMAGE <dim>			2D_MAX_WIDTH	 8192
					2D_MAX_HEIGHT	 8192
					3D_MAX_WIDTH	 2048
					3D_MAX_HEIGHT	 2048
					3D_MAX_DEPTH	 2048

  CL_DEVICE_EXTENSIONS:			cl_khr_fp64
					cl_amd_fp64
					cl_khr_global_int32_base_atomics
					cl_khr_global_int32_extended_atomics
					cl_khr_local_int32_base_atomics
					cl_khr_local_int32_extended_atomics
					cl_khr_3d_image_writes
					cl_khr_byte_addressable_store
					cl_khr_gl_sharing
					cl_ext_atomic_counters_32
					cl_amd_device_attribute_query
					cl_amd_vec3
					cl_amd_printf
					cl_amd_media_ops
					cl_amd_popcnt
					cl_khr_d3d10_sharing
  CL_DEVICE_PREFERRED_VECTOR_WIDTH_<t>	CHAR 16, SHORT 8, INT 4, FLOAT 2, DOUBLE 4
