/*
 * RevCLFFTInPlace.h
 *
 *  Created on: 14-09-2012
 *      Author: Revers
 */

#ifndef REVCLFFTINPLACE_TEST_H_
#define REVCLFFTINPLACE_TEST_H_

#include <string>
#include <ostream>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/api/RevICLFFTInPlace.h>
#include <rev/cla/RevICLAlgorithmFactory.hpp>
#include <rev/cla/RevCLAOption.h>
#include <rev/common/RevAssert.h>
#include <rev/cl/RevCLProgram.h>
#include <rev/cl/RevCLContext.h>
#include <rev/cl/RevCLBuffer.h>
#include <rev/cl/RevCLAssert.h>

#include "clFFT.h"
#include "clFFTCommon.h"

namespace rev {
	namespace test {

		class CLFFTInPlace2D {
			int width = 0;
			int height = 0;
			int localMemSize = 0;

			cl_ulong gMemSize;
			//clFFT_Direction dir; // = clFFT_Forward;
			//int numIter = 1;
			clFFT_Dim3 n; // = {1024, 1, 1};
			int batchSize; // = 1;
			clFFT_DataFormat dataFormat; // = clFFT_SplitComplexFormat;
			clFFT_Dimension dim; // = clFFT_1D;
			clFFT_TestType testType = clFFT_IN_PLACE;
			clFFT_Plan plan = NULL;

			CLFFTInPlace2D(const CLFFTInPlace2D& orig);
			CLFFTInPlace2D& operator=(const CLFFTInPlace2D& orig);

			int baseTypeSize = 0;
			int bufferOffset = 0;
		public:

			CLFFTInPlace2D() {
			}

			~CLFFTInPlace2D() {
				if (plan) {
					clFFT_DestroyPlan(plan);
				}
			}

			bool init(int width, int height);

			/**
			 * @Override
			 */
			virtual int getMinimalLocalMemorySize() {
				return localMemSize;
			}

			/**
			 * @Override
			 */
			void fft(rev::CLBuffer& inOutBuffer, bool inverse = false);

		private:

			bool checkMemRequirements(clFFT_Dim3 n, int batchSize,
					clFFT_TestType testType, cl_ulong gMemSize);

		};
	} /* namespace test */
} /* namespace rev */
#endif /* REVCLFFTINPLACE_TEST_H_ */
