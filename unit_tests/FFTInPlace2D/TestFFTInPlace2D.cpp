/*
 * TestFFTInPlace.cpp
 *
 *  Created on: 03-09-2012
 *      Author: Revers
 */

#include <gtest/gtest.h>
#include <memory>
#include <iostream>
#include <cstdlib>

#include <rev/common/RevErrorStream.h>
#include <rev/cl/RevCLContext.h>
#include <rev/cl/RevCLBuffer.h>

#include <rev/common/RevCommonUtil.h>
#include <rev/common/RevDataTypes.h>
#include <rev/common/RevArray.h>
#include <rev/common/RevArrayUtil.h>
#include <rev/common/RevConfigReader.h>
#include <rev/common/test/RevColorUtil.h>
#include <rev/common/RevProfiler.h>
#include <rev/common/RevStringUtil.h>
#include <rev/common/test/RevTestUtil.hpp>

#include "RevCLFFTInPlace2D.h"
#include "MyFFT2D.h"

using namespace rev;
using namespace std;

class FFTInPlace2DTest: public ::testing::Test {
protected:

	FFTInPlace2DTest() {
	}

	virtual ~FFTInPlace2DTest() {
	}

	virtual void SetUp() {
	}

	virtual void TearDown() {
	}

public:

	static void testFFT2D(int width, int height, float maxDiff) {
		rev::test::CLFFTInPlace2D fft2D;
		rev::MyFFT2D myFFT2D;
	
		typedef float base_type;
		typedef ArrayWrapper2D<base_type> ArrayType;
		typedef std::shared_ptr<ArrayType> ArrayTypePtr;

		int arrayWidth = 2 * width;
		int arrayHeight = height;

		TestUtil::printHeader();

		TestUtil::printTitle(string("FFT2D"), a2s(R(width), R(height), R(maxDiff)));

		ArrayTypePtr vec = ArrayUtil::generateArray2D<base_type > (arrayWidth, arrayHeight, 0.0, 1.0, 5);
		
		//cout << *vec << endl;
		
		cout << P(FADED_COLOR, string("vec->getSize() = ") + a2s(vec->getSize())) << endl;

		CLBuffer inBuffer1(vec->getSize() * sizeof (base_type));
		TestUtil::printSize("inBuffer1", inBuffer1.getSize());
		ASSERT_TRUE(inBuffer1.create());
		ASSERT_TRUE(inBuffer1.write(vec->data()));
		
		CLBuffer inBuffer2(vec->getSize() * sizeof (base_type));
		TestUtil::printSize("inBuffer2", inBuffer2.getSize());
		ASSERT_TRUE(inBuffer2.create());
		ASSERT_TRUE(inBuffer2.write(vec->data()));
		
		CLBuffer interBuffer(vec->getSize() * sizeof (base_type));
		TestUtil::printSize("interBuffer", interBuffer.getSize());
		ASSERT_TRUE(interBuffer.create());

		Profiler pInit;
		pInit.start();
		ASSERT_TRUE(fft2D.init(width, height));
		pInit.stop();
		cout << FADED_COLOR << "\nInit 'fft2D' took "
				<< StringUtil::format("%.6f", pInit.getTotalTimeSec())
				<< " seconds." << endl;

		pInit.reset();
		pInit.start();
		ASSERT_TRUE(myFFT2D.init(width, height));
		pInit.stop();
		cout << FADED_COLOR << "\nInit 'myFFT2D' took "
				<< StringUtil::format("%.6f", pInit.getTotalTimeSec())
				<< " seconds." << endl;

		Array2D<base_type> outArr1(arrayWidth, arrayHeight);
		Array2D<base_type> outArr2(arrayWidth, arrayHeight);

		Profiler profiler1;
        Profiler profiler2;

		profiler1.start();
        fft2D.fft(inBuffer1);
		profiler1.stop();
		
		profiler2.start();
        myFFT2D.fft(inBuffer2, interBuffer);
		profiler2.stop();

        TestUtil::printProfilingInfo("fft2D", profiler1);
        TestUtil::printProfilingInfo("myFFT2D", profiler2);

        ASSERT_TRUE(inBuffer1.read(outArr1.data()));
        ASSERT_TRUE(inBuffer2.read(outArr2.data()));

        //cout << "outArr1 = " << outArr1 << endl;
		//cout << "\n\n\n";
        //cout << "\noutArr2 = " << outArr2 << endl;

        //ASSERT_TRUE(ArrayUtil::equals(outArr1, outArr2));

        base_type totalDiff = 0;
        base_type meanDiff = 0;
        bool eq = ArrayUtil::equalsApproximately(outArr1, outArr2, maxDiff, &totalDiff, &meanDiff);

        cout << "\nmaxDiff \t= " << maxDiff << endl;
        cout << "meanDiff \t= " << EMPHASIS_COLOR << meanDiff << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ASSERT_TRUE(eq);
	}
};

TEST(FFTInPlace2DTest, test8x8) {

	FFTInPlace2DTest::testFFT2D(8, 8, 0.01);
}

TEST(FFTInPlace2DTest, test64x64) {

	FFTInPlace2DTest::testFFT2D(64, 64, 0.01);
}

TEST(FFTInPlace2DTest, test16x128) {

	FFTInPlace2DTest::testFFT2D(16, 128, 0.01);
}

TEST(FFTInPlace2DTest, test1024x512) {

	FFTInPlace2DTest::testFFT2D(1024, 512, 0.01);
}

TEST(FFTInPlace2DTest, test2048x2048) {

	FFTInPlace2DTest::testFFT2D(2048, 2048, 0.01);
}
