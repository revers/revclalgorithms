/*
 * clFFTCommon.cpp
 *
 *  Created on: 15-09-2012
 *      Author: Revers
 */

#include "clFFTCommon.h"

namespace rev {
	namespace test {

		std::ostream& operator<<(std::ostream& out, const clFFT_TestType& val) {
			switch (val) {
			case clFFT_OUT_OF_PLACE: {
				out << "clFFT_OUT_OF_PLACE";
				break;
			}
			case clFFT_IN_PLACE: {
				out << "clFFT_IN_PLACE";
				break;
			}
			default:
				break;
			}
			return out;
		}

		std::ostream& operator<<(std::ostream& out, const clFFT_Dim3& val) {
			out << "clFFT_Dim3(" << val.x << "," << val.y << "," << val.z << ")";
			return out;
		}

	} /* namespace apple */
} /* namespace rev */
