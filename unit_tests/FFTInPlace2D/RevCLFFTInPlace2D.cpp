/*
 * RevCLFFTInPlace.cpp
 *
 *  Created on: 14-09-2012
 *      Author: Revers
 */

#include <iostream>

#include "RevCLFFTInPlace2D.h"
#include "fft_internal.h"

using namespace std;

using namespace rev;
using namespace rev::test;

/**
 * @Override
 */
bool CLFFTInPlace2D::init(int width, int height) {
	this->width = width;
	this->height = height;

	baseTypeSize = sizeof(float);

	dataFormat = clFFT_InterleavedComplexFormat;
	batchSize = 1;
	dim = clFFT_2D;
	n = {width, height, 1};

	rev::CLContext& ctx = rev::CLContext::get();
	gMemSize = ctx.globalMemorySize();
	gMemSize /= (1024 * 1024);

	if (!checkMemRequirements(n, batchSize, testType, gMemSize)) {
		return false;
	}

	cl_int err;
	plan = clFFT_CreatePlan(ctx.context()(), n, dim, dataFormat, &err, 0);
	clAssert(err);

	if (plan == NULL || err != CL_SUCCESS) {
		return false;
	}
//    else {
//        clFFT_DumpPlan(plan, stdout);
//    }

	return true;
}

bool CLFFTInPlace2D::checkMemRequirements(clFFT_Dim3 n, int batchSize,
		clFFT_TestType testType, cl_ulong gMemSize) {
	cl_ulong memReq = (testType == clFFT_OUT_OF_PLACE) ? 3 : 2;
	memReq *= n.x * n.y * n.z * sizeof(clFFT_Complex) * batchSize;
	memReq = memReq / 1024 / 1024;

	if (memReq >= gMemSize) {
		assertMsg(memReq >= gMemSize,
				"ERROR: memory not sufficient: " << rev::a2s(R(memReq), R(gMemSize), R(n), R(testType), R(batchSize)));

		return false;
	}

	return true;
}

void CLFFTInPlace2D::fft(rev::CLBuffer& inOutBuffer, bool inverse) {

	assertMsg(
			(width != 0) && !(width & (width - 1)),
			"Error: Size '" << width << "' is not power of 2!!");
	assertMsg(
				(height != 0) && !(height & (height - 1)),
				"Error: Size '" << height << "' is not power of 2!!");

	((cl_fft_plan *) plan)->buffer_offset = bufferOffset;

//	if (dataFormat == clFFT_InterleavedComplexFormat) {
	cl_mem mem = inOutBuffer.getBuffer()();
	cl_int err = clFFT_ExecuteInterleaved(rev::CLContext::get().commandQueue()(),
			plan, batchSize, inverse ? clFFT_Inverse : clFFT_Forward, mem, mem, 0,
			NULL, NULL);
	clAssert(err);
//	} else { // dataFormat == clFFT_SplitComplexFormat
//		cl_mem mem = inOutBuffer.getBuffer()();
//
//		int halfSize = baseTypeSize * width;
//		cl_buffer_region reRegion = { 0, halfSize };
//		cl_buffer_region imRegion = { halfSize, halfSize };
//
//		cl_int err;
//		cl_mem data_in_real = clCreateSubBuffer(mem, CL_MEM_READ_WRITE,
//				CL_BUFFER_CREATE_TYPE_REGION, &reRegion, &err);
//		clAssert(err);
//
//		cl_mem data_in_imag = clCreateSubBuffer(mem, CL_MEM_READ_WRITE,
//				CL_BUFFER_CREATE_TYPE_REGION, &imRegion, &err);
//		clAssert(err);
//
//		cl_mem data_out_real = data_in_real;
//		cl_mem data_out_imag = data_in_imag;
//
//		err = clFFT_ExecutePlannar(rev::CLContext::get().commandQueue()(), plan,
//				batchSize, inverse ? clFFT_Inverse : clFFT_Forward, data_in_real,
//				data_in_imag, data_out_real, data_out_imag, 0, NULL, NULL);
//		clAssert(err);
//	}
}

