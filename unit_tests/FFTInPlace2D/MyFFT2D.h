/*
 * MyFFT2D.h
 *
 *  Created on: 21-09-2012
 *      Author: Kamil
 */

#ifndef MYFFT2D_H_
#define MYFFT2D_H_

#include <iostream>
#include <rev/cl/RevCLBuffer.h>

#include <rev/cla/api/RevICLFFTInPlace.h>
#include <rev/cla/api/RevCLFFTInPlaceAbstractFactory.h>
#include <rev/common/RevArray.h>
#include <rev/common/RevArrayUtil.h>
#include <rev/common/RevAssert.h>
#include <rev/cl/RevCLAssert.h>

#include "CLTransposeProgram.h"
#include "CLImageTransformProgram.h"

namespace rev {

	class MyFFT2D {
		int width = 0;
		int height = 0;
		
		rev::CLFFTInPlacePtr clFFTHorizontalPtr;
		rev::CLFFTInPlacePtr clFFTVerticalPtr;

		rev::CLTransposeProgram transposeProgram;
		rev::CLImageTransformProgram imageTransProgram;
	public:
		MyFFT2D() {}
		virtual ~MyFFT2D() {}
		
		bool init(int width, int height);

		bool reinit(int width, int height);

		void fft(rev::CLBuffer& redBuffer, rev::CLBuffer& interBuffer);

	private:
		bool createFFTAlgorithms();
		
		  template<typename base_type>
		  static void printBuffer(rev::CLBuffer& buffer, int width, int height,
		          int columnWidth = 4) {
		      alwaysAssert(width * height * sizeof(base_type) <= buffer.getSize());

		      rev::Array2D<base_type> arr(width, height);
		      alwaysAssert(buffer.read(arr.data()));

		      base_type minVal = arr(0, 0);
		      base_type maxVal = arr(0, 0);

		      for (int i = 1; i < arr.getSize(); i++) {
		          base_type& val = arr.data()[i];

		          if (val < minVal) {
		              minVal = val;
		          }
		          if (val > maxVal) {
		              maxVal = val;
		          }
		      }

		      for (int j = 0; j < arr.getHeight(); j++) {
		          for (int i = 0; i < arr.getWidth(); i++) {
		              std::string val = rev::StringUtil::toString(arr(i, j));


		              int diff = 0;

		              if(val.length() < columnWidth) {
		                  diff = columnWidth - val.length();
		              }

		              for (int k = 0; k < diff; k++) {
		                  std::cout << " ";
		              }
		              std::cout << val << ", ";
		          }
		          std::cout << std::endl;
		      }

		      std::cout << "MIN: " << minVal << std::endl;
		      std::cout << "MAX: " << maxVal << std::endl;
		  }

		
	};

} /* namespace rev */
#endif /* MYFFT2D_H_ */
