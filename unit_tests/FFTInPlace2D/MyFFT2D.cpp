/*
 * MyFFT2D.cpp
 *
 *  Created on: 21-09-2012
 *      Author: Kamil
 */

#include "MyFFT2D.h"

using namespace std;
using namespace rev;

#define COMPONENT_COUNT 2
#define COMPONENT_SIZE (sizeof(cl_float) * COMPONENT_COUNT)

bool MyFFT2D::init(int width, int height) {
	this->width = width;
	this->height = height;
	
	if (!transposeProgram.init()) {
		std::cout << "transposeProgram.init() FAILED!!" << std::endl;
		assert(false);
		return false;
	}

	if (!imageTransProgram.init()) {
		std::cout <<  "imageTransProgram.init() FAILED!!" << std::endl;
		assert(false);
		return false;
	}
	
	if(!createFFTAlgorithms()) {
		std::cout <<  "createFFTAlgorithms() FAILED!!" << std::endl;
	
		assert(false);
		return false;
	}
	
	return true;
}

bool MyFFT2D::reinit(int width, int height) {
	this->width = width;
	this->height = height;

	if(!createFFTAlgorithms()) {
		std::cout <<  "createFFTAlgorithms() FAILED!!" << std::endl;
	
		assert(false);
		return false;
	}
	
	return true;
}

void MyFFT2D::fft(rev::CLBuffer& redBuffer, rev::CLBuffer& interBuffer) {
	int w = width;
	int h = height;

	int origin = 0;
	int size = w * COMPONENT_SIZE;
	

	cl_int err;
	for (int i = 0; i < h; i++) {

		clFFTHorizontalPtr->setBufferOffset(origin);
		clFFTHorizontalPtr->fft(redBuffer);
		origin += size;
	}
	
//	printBuffer<float>(redBuffer, w * 2, h, 10);
    // Debugger break:
    
	float scaleFactor = 1.0f;/// w;
	transposeProgram.transposeMulNorm(redBuffer, interBuffer, w, h, scaleFactor);
	redBuffer.swap(interBuffer);
	
	//cout << "\n\n" << endl;
	//printBuffer<float>(redBuffer, w * 2, h, 10);
    // Debugger break:
	//exit(0);

	w = height;
	h = width;

	origin = 0;
	size = w * COMPONENT_SIZE;

	for (int i = 0; i < h; i++) {
		clFFTVerticalPtr->setBufferOffset(origin);
		clFFTVerticalPtr->fft(redBuffer);
		origin += size;
	}

	scaleFactor = 1.0f;// / w;
	transposeProgram.transposeMulNorm(redBuffer, interBuffer, w, h,
			scaleFactor);
	redBuffer.swap(interBuffer);
}

bool MyFFT2D::createFFTAlgorithms() {
	alwaysAssertMsg(width <= 2048 && height <= 2048,
			"This FFT algorithm handles only data below or equal 2048 units. " << rev::a2s(R(width), R(height)));

	CLFFTInPlaceAbstractFactory& abstractFactory =
			CLFFTInPlaceAbstractFactory::getInstance();

	clFFTHorizontalPtr = abstractFactory.create(
			ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_ARR_INTERLEAVED);

	if (!clFFTHorizontalPtr) {
		assert(false);
		return false;
	}

	if (clFFTHorizontalPtr->init(width) == false) {
		assert(false);
		return false;
	}

	clFFTVerticalPtr = abstractFactory.create(
			ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_ARR_INTERLEAVED);

	if (!clFFTVerticalPtr) {
		assert(false);
		return false;
	}

	if (clFFTVerticalPtr->init(height) == false) {
		assert(false);
		return false;
	}

	return true;
}