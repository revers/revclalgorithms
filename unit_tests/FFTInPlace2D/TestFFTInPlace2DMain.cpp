#include <string>

#include <gtest/gtest.h>

#include <rev/cl/RevCLContext.h>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevConfigReader.h>

using namespace rev;
using namespace std;

#define CONFIG_FILE "fft-in-place-2D-config.properties"

int main(int argc, char **argv) {
    if (argc > 1) {
        if (!ConfigReader::load(argv[1])) {
            ConfigReader::load(CONFIG_FILE);
        }
    } else {
		ConfigReader::load(CONFIG_FILE);
	}

    ::testing::InitGoogleTest(&argc, argv);

    string clDeviceType = ConfigReader::getInstance().getString("cl.device.type", "CL_DEVICE_TYPE_GPU");

    cl_device_type deviceType = CL_DEVICE_TYPE_GPU;

    if (clDeviceType == "CL_DEVICE_TYPE_CPU") {
        cout << "Using CL_DEVICE_TYPE_CPU" << endl;
        deviceType = CL_DEVICE_TYPE_CPU;
    } else {
        cout << "Using CL_DEVICE_TYPE_GPU" << endl;
    }

    rev::CLContextInitializer clInitializer(deviceType, false);

    if (!clInitializer.initialize()) {
        REV_ERROR_MSG("Initialization of CL context FAILED!");
        return -1;
    } else {
        REV_ERROR_MSG("Initialization of CL context succeed.\n"
                << CLContext::get().deviceInfo());
    }

    return RUN_ALL_TESTS();
}
