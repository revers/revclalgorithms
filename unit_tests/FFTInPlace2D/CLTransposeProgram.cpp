/*
 * CLTransposeProgram.cpp
 *
 *  Created on: 20-09-2012
 *      Author: Revers
 */
#include <string>
#include <iostream>
#include "CLTransposeProgram.h"

//#include <log4cplus/logger.h>
//#include <log4cplus/loggingmacros.h>
//#include "../LoggingDefines.h"

#include <rev/common/RevAssert.h>
#include <rev/cl/RevCLAssert.h>

//using namespace log4cplus;

using namespace std;
using namespace rev;

#define REVCLA_USE_FAST_MATH
#define KERNEL_FILENAME "Transpose.cl"

//Logger CLTransposeProgram::logger = Logger::getInstance(
  //      "effects.rev.CLTransposeProgram");

//__kernel void transposeMulNorm(__global float2* odata, __global float2* idata,
//        uint width, uint height, float scaleFactor) {
namespace KernelMulNorm {
    enum {
        ARG_OUT_DATA, ARG_IN_DATA, ARG_WIDTH, ARG_HEIGHT, ARG_SCALE_FACTOR
    };
}

bool CLTransposeProgram::init() {
#ifdef REVCLA_USE_FAST_MATH
    string programFlags = "-cl-fast-relaxed-math";
#else
    string programFlags = "";
#endif
    program.setFlags(programFlags);

    cout << "Compiling " KERNEL_FILENAME << endl;

    if (!program.compileFromFile(KERNEL_FILENAME)) {
      cout << "Failed to compile " KERNEL_FILENAME << endl;
        return false;
    }

    cl_int err;
    mulNormKernel = cl::Kernel(program(), "transposeMulNorm", &err);
    clAssert(err);

    if (err != CL_SUCCESS) {
        return false;
    }

    return true;
}

void CLTransposeProgram::transposeMulNorm(rev::CLBuffer& inBuffer,
        rev::CLBuffer& outBuffer, int width, int height, float scaleFactor) {
    cl_int err;
    err = mulNormKernel.setArg(KernelMulNorm::ARG_IN_DATA, inBuffer.getBuffer());
    clAssert(err);

    err = mulNormKernel.setArg(KernelMulNorm::ARG_OUT_DATA, outBuffer.getBuffer());
    clAssert(err);

    err = mulNormKernel.setArg(KernelMulNorm::ARG_WIDTH, width);
    clAssert(err);

    err = mulNormKernel.setArg(KernelMulNorm::ARG_HEIGHT, height);
    clAssert(err);

    err = mulNormKernel.setArg(KernelMulNorm::ARG_SCALE_FACTOR, scaleFactor);
    clAssert(err);

    uint w = roundUp(LOCAL_SIZE_X, width);
    uint h = roundUp(LOCAL_SIZE_Y, height);

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(mulNormKernel, cl::NullRange,
            cl::NDRange(w, h), cl::NDRange(LOCAL_SIZE_X, LOCAL_SIZE_Y), NULL, NULL);
    clAssert(err);
}
