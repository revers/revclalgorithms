/*
 * CLImageTransformProgram.cpp
 *
 *  Created on: 20-09-2012
 *      Author: Revers
 */

#include <string>
#include <iostream>
#include "CLImageTransformProgram.h"

//#include <log4cplus/logger.h>
//#include <log4cplus/loggingmacros.h>
//#include "../LoggingDefines.h"

#include <rev/common/RevAssert.h>
#include <rev/cl/RevCLAssert.h>

//using namespace log4cplus;

using namespace std;
using namespace rev;

#define REVCLA_USE_FAST_MATH
#define KERNEL_FILENAME "ImageTransform.cl"

//Logger CLImageTransformProgram::logger = Logger::getInstance(
//        "effects.rev.CLImageTransformProgram");

namespace KernelRedToBuffer {
    enum {
        ARG_IMAGE, ARG_BUFFER, ARG_IMG_WIDTH, ARG_IMG_HEIGHT, ARG_BUFFER_WIDTH
    };
}

namespace KernelModBufferToImage {
    enum {
        ARG_IMAGE, ARG_BUFFER, ARG_IMG_WIDTH, ARG_IMG_HEIGHT, ARG_BUFFER_WIDTH
    };
}

bool CLImageTransformProgram::init() {
#ifdef REVCLA_USE_FAST_MATH
    string programFlags = "-cl-fast-relaxed-math";
#else
    string programFlags = "";
#endif
    program.setFlags(programFlags);

   cout << "Compiling " KERNEL_FILENAME << endl;

    if (!program.compileFromFile(KERNEL_FILENAME)) {
       cout << "Failed to compile " KERNEL_FILENAME << endl;
        return false;
    }

    cl_int err;
    redToBufferKernel = cl::Kernel(program(), "copyImageRedToBuffer", &err);
    clAssert(err);

    if (err != CL_SUCCESS) {
        return false;
    }

    modBufferToImgKernel = cl::Kernel(program(), "copyModBufferToImage", &err);
    clAssert(err);

    if (err != CL_SUCCESS) {
        return false;
    }

    return true;
}

void CLImageTransformProgram::copyImageRedToBuffer(rev::CLTexture2D& img,
        rev::CLBuffer& buffer, int interBuffWidth, int interBuffHeight) {
    cl_int err;
    err = redToBufferKernel.setArg(KernelRedToBuffer::ARG_IMAGE, img.getBuffer());
    clAssert(err);

    err = redToBufferKernel.setArg(KernelRedToBuffer::ARG_IMG_WIDTH,
            img.getWidth());
    clAssert(err);

    err = redToBufferKernel.setArg(KernelRedToBuffer::ARG_IMG_HEIGHT,
            img.getHeight());
    clAssert(err);

    err = redToBufferKernel.setArg(KernelRedToBuffer::ARG_BUFFER,
            buffer.getBuffer());
    clAssert(err);

    err = redToBufferKernel.setArg(KernelRedToBuffer::ARG_BUFFER_WIDTH,
            interBuffWidth * 2);
    clAssert(err);

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(redToBufferKernel, cl::NullRange,
            cl::NDRange(interBuffWidth, interBuffHeight),
            cl::NDRange(LOCAL_SIZE_X, LOCAL_SIZE_Y), NULL, NULL);
    clAssert(err);
}

void CLImageTransformProgram::modBufferToImage(rev::CLTexture2D& img,
        rev::CLBuffer& buffer, int interBuffWidth, int interBuffHeight) {
    cl_int err;
    err = modBufferToImgKernel.setArg(KernelRedToBuffer::ARG_IMAGE,
            img.getBuffer());
    clAssert(err);

    err = modBufferToImgKernel.setArg(KernelRedToBuffer::ARG_IMG_WIDTH,
            img.getWidth());
    clAssert(err);

    err = modBufferToImgKernel.setArg(KernelRedToBuffer::ARG_IMG_HEIGHT,
            img.getHeight());
    clAssert(err);

    err = modBufferToImgKernel.setArg(KernelRedToBuffer::ARG_BUFFER,
            buffer.getBuffer());
    clAssert(err);

    err = modBufferToImgKernel.setArg(KernelRedToBuffer::ARG_BUFFER_WIDTH,
            interBuffWidth * 2);
    clAssert(err);

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(modBufferToImgKernel, cl::NullRange,
            cl::NDRange(interBuffWidth, interBuffHeight),
            cl::NDRange(LOCAL_SIZE_X, LOCAL_SIZE_Y), NULL, NULL);
    clAssert(err);
}
