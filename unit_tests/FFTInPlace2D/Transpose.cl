/*
 * Transpose.cl
 *
 *  Created on: 20-09-2012
 *      Author: Revers
 */

#ifdef FAKE_INCLUDE
#include <FAKE_INCLUDE/opencl_fake.hxx>
#endif

#define DIVIDE(a, b) native_divide(a, b)
#define SQRT(x) native_sqrt(x)
#define DOT(a, b) dot(a, b)

__constant sampler_t IMAGE_SAMPLER = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_REPEAT
        | CLK_FILTER_LINEAR;

__constant sampler_t IMAGE_SAMPLER2 = CLK_NORMALIZED_COORDS_FALSE
        | CLK_ADDRESS_CLAMP | CLK_FILTER_NEAREST;

__kernel void transpose(__global float2* odata, __global float2* idata, uint width,
        uint height) {
    uint xIndex = get_global_id(0);
    uint yIndex = get_global_id(1);

    if (xIndex < width && yIndex < height) {
        uint index_in = xIndex + width * yIndex;
        uint index_out = yIndex + height * xIndex;
        odata[index_out] = idata[index_in];
    }
}

__kernel void transposeMul(__global float2* odata, __global float2* idata,
        uint width, uint height, float scaleFactor) {
    uint xIndex = get_global_id(0);
    uint yIndex = get_global_id(1);

    if (xIndex < width && yIndex < height) {
        uint index_in = xIndex + width * yIndex;
        uint index_out = yIndex + height * xIndex;
        odata[index_out] = idata[index_in] * scaleFactor;
    }
}

__kernel void transposeMulNorm(__global float2* odata, __global float2* idata,
        uint width, uint height, float scaleFactor) {
    uint xIndex = get_global_id(0);
    uint yIndex = get_global_id(1);

    if (xIndex < width && yIndex < height) {
        uint index_in = xIndex + width * yIndex;
        uint index_out = yIndex + height * xIndex;
        odata[index_out] = idata[index_in] * scaleFactor;// * 0.5f + 0.5f;
    }
}
