/*
 * TestFFTInPlace.cpp
 *
 *  Created on: 03-09-2012
 *      Author: Revers
 */

#include <gtest/gtest.h>
#include <memory>
#include <iostream>
#include <cstdlib>

//#define REVCOMMON_NO_COLORIZED_OUT

#include <rev/cl/RevCLContext.h>
#include <rev/cl/RevCLBuffer.h>

#include <rev/cla/api/RevCLFFTInPlaceAbstractFactory.h>
#include <rev/cla/api/RevICLFFTInPlace.h>
#include <rev/cla/api/RevCLFFT2DInPlaceAbstractFactory.h>
#include <rev/cla/api/RevICLFFT2DInPlace.h>

#include <rev/common/RevErrorStream.h>
#include <rev/common/RevCommonUtil.h>
#include <rev/common/RevDataTypes.h>
#include <rev/common/RevArray.h>
#include <rev/common/RevArrayUtil.h>
#include <rev/common/RevConfigReader.h>
#include <rev/common/test/RevColorUtil.h>
#include <rev/common/RevProfiler.h>
#include <rev/common/RevStringUtil.h>

#include <rev/common/test/RevTestUtil.hpp>

using namespace rev;
using namespace std;

#define DEFAULT_BG_COLOR rev::bg_white
#define DEFAULT_FG_COLOR rev::fg_black
#define TITLE_COLOR rev::fg_blue
#define EMPHASIS_COLOR rev::fg_red
#define TIME_COLOR rev::fg_lo_green
#define FADED_COLOR rev::fg_gray
#define WIDTH_COLOR rev::fg_lo_red

class FFTInPlaceTest: public ::testing::Test {
protected:

	FFTInPlaceTest() {
	}

	virtual ~FFTInPlaceTest() {
	}

	virtual void SetUp() {
	}

	virtual void TearDown() {
	}

public:

	static void printSize(const char* name, int size) {
		cout << FADED_COLOR;

		cout << "size of " << name << " \t= " << CommonUtil::getSizeMB(size)
		<< endl;

		cout << DEFAULT_FG_COLOR;
	}

	static void printProfilingInfo(const char* spiName, const Profiler& p) {
		cout << TIME_COLOR;

		cout << "Time for SPI: " << spiName << ":\t" << p.getTotalTimeMicro() <<" micro seconds." << endl;

        cout << DEFAULT_FG_COLOR;
    }

    template<typename base_type>
    static void testInverse(const char* spi, int size, CLAOption options,
            base_type maxDiff, bool scale = false) {

        typedef ArrayWrapper1D<base_type> ArrayType;
        typedef std::shared_ptr<ArrayType> ArrayTypePtr;

        cout << DEFAULT_BG_COLOR;
        const ::testing::TestInfo * const test_info =
                ::testing::UnitTest::GetInstance()->current_test_info();
        cout << "BEGINNING TEST: -------------------------------------- "
                << EMPHASIS_COLOR << test_info->test_case_name() << "." << test_info->name()
                << DEFAULT_FG_COLOR << " ---------------------------------------" << endl;


        cout << TITLE_COLOR << "\n\nTESTING INVERSE FFT SPI '"
                << EMPHASIS_COLOR << spi << TITLE_COLOR
                << "' with parameters\n"
                << "WIDTH: " << WIDTH_COLOR << size << TITLE_COLOR
                << "; Options: " << options << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ArrayTypePtr vecPtr = ArrayUtil::generateArray1D<base_type > (size * 2, -500, 500, 5);
        //cout << "in = " << *vecPtr << endl;

        CLBuffer inBuffer(vecPtr->getSize() * sizeof (base_type));
        printSize("inBuffer", inBuffer.getSize());

        CLBuffer outBuffer(vecPtr->getSize() * sizeof (base_type));
        printSize("outBuffer", outBuffer.getSize());

        ASSERT_TRUE(inBuffer.create());
        ASSERT_TRUE(outBuffer.create());

        ASSERT_TRUE(inBuffer.write(vecPtr->data()));

        CLFFTInPlaceAbstractFactory& abstractFactory = CLFFTInPlaceAbstractFactory::getInstance();
        CLFFTInPlaceFactoryPtr factoryPtr = abstractFactory.getFactoryFor(spi);
        ASSERT_TRUE((bool) factoryPtr) << "ERROR: Cannot find factory for spi: '"
                << spi << "'!!\n";

        if (factoryPtr->areOptionsSupported(options) == false) {
            cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                    << spi << "'! (Options: " << options << ")\n\n";

            ASSERT_TRUE(false);
            return;
        }

        CLFFTInPlacePtr clFFTPtr = factoryPtr->create(options);
        ASSERT_TRUE((bool) clFFTPtr);

        Profiler pInit;
        pInit.start();
        ASSERT_TRUE(clFFTPtr->init(size));
        pInit.stop();
        cout << FADED_COLOR << "\nInit took " << StringUtil::format("%.6f", pInit.getTotalTimeSec()) << " seconds.\n" << endl;
        cout << DEFAULT_FG_COLOR;

        Profiler profiler;

        clFFTPtr->fft(inBuffer, profiler);

        printProfilingInfo(spi, profiler);

        profiler.reset();
        clFFTPtr->fft(inBuffer, true, profiler);
        printProfilingInfo(spi, profiler);

        Array1D<base_type> outArr(size * 2);
        ASSERT_TRUE(inBuffer.read(outArr.data()));

        //        cout << "\noutArr2 = " << outArr2 << endl;


        if (scale) {
            base_type factor = base_type(1) / size;
            for (auto& val : outArr) {
                val *= factor;
            }
        }

        base_type totalDiff;
        base_type meanDiff;

        bool eq = ArrayUtil::equalsApproximately(*vecPtr, outArr, maxDiff, &totalDiff, &meanDiff);
        cout << "\nmaxDiff \t= " << maxDiff << endl;
        cout << "meanDiff \t= " << EMPHASIS_COLOR << meanDiff << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ASSERT_TRUE(eq);
    }
	
	template<typename base_type>
    static void testCompare2D(const char* spi1, const char* spi2, int width, int height, CLAOption options,
            base_type maxDiff) {
        typedef ArrayWrapper1D<base_type> ArrayType;
        typedef std::shared_ptr<ArrayType> ArrayTypePtr;

        int arraySize = 2 * width * height;

        TestUtil::printHeader();

	    TestUtil::printTitle(string("Compare 2D SPI '") + spi1 + "' vs '" + spi2 + "'",
			   a2s(R(width), R(height), R(maxDiff), R(options)));


        ArrayTypePtr vecPtr = ArrayUtil::generateArray1D<base_type > (arraySize, -500, 500, 5);
        //     cout << "in = \n" << *vecPtr << endl;

        CLBuffer inBuffer1(vecPtr->getSize() * sizeof (base_type));
        printSize("inBuffer1", inBuffer1.getSize());
        ASSERT_TRUE(inBuffer1.create());


        CLBuffer inBuffer2(vecPtr->getSize() * sizeof (base_type));
        printSize("inBuffer2", inBuffer1.getSize());
        ASSERT_TRUE(inBuffer2.create());

        ASSERT_TRUE(inBuffer1.write(vecPtr->data()));
        ASSERT_TRUE(inBuffer2.write(vecPtr->data()));

        CLFFT2DInPlaceAbstractFactory& abstractFactory1 = CLFFT2DInPlaceAbstractFactory::getInstance();

        CLFFT2DInPlaceFactoryPtr factoryPtr1 = abstractFactory1.getFactoryFor(spi1);
        ASSERT_TRUE((bool) factoryPtr1) << "ERROR: Cannot find factory for spi: '"
                << spi1 << "'!!\n";

        CLFFT2DInPlaceFactoryPtr factoryPtr2 = abstractFactory1.getFactoryFor(spi2);
        ASSERT_TRUE((bool) factoryPtr2) << "ERROR: Cannot find factory for spi: '"
                << spi2 << "'!!\n";

        if (factoryPtr1->areOptionsSupported(options) == false) {
            cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                    << spi1 << "'! (Options: " << options << ")\n\n";

            ASSERT_TRUE(false);
            return;
        }

        if (factoryPtr2->areOptionsSupported(options) == false) {
            cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                    << spi2 << "'! (Options: " << options << ")\n\n";

            ASSERT_TRUE(false);
            return;
        }

        CLFFT2DInPlacePtr clFFTPtr1 = factoryPtr1->create(options);
        ASSERT_TRUE((bool) clFFTPtr1);

        CLFFT2DInPlacePtr clFFTPtr2 = factoryPtr2->create(options);
        ASSERT_TRUE((bool) clFFTPtr2);

        Profiler pInit;
        pInit.start();
        ASSERT_TRUE(clFFTPtr1->init(width, height));
        pInit.stop();
        cout << FADED_COLOR << "\nInit '" << spi1 << "' took "
                << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                << " seconds." << endl;

        pInit.reset();
        pInit.start();
        ASSERT_TRUE(clFFTPtr2->init(width, height));
        pInit.stop();
        cout << FADED_COLOR << "Init '" << spi2 << "' took "
                << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                << " seconds.\n" << endl;
        cout << DEFAULT_FG_COLOR;

        Profiler profiler1;
        Profiler profiler2;

        clFFTPtr1->fft(inBuffer1, profiler1);
        cout << "FFT '" << spi1 << "' done." << endl;
        clFFTPtr2->fft(inBuffer2, profiler2);
        cout << "FFT '" << spi2 << "' done." << endl;

        printProfilingInfo(spi1, profiler1);
        printProfilingInfo(spi2, profiler2);

        Array1D<base_type> outArr1(arraySize);
        Array1D<base_type> outArr2(arraySize);

        ASSERT_TRUE(inBuffer1.read(outArr1.data()));
        ASSERT_TRUE(inBuffer2.read(outArr2.data()));

        //  cout << "outArr1 = " << outArr1 << endl;
        //  cout << "\noutArr2 = " << outArr2 << endl;

        //ASSERT_TRUE(ArrayUtil::equals(outArr1, outArr2));

        base_type totalDiff;
        base_type meanDiff;
        bool eq = ArrayUtil::equalsApproximately(outArr1, outArr2, maxDiff, &totalDiff, &meanDiff);

        cout << "\nmaxDiff \t= " << maxDiff << endl;
        cout << "meanDiff \t= " << EMPHASIS_COLOR << meanDiff << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ASSERT_TRUE(eq);
    }

    template<typename base_type>
    static void testCompare(const char* spi1, const char* spi2, int size, CLAOption options,
            base_type maxDiff) {
        typedef ArrayWrapper1D<base_type> ArrayType;
        typedef std::shared_ptr<ArrayType> ArrayTypePtr;

        int arraySize = 2 * size;

        cout << DEFAULT_BG_COLOR;
        const ::testing::TestInfo * const test_info =
                ::testing::UnitTest::GetInstance()->current_test_info();
        cout << "BEGINNING TEST: -------------------------------------- "
                << EMPHASIS_COLOR << test_info->test_case_name() << "." << test_info->name()
                << DEFAULT_FG_COLOR << " ---------------------------------------" << endl;


        cout << TITLE_COLOR << "\n\nCOMPARING SPI '" << EMPHASIS_COLOR << spi1 << TITLE_COLOR << "' WITH '"
                << EMPHASIS_COLOR << spi2 << TITLE_COLOR << "' with parameters\n"
                << "WIDTH: " << WIDTH_COLOR << size << TITLE_COLOR
                << "; Options: " << options << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ArrayTypePtr vecPtr = ArrayUtil::generateArray1D<base_type > (arraySize, -500, 500, 5);
        //     cout << "in = \n" << *vecPtr << endl;

        CLBuffer inBuffer1(vecPtr->getSize() * sizeof (base_type));
        printSize("inBuffer1", inBuffer1.getSize());
        ASSERT_TRUE(inBuffer1.create());


        CLBuffer inBuffer2(vecPtr->getSize() * sizeof (base_type));
        printSize("inBuffer2", inBuffer1.getSize());
        ASSERT_TRUE(inBuffer2.create());

        ASSERT_TRUE(inBuffer1.write(vecPtr->data()));
        ASSERT_TRUE(inBuffer2.write(vecPtr->data()));

        CLFFTInPlaceAbstractFactory& abstractFactory1 = CLFFTInPlaceAbstractFactory::getInstance();

        CLFFTInPlaceFactoryPtr factoryPtr1 = abstractFactory1.getFactoryFor(spi1);
        ASSERT_TRUE((bool) factoryPtr1) << "ERROR: Cannot find factory for spi: '"
                << spi1 << "'!!\n";

        CLFFTInPlaceFactoryPtr factoryPtr2 = abstractFactory1.getFactoryFor(spi2);
        ASSERT_TRUE((bool) factoryPtr2) << "ERROR: Cannot find factory for spi: '"
                << spi2 << "'!!\n";

        if (factoryPtr1->areOptionsSupported(options) == false) {
            cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                    << spi1 << "'! (Options: " << options << ")\n\n";

            ASSERT_TRUE(false);
            return;
        }

        if (factoryPtr2->areOptionsSupported(options) == false) {
            cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                    << spi2 << "'! (Options: " << options << ")\n\n";

            ASSERT_TRUE(false);
            return;
        }

        CLFFTInPlacePtr clFFTPtr1 = factoryPtr1->create(options);
        ASSERT_TRUE((bool) clFFTPtr1);

        CLFFTInPlacePtr clFFTPtr2 = factoryPtr2->create(options);
        ASSERT_TRUE((bool) clFFTPtr2);

        Profiler pInit;
        pInit.start();
        ASSERT_TRUE(clFFTPtr1->init(size));
        pInit.stop();
        cout << FADED_COLOR << "\nInit '" << spi1 << "' took "
                << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                << " seconds." << endl;

        pInit.reset();
        pInit.start();
        ASSERT_TRUE(clFFTPtr2->init(size));
        pInit.stop();
        cout << FADED_COLOR << "Init '" << spi2 << "' took "
                << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                << " seconds.\n" << endl;
        cout << DEFAULT_FG_COLOR;

        Profiler profiler1;
        Profiler profiler2;

        clFFTPtr1->fft(inBuffer1, profiler1);
        clFFTPtr2->fft(inBuffer2, profiler2);

        printProfilingInfo(spi1, profiler1);
        printProfilingInfo(spi2, profiler2);

        Array1D<base_type> outArr1(arraySize);
        Array1D<base_type> outArr2(arraySize);

        ASSERT_TRUE(inBuffer1.read(outArr1.data()));
        ASSERT_TRUE(inBuffer2.read(outArr2.data()));

        //  cout << "outArr1 = " << outArr1 << endl;
        //  cout << "\noutArr2 = " << outArr2 << endl;

        //ASSERT_TRUE(ArrayUtil::equals(outArr1, outArr2));

        base_type totalDiff;
        base_type meanDiff;
        bool eq = ArrayUtil::equalsApproximately(outArr1, outArr2, maxDiff, &totalDiff, &meanDiff);

        cout << "\nmaxDiff \t= " << maxDiff << endl;
        cout << "meanDiff \t= " << EMPHASIS_COLOR << meanDiff << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ASSERT_TRUE(eq);
    }

    template<typename base_type>
        static void testIterations(const char* spi, int size, CLAOption options, int iterations) {

            typedef ArrayWrapper1D<base_type> ArrayType;
            typedef std::shared_ptr<ArrayType> ArrayTypePtr;

           TestUtil::printHeader();

           TestUtil::printTitle(string("ITERATIONS for SPI ") + spi, a2s(R(size), R(options), R(iterations)));

            ArrayTypePtr vecPtr = ArrayUtil::generateArray1D<base_type > (size * 2, -500, 500, 5);

            CLBuffer inBuffer(vecPtr->getSize() * sizeof (base_type));
            printSize("inBuffer", inBuffer.getSize());

            ASSERT_TRUE(inBuffer.create());

            ASSERT_TRUE(inBuffer.write(vecPtr->data()));

            CLFFTInPlaceAbstractFactory& abstractFactory = CLFFTInPlaceAbstractFactory::getInstance();
            CLFFTInPlaceFactoryPtr factoryPtr = abstractFactory.getFactoryFor(spi);
            ASSERT_TRUE((bool) factoryPtr) << "ERROR: Cannot find factory for spi: '"
                    << spi << "'!!\n";

            if (factoryPtr->areOptionsSupported(options) == false) {
                cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                        << spi << "'! (Options: " << options << ")\n\n";

                ASSERT_TRUE(false);
                return;
            }

            CLFFTInPlacePtr clFFTPtr = factoryPtr->create(options);
            ASSERT_TRUE((bool) clFFTPtr);

            Profiler pInit;
            pInit.start();
            ASSERT_TRUE(clFFTPtr->init(size));
            pInit.stop();
            cout << FADED_COLOR << "\nInit took " << StringUtil::format("%.6f", pInit.getTotalTimeSec()) << " seconds.\n" << endl;
            cout << DEFAULT_FG_COLOR;

            Profiler profiler;

            for(int i = 0; i < iterations; i++) {
            	clFFTPtr->fft(inBuffer, profiler);
            }

            printProfilingInfo(spi, profiler);
        }

    template<typename base_type>
            static void testIterationsSubBuf(const char* spi, int size, CLAOption options, int iterations) {

                typedef ArrayWrapper1D<base_type> ArrayType;
                typedef std::shared_ptr<ArrayType> ArrayTypePtr;

               TestUtil::printHeader();

               TestUtil::printTitle(string("ITERATIONS for SPI ") + spi, a2s(R(size), R(options), R(iterations)));

                ArrayTypePtr vecPtr = ArrayUtil::generateArray1D<base_type > (size * 2 * iterations, -500, 500, 5);

                cout << "vecPtr size = " << vecPtr->getSize() << endl;

                CLBuffer inBuffer(vecPtr->getSize() * sizeof (base_type));
                printSize("inBuffer", inBuffer.getSize());

                ASSERT_TRUE(inBuffer.create());

                ASSERT_TRUE(inBuffer.write(vecPtr->data()));

                CLFFTInPlaceAbstractFactory& abstractFactory = CLFFTInPlaceAbstractFactory::getInstance();
                CLFFTInPlaceFactoryPtr factoryPtr = abstractFactory.getFactoryFor(spi);
                ASSERT_TRUE((bool) factoryPtr) << "ERROR: Cannot find factory for spi: '"
                        << spi << "'!!\n";

                if (factoryPtr->areOptionsSupported(options) == false) {
                    cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                            << spi << "'! (Options: " << options << ")\n\n";

                    ASSERT_TRUE(false);
                    return;
                }

                CLFFTInPlacePtr clFFTPtr = factoryPtr->create(options);
                ASSERT_TRUE((bool) clFFTPtr);

                Profiler pInit;
                pInit.start();
                ASSERT_TRUE(clFFTPtr->init(size));
                pInit.stop();
                cout << FADED_COLOR << "\nInit took " << StringUtil::format("%.6f", pInit.getTotalTimeSec()) << " seconds.\n" << endl;
                cout << DEFAULT_FG_COLOR;

                Profiler profiler;

                int origin = 0;
                int sz = size * sizeof(base_type) * 2;
                for(int i = 0; i < iterations; i++) {

                      ASSERT_TRUE(origin + sz <= inBuffer.getSize());
                      rev::CLBuffer buf = inBuffer.createSubBuffer(origin, sz);
                      clFFTPtr->fft(buf, profiler);
                      origin += sz;
                  }

                printProfilingInfo(spi, profiler);
            }

    template<typename base_type>
    static void testInterleavedVsFlat(const char* spi, int size,
            CLAOption options,
            base_type maxDiff) {
        typedef ArrayWrapper1D<base_type> ArrayType;
        typedef std::shared_ptr<ArrayType> ArrayTypePtr;

        int arraySize = 2 * size;

        cout << DEFAULT_BG_COLOR;
        const ::testing::TestInfo * const test_info =
                ::testing::UnitTest::GetInstance()->current_test_info();
        cout << "BEGINNING TEST: -------------------------------------- "
                << EMPHASIS_COLOR << test_info->test_case_name() << "." << test_info->name()
                << DEFAULT_FG_COLOR << " ---------------------------------------" << endl;


        cout << TITLE_COLOR << "\n\nComparing INTERLEAVED VS FLAT for SPI '" << EMPHASIS_COLOR
                << spi << TITLE_COLOR << "' with parameters\n"
                << "WIDTH: " << WIDTH_COLOR << size << TITLE_COLOR
                << "; Options: " << options << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ArrayTypePtr interVecPtr = ArrayUtil::generateArray1D<base_type > (arraySize, -500, 500, 5);
        Array1D<base_type> flatVec(arraySize);

        ArrayUtil::flattenComplexArray<base_type > (interVecPtr->data(),
                flatVec.data(), size);

        CLBuffer inBuffer1(interVecPtr->getSize() * sizeof (base_type));
        printSize("inBuffer1", inBuffer1.getSize());
        ASSERT_TRUE(inBuffer1.create());

        CLBuffer inBuffer2(interVecPtr->getSize() * sizeof (base_type));
        printSize("inBuffer2", inBuffer1.getSize());
        ASSERT_TRUE(inBuffer2.create());

        ASSERT_TRUE(inBuffer1.write(interVecPtr->data()));
        ASSERT_TRUE(inBuffer2.write(flatVec.data()));

        CLFFTInPlaceAbstractFactory& abstractFactory1 = CLFFTInPlaceAbstractFactory::getInstance();

        CLFFTInPlaceFactoryPtr factoryPtr1 = abstractFactory1.getFactoryFor(spi);
        ASSERT_TRUE((bool) factoryPtr1) << "ERROR: Cannot find factory for spi: '"
                << spi << "'!!\n";

        if (factoryPtr1->areOptionsSupported(options) == false) {
            cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                    << spi << "'! (Options: " << options << ")\n\n";

            ASSERT_TRUE(false);
            return;
        }

        CLAOption flatOptions = ICLAlgorithm::OPTION_ARR_FLAT | options;

        if (factoryPtr1->areOptionsSupported(flatOptions) == false) {
            cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                    << spi << "'! (Options: " << flatOptions << ")\n\n";

            ASSERT_TRUE(false);
            return;
        }

        CLFFTInPlacePtr clFFTPtr1 = factoryPtr1->create(options);
        ASSERT_TRUE((bool) clFFTPtr1);

        CLFFTInPlacePtr clFFTPtr2 = factoryPtr1->create(flatOptions);
        ASSERT_TRUE((bool) clFFTPtr2);

        Profiler pInit;
        pInit.start();
        ASSERT_TRUE(clFFTPtr1->init(size));
        pInit.stop();
        cout << FADED_COLOR << "\nInit '" << spi << "' took "
                << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                << " seconds." << endl;

        pInit.reset();
        pInit.start();
        ASSERT_TRUE(clFFTPtr2->init(size));
        pInit.stop();
        cout << FADED_COLOR << "Init '" << spi << "' took "
                << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                << " seconds.\n" << endl;
        cout << DEFAULT_FG_COLOR;

        Profiler profiler1;
        Profiler profiler2;

        clFFTPtr1->fft(inBuffer1, profiler1);
        clFFTPtr2->fft(inBuffer2, profiler2);

        printProfilingInfo(spi, profiler1);
        printProfilingInfo(spi, profiler2);

        Array1D<base_type> outArr1(arraySize);
        Array1D<base_type> outArr2Temp(arraySize);
        Array1D<base_type> outArr2(arraySize);

        ASSERT_TRUE(inBuffer1.read(outArr1.data()));
        ASSERT_TRUE(inBuffer2.read(outArr2Temp.data()));

        ArrayUtil::interleaveComplexArray<base_type > (outArr2Temp.data(),
                outArr2.data(), size);
        //  cout << "outArr1 = " << outArr1 << endl;
        //  cout << "\noutArr2 = " << outArr2 << endl;

        //ASSERT_TRUE(ArrayUtil::equals(outArr1, outArr2));

        base_type totalDiff;
        base_type meanDiff;
        bool eq = ArrayUtil::equalsApproximately(outArr1, outArr2, maxDiff, &totalDiff, &meanDiff);

        cout << "\nmaxDiff \t= " << maxDiff << endl;
        cout << "meanDiff \t= " << EMPHASIS_COLOR << meanDiff << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ASSERT_TRUE(eq);
    }

    template<typename base_type>
        static void testBufferOffset(const char* spi1, const char* spi2, int size, CLAOption options,
                base_type maxDiff, int iterations) {
            typedef ArrayWrapper1D<base_type> ArrayType;
            typedef std::shared_ptr<ArrayType> ArrayTypePtr;

            int arraySize = 2 * size;

            TestUtil::printHeader();

            TestUtil::printTitle(string("BUFFER OFFSET for SPI '") + spi1 + string("' vs '") + spi2 + string("'"),
            		a2s(R(size), R(options), R(iterations), R(maxDiff)));

            ArrayTypePtr vecPtr = ArrayUtil::generateArray1D<base_type > (arraySize * iterations, -500, 500, 5);

            cout << P(FADED_COLOR, string("vecPtr.getSize() = ") + a2s(vecPtr->getSize())) << endl;

            CLBuffer inBuffer1(vecPtr->getSize() * sizeof (base_type));
            printSize("inBuffer1", inBuffer1.getSize());
            ASSERT_TRUE(inBuffer1.create());


            CLBuffer inBuffer2(vecPtr->getSize() * sizeof (base_type));
            printSize("inBuffer2", inBuffer1.getSize());
            ASSERT_TRUE(inBuffer2.create());

            ASSERT_TRUE(inBuffer1.write(vecPtr->data()));
            ASSERT_TRUE(inBuffer2.write(vecPtr->data()));

            CLFFTInPlaceAbstractFactory& abstractFactory1 = CLFFTInPlaceAbstractFactory::getInstance();

            CLFFTInPlaceFactoryPtr factoryPtr1 = abstractFactory1.getFactoryFor(spi1);
            ASSERT_TRUE((bool) factoryPtr1) << "ERROR: Cannot find factory for spi: '"
                    << spi1 << "'!!\n";

            CLFFTInPlaceFactoryPtr factoryPtr2 = abstractFactory1.getFactoryFor(spi2);
            ASSERT_TRUE((bool) factoryPtr2) << "ERROR: Cannot find factory for spi: '"
                    << spi2 << "'!!\n";

            if (factoryPtr1->areOptionsSupported(options) == false) {
                cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                        << spi1 << "'! (Options: " << options << ")\n\n";

                ASSERT_TRUE(false);
                return;
            }

            if (factoryPtr2->areOptionsSupported(options) == false) {
                cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                        << spi2 << "'! (Options: " << options << ")\n\n";

                ASSERT_TRUE(false);
                return;
            }

            CLFFTInPlacePtr clFFTPtr1 = factoryPtr1->create(options);
            ASSERT_TRUE((bool) clFFTPtr1);

            CLFFTInPlacePtr clFFTPtr2 = factoryPtr2->create(options);
            ASSERT_TRUE((bool) clFFTPtr2);

            Profiler pInit;
            pInit.start();
            ASSERT_TRUE(clFFTPtr1->init(size));
            pInit.stop();
            cout << FADED_COLOR << "\nInit '" << spi1 << "' took "
                    << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                    << " seconds." << endl;

            pInit.reset();
            pInit.start();
            ASSERT_TRUE(clFFTPtr2->init(size));
            pInit.stop();
            cout << FADED_COLOR << "Init '" << spi2 << "' took "
                    << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                    << " seconds.\n" << endl;
            cout << DEFAULT_FG_COLOR;

            Profiler profiler1;
            Profiler profiler2;

            Array1D<base_type> outArr1(arraySize);
            Array1D<base_type> outArr2(arraySize);

            int offset = 0;
            for(int i = 0; i < iterations; i++) {

            	clFFTPtr1->setBufferOffset(offset);
            	clFFTPtr2->setBufferOffset(offset);

            	clFFTPtr1->fft(inBuffer1, profiler1);
            	clFFTPtr2->fft(inBuffer2, profiler2);

            	ASSERT_TRUE(inBuffer1.read(outArr1.data(), offset, arraySize * sizeof(base_type)));
				ASSERT_TRUE(inBuffer2.read(outArr2.data(), offset, arraySize * sizeof(base_type)));

				base_type totalDiff;
				base_type meanDiff;
				bool eq = ArrayUtil::equalsApproximately(outArr1, outArr2, maxDiff, &totalDiff, &meanDiff);

				if(!eq) {
					cout << "i \t= " << i << endl;
					cout << "\nmaxDiff \t= " << maxDiff << endl;
					cout << "meanDiff \t= " << EMPHASIS_COLOR << meanDiff << "\n\n";
					cout << DEFAULT_FG_COLOR;
				}

				ASSERT_TRUE(eq);

				offset += arraySize * sizeof(base_type);
            }

            printProfilingInfo(spi1, profiler1);
            printProfilingInfo(spi2, profiler2);
        }

    template<typename base_type>
            static void testBufferOffsetSelf(const char* spi1, int size, CLAOption options, int iterations) {
                typedef ArrayWrapper1D<base_type> ArrayType;
                typedef std::shared_ptr<ArrayType> ArrayTypePtr;

                int arraySize = 2 * size;

                TestUtil::printHeader();

                TestUtil::printTitle(string("BUFFER OFFSET 'SELF' for SPI '") + spi1 + string("'"),
                		a2s(R(size), R(options), R(iterations)));

                Array1D<base_type> vec(arraySize * iterations);
                base_type someConstVal(1.0);

                for(auto& v : vec) {
                	v = someConstVal;
                }

                cout << P(FADED_COLOR, string("vec.getSize() = ") + a2s(vec.getSize())) << endl;

                CLBuffer inBuffer1(vec.getSize() * sizeof (base_type));
                printSize("inBuffer1", inBuffer1.getSize());
                ASSERT_TRUE(inBuffer1.create());

                ASSERT_TRUE(inBuffer1.write(vec.data()));

                CLFFTInPlaceAbstractFactory& abstractFactory1 = CLFFTInPlaceAbstractFactory::getInstance();

                CLFFTInPlaceFactoryPtr factoryPtr1 = abstractFactory1.getFactoryFor(spi1);
                ASSERT_TRUE((bool) factoryPtr1) << "ERROR: Cannot find factory for spi: '"
                        << spi1 << "'!!\n";

                if (factoryPtr1->areOptionsSupported(options) == false) {
                    cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                            << spi1 << "'! (Options: " << options << ")\n\n";

                    ASSERT_TRUE(false);
                    return;
                }

                CLFFTInPlacePtr clFFTPtr1 = factoryPtr1->create(options);


                Profiler pInit;
                pInit.start();
                ASSERT_TRUE(clFFTPtr1->init(size));
                pInit.stop();
                cout << FADED_COLOR << "\nInit '" << spi1 << "' took "
                        << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                        << " seconds." << endl;

                pInit.reset();

                Profiler profiler1;
                Profiler profiler2;

                Array1D<base_type> outArr1(arraySize);
                Array1D<base_type> outArr2(arraySize);

                int offset = 0;
                int sz = arraySize * sizeof(base_type);

                clFFTPtr1->fft(inBuffer1, profiler1);
                ASSERT_TRUE(inBuffer1.read(outArr2.data(), offset, sz));

                offset += sz;

                for(int i = 1; i < iterations; i++) {

                	clFFTPtr1->setBufferOffset(offset);
                	clFFTPtr1->fft(inBuffer1, profiler1);

                	ASSERT_TRUE(inBuffer1.read(outArr1.data(), offset, sz));

    				bool eq = ArrayUtil::equals(outArr1, outArr2);

    				if(!eq) {
    					cout << "i \t= " << i << endl;
    				}

    				ASSERT_TRUE(eq);

    				offset += sz;
                }

                printProfilingInfo(spi1, profiler1);
            }
};

TEST(FFTInPlaceTest, CreateNoCLTest) {

	const int complexSize = 256;

	typedef double base_type;
	typedef ArrayWrapper1D<base_type> ArrayType;
	typedef std::shared_ptr<ArrayType> ArrayTypePtr;

	ArrayTypePtr vecPtr = ArrayUtil::generateArray1D<base_type>(complexSize * 2,
			-50, 50);

	//    cout << "\n\ninPtr = \n" << *vecPtr << endl;

	CLBuffer inBuffer(vecPtr->getSize() * sizeof(base_type));

	ASSERT_TRUE(inBuffer.create());

	ASSERT_TRUE(inBuffer.write(vecPtr->data()));

	CLFFTInPlaceAbstractFactory& factory =
			CLFFTInPlaceAbstractFactory::getInstance();

	CLFFTInPlacePtr clFFTInPlacePtr = factory.createNoCL(
			ICLAlgorithm::OPTION_TYPE_DOUBLE | ICLAlgorithm::OPTION_ARR_FLAT);

	ASSERT_TRUE((bool) clFFTInPlacePtr);

	ASSERT_TRUE(clFFTInPlacePtr->init(complexSize));

	clFFTInPlacePtr->fft(inBuffer, complexSize);
	Array1D<base_type> outArr(complexSize * 2);

	ASSERT_TRUE(inBuffer.read(outArr.data()));

	// cout << "vecPtr = \n" << outArr << endl;
}

TEST(FFTInPlaceTest, CompareD) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	string spi1 = ConfigReader::getInstance().getString("spi1", REVCLA_SPI_NO_CL);
	string spi2 = ConfigReader::getInstance().getString("spi2", REVCLA_SPI_NO_CL);

	FFTInPlaceTest::testCompare<double>(spi1.c_str(), spi2.c_str(), size,
			ICLAlgorithm::OPTION_TYPE_DOUBLE, 0.001);
}

TEST(FFTInPlaceTest, InverseNoCLd) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	FFTInPlaceTest::testInverse<double>(REVCLA_SPI_NO_CL, size,
			ICLAlgorithm::OPTION_TYPE_DOUBLE, 0.001);
}

TEST(FFTInPlaceTest, InverseNoCLf) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	FFTInPlaceTest::testInverse<float>(REVCLA_SPI_NO_CL, size, CLAOption(), 0.001);
}

TEST(FFTInPlaceTest, InverseApplef) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	FFTInPlaceTest::testInverse<float>(REVCLA_SPI_APPLE, size,
			ICLAlgorithm::OPTION_ARR_FLAT, 0.001, true);
}

TEST(FFTInPlaceTest, InverseAppleFlatf) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	FFTInPlaceTest::testInverse<float>(REVCLA_SPI_APPLE, size, CLAOption(), 0.001,
			true);
}

TEST(FFTInPlaceTest, CompareAppleVsNoCLf) {
	int size = ConfigReader::getInstance().getUint("size", 1024);

	FFTInPlaceTest::testCompare<float>(REVCLA_SPI_APPLE, REVCLA_SPI_NO_CL, size,
			CLAOption(), 0.4);
}

TEST(FFTInPlaceTest, CompareAppleVsNoCLFlatf) {
	int size = ConfigReader::getInstance().getUint("size", 1024);

	FFTInPlaceTest::testCompare<float>(REVCLA_SPI_APPLE, REVCLA_SPI_NO_CL, size,
			ICLAlgorithm::OPTION_ARR_FLAT, 0.4);
}

TEST(FFTInPlaceTest, InterleaveVsFlatNoCLd) {
	int size = ConfigReader::getInstance().getUint("size", 1024);

	FFTInPlaceTest::testInterleavedVsFlat<double>(REVCLA_SPI_NO_CL, size,
			ICLAlgorithm::OPTION_TYPE_DOUBLE, 0.4);
}

TEST(FFTInPlaceTest, InterleaveVsFlatNoCLf) {
	int size = ConfigReader::getInstance().getUint("size", 1024);

	FFTInPlaceTest::testInterleavedVsFlat<float>(REVCLA_SPI_NO_CL, size,
			CLAOption(), 0.4);
}

TEST(FFTInPlaceTest, InterleaveVsFlatApplef) {
	int size = ConfigReader::getInstance().getUint("size", 1024);

	FFTInPlaceTest::testInterleavedVsFlat<float>(REVCLA_SPI_APPLE, size,
			CLAOption(), 0.4);
}

TEST(FFTInPlaceTest, IterationsApplef) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	int iterations = ConfigReader::getInstance().getUint("iterations", 2048);

	FFTInPlaceTest::testIterations<float>(REVCLA_SPI_APPLE, size, CLAOption(),
			iterations);
}

TEST(FFTInPlaceTest, IterationsSubBufferApplef) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	int iterations = ConfigReader::getInstance().getUint("iterations", 2048);

	FFTInPlaceTest::testIterationsSubBuf<float>(REVCLA_SPI_APPLE, size, CLAOption(),
			iterations);
}

TEST(FFTInPlaceTest, BufferOffsetSelfApplef) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	int iterations = ConfigReader::getInstance().getUint("iterations", 512);

	FFTInPlaceTest::testBufferOffsetSelf<float>(REVCLA_SPI_APPLE, size, CLAOption(),
			iterations);
}

TEST(FFTInPlaceTest, BufferOffsetSelfNoCLf) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	int iterations = ConfigReader::getInstance().getUint("iterations", 512);

	FFTInPlaceTest::testBufferOffsetSelf<float>(REVCLA_SPI_NO_CL, size, CLAOption(),
			iterations);
}

TEST(FFTInPlaceTest, BufferOffsetAppleVsNoCLf) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	int iterations = ConfigReader::getInstance().getUint("iterations", 512);

	FFTInPlaceTest::testBufferOffset<float>(REVCLA_SPI_APPLE, REVCLA_SPI_NO_CL,
			size, CLAOption(), 0.012, iterations);
}

TEST(FFTInPlaceTest, Compare2DAppleVsNoCLf) {
	int width = ConfigReader::getInstance().getUint("width", 1024);
	int height = ConfigReader::getInstance().getUint("height", 512);

	FFTInPlaceTest::testCompare2D<float>(REVCLA_SPI_APPLE, REVCLA_SPI_NO_CL, width,
			height,
			ICLAlgorithm::OPTION_SCALED_FALSE | ICLAlgorithm::OPTION_TYPE_FLOAT
					| ICLAlgorithm::OPTION_ARR_INTERLEAVED, 0.4);
}
