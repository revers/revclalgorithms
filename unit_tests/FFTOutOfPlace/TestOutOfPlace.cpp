/*
 * TestFFTOutOfPlace.cpp
 *
 *  Created on: 03-09-2012
 *      Author: Revers
 */

#include <gtest/gtest.h>
#include <memory>
#include <iostream>
#include <cstdlib>

//#define REVCOMMON_NO_COLORIZED_OUT

#include <rev/common/RevErrorStream.h>
#include <rev/cl/RevCLContext.h>
#include <rev/cl/RevCLBuffer.h>

#include <rev/cla/api/RevCLFFTOutOfPlaceAbstractFactory.h>
#include <rev/cla/api/RevCLFFTInPlaceAbstractFactory.h>
#include <rev/cla/api/RevICLFFTOutOfPlace.h>
#include <rev/cla/api/RevICLFFTInPlace.h>

#include <rev/common/RevCommonUtil.h>
#include <rev/common/RevDataTypes.h>
#include <rev/common/RevArray.h>
#include <rev/common/RevArrayUtil.h>
#include <rev/common/RevConfigReader.h>
#include <rev/common/test/RevColorUtil.h>
#include <rev/common/RevProfiler.h>

using namespace rev;
using namespace std;

#define DEFAULT_BG_COLOR rev::bg_white
#define DEFAULT_FG_COLOR rev::fg_black
#define TITLE_COLOR rev::fg_blue
#define EMPHASIS_COLOR rev::fg_red
#define TIME_COLOR rev::fg_lo_green
#define FADED_COLOR rev::fg_gray
#define WIDTH_COLOR rev::fg_lo_red

class FFTOutOfPlaceTest: public ::testing::Test {
protected:

	FFTOutOfPlaceTest() {
	}

	virtual ~FFTOutOfPlaceTest() {
	}

	virtual void SetUp() {
	}

	virtual void TearDown() {
	}

public:

	static void printSize(const char* name, int size) {
		cout << FADED_COLOR;

		cout << "size of " << name << " \t= " << CommonUtil::getSizeMB(size)
		<< endl;

		cout << DEFAULT_FG_COLOR;
	}

	static void printProfilingInfo(const char* spiName, const Profiler& p) {
		cout << TIME_COLOR;

		cout << "Time for SPI: " << spiName << ":\t" << p.getTotalTimeMicro() <<" micro seconds." << endl;

        cout << DEFAULT_FG_COLOR;
    }

    template<typename base_type>
    static void testInverse(const char* spi, int size, CLAOption options,
            base_type maxDiff,
            bool scale = false) {
        typedef ArrayWrapper1D<base_type> ArrayType;
        typedef std::shared_ptr<ArrayType> ArrayTypePtr;

        cout << DEFAULT_BG_COLOR;
        const ::testing::TestInfo * const test_info =
                ::testing::UnitTest::GetInstance()->current_test_info();
        cout << "BEGINNING TEST: -------------------------------------- "
                << EMPHASIS_COLOR << test_info->test_case_name() << "." << test_info->name()
                << DEFAULT_FG_COLOR << " ---------------------------------------" << endl;

        cout << TITLE_COLOR << "\n\nTESTING INVERSE FFT SPI '"
                << EMPHASIS_COLOR << spi << TITLE_COLOR
                << "' with parameters\n"
                << "WIDTH: " << WIDTH_COLOR << size << TITLE_COLOR
                << "; Options: " << options << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ArrayTypePtr vecPtr = ArrayUtil::generateArray1D<base_type > (size * 2, -500, 500, 5);
        //cout << "in = " << *vecPtr << endl;

        CLBuffer inBuffer(vecPtr->getSize() * sizeof (base_type));
        printSize("inBuffer", inBuffer.getSize());

        CLBuffer outBuffer(vecPtr->getSize() * sizeof (base_type));
        printSize("outBuffer", outBuffer.getSize());

        ASSERT_TRUE(inBuffer.create());
        ASSERT_TRUE(outBuffer.create());

        ASSERT_TRUE(inBuffer.write(vecPtr->data()));


        CLFFTOutOfPlaceAbstractFactory& abstractFactory = CLFFTOutOfPlaceAbstractFactory::getInstance();
        CLFFTOutOfPlaceFactoryPtr factoryPtr = abstractFactory.getFactoryFor(spi);
        ASSERT_TRUE((bool) factoryPtr) << "ERROR: Cannot find factory for spi: '"
                << spi << "'!!\n";

        if (factoryPtr->areOptionsSupported(options) == false) {
            cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                    << spi << "'! (Options: " << options << ")\n\n";

            ASSERT_TRUE(false);
            return;
        }

        CLFFTOutOfPlacePtr clFFTPtr = factoryPtr->create(options);
        ASSERT_TRUE((bool) clFFTPtr);

        Profiler pInit;
        pInit.start();
        ASSERT_TRUE(clFFTPtr->init(size));
        pInit.stop();
        cout << FADED_COLOR << "\nInit took " << StringUtil::format("%.6f", pInit.getTotalTimeSec()) << " seconds.\n" << endl;
        cout << DEFAULT_FG_COLOR;

        Profiler profiler;

        clFFTPtr->fft(inBuffer, outBuffer, profiler);

        printProfilingInfo(spi, profiler);

        Array1D<base_type> newInArr(size * 2);
        ASSERT_TRUE(outBuffer.read(newInArr.data()));

        ASSERT_TRUE(inBuffer.write(newInArr.data()));

        profiler.reset();
        clFFTPtr->fft(inBuffer, outBuffer, true, profiler);
        printProfilingInfo(spi, profiler);

        Array1D<base_type> outArr(size * 2);
        ASSERT_TRUE(outBuffer.read(outArr.data()));

        //        cout << "\noutArr2 = " << outArr2 << endl;

        if (scale) {
            base_type factor = base_type(1) / size;
            for (auto& val : outArr) {
                val *= factor;
            }
        }

        base_type totalDiff;
        base_type meanDiff;

        bool eq = ArrayUtil::equalsApproximately(*vecPtr, outArr, maxDiff, &totalDiff, &meanDiff);
        cout << "\nmaxDiff \t= " << maxDiff << endl;
        cout << "meanDiff \t= " << EMPHASIS_COLOR << meanDiff << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ASSERT_TRUE(eq);
    }

    template<typename base_type>
        static void testCompareIPvsOOP(const char* spi1, const char* spi2, int size, CLAOption options,
                base_type maxDiff) {
            typedef ArrayWrapper1D<base_type> ArrayType;
            typedef std::shared_ptr<ArrayType> ArrayTypePtr;

            cout << DEFAULT_BG_COLOR;
            const ::testing::TestInfo * const test_info =
                    ::testing::UnitTest::GetInstance()->current_test_info();
            cout << "BEGINNING TEST: -------------------------------------- "
                    << EMPHASIS_COLOR << test_info->test_case_name() << "." << test_info->name()
                    << DEFAULT_FG_COLOR << " ---------------------------------------" << endl;

            cout << TITLE_COLOR << "\n\nCOMPARING FFTIP SPI '" << EMPHASIS_COLOR << spi1 << TITLE_COLOR << "' WITH FFTOoP '"
                    << EMPHASIS_COLOR << spi2 << TITLE_COLOR << "' with parameters\n"
                    << "WIDTH: " << WIDTH_COLOR << size << TITLE_COLOR
                    << "; Options: " << options << "\n\n";
            cout << DEFAULT_FG_COLOR;

            ArrayTypePtr vecPtr = ArrayUtil::generateArray1D<base_type > (size * 2, -500, 500, 5);
            // cout << "in = " << *vecPtr << endl;
            // cout << "vecPtr = \n" << *vecPtr << endl;

            CLBuffer inBuffer1(vecPtr->getSize() * sizeof (base_type));
            printSize("inBuffer1", inBuffer1.getSize());

            ASSERT_TRUE(inBuffer1.create());

            CLBuffer inBuffer2(vecPtr->getSize() * sizeof (base_type));
            printSize("inBuffer2", inBuffer1.getSize());

            CLBuffer outBuffer2(vecPtr->getSize() * sizeof (base_type));
            printSize("outBuffer2", outBuffer2.getSize());

            ASSERT_TRUE(inBuffer2.create());
            ASSERT_TRUE(outBuffer2.create());

            ASSERT_TRUE(inBuffer1.write(vecPtr->data()));
            ASSERT_TRUE(inBuffer2.write(vecPtr->data()));

            CLFFTInPlaceAbstractFactory& abstractFactory1 = CLFFTInPlaceAbstractFactory::getInstance();

            CLFFTInPlaceFactoryPtr factoryPtr1 = abstractFactory1.getFactoryFor(spi1);
            ASSERT_TRUE((bool) factoryPtr1) << "ERROR: Cannot find factory for spi: '"
                    << spi1 << "'!!\n";

            CLFFTOutOfPlaceAbstractFactory& abstractFactory2 = CLFFTOutOfPlaceAbstractFactory::getInstance();
            CLFFTOutOfPlaceFactoryPtr factoryPtr2 = abstractFactory2.getFactoryFor(spi2);
            ASSERT_TRUE((bool) factoryPtr2) << "ERROR: Cannot find factory for spi: '"
                    << spi1 << "'!!\n";

            if (factoryPtr1->areOptionsSupported(options) == false) {
                cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                        << spi1 << "'! (Options: " << options << ")\n\n";

                ASSERT_TRUE(false);
                return;
            }

            if (factoryPtr2->areOptionsSupported(options) == false) {
                cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                        << spi2 << "'! (Options: " << options << ")\n\n";

                ASSERT_TRUE(false);
                return;
            }

            CLFFTInPlacePtr clFFTPtr1 = factoryPtr1->create(options);
            ASSERT_TRUE((bool) clFFTPtr1);

            CLFFTOutOfPlacePtr clFFTPtr2 = factoryPtr2->create(options);
            ASSERT_TRUE((bool) clFFTPtr2);

            Profiler pInit;
            pInit.start();
            ASSERT_TRUE(clFFTPtr1->init(size));
            pInit.stop();
            cout << FADED_COLOR << "\nInit '" << spi1 << "' took "
                    << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                    << " seconds." << endl;

            pInit.reset();
            pInit.start();
            ASSERT_TRUE(clFFTPtr2->init(size));
            pInit.stop();
            cout << FADED_COLOR << "Init '" << spi2 << "' took "
                    << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                    << " seconds.\n" << endl;
            cout << DEFAULT_FG_COLOR;

            Profiler profiler1;
            Profiler profiler2;

            clFFTPtr1->fft(inBuffer1, profiler1);
            clFFTPtr2->fft(inBuffer2, outBuffer2, profiler2);

            printProfilingInfo(spi1, profiler1);
            printProfilingInfo(spi2, profiler2);

            Array1D<base_type> outArr1(size * 2);
            Array1D<base_type> outArr2(size * 2);

            ASSERT_TRUE(inBuffer1.read(outArr1.data()));
            ASSERT_TRUE(outBuffer2.read(outArr2.data()));

            // cout << "outArr1 = " << outArr1 << endl;
            // cout << "\noutArr2 = " << outArr2 << endl;

            base_type totalDiff;
            base_type meanDiff;
            bool eq = ArrayUtil::equalsApproximately(outArr1, outArr2, maxDiff, &totalDiff, &meanDiff);

            cout << "\nmaxDiff \t= " << maxDiff << endl;
            cout << "meanDiff \t= " << EMPHASIS_COLOR << meanDiff << "\n\n";
            cout << DEFAULT_FG_COLOR;

            ASSERT_TRUE(eq);
        }

    template<typename base_type>
    static void testCompare(const char* spi1, const char* spi2, int size,
            CLAOption options, base_type maxDiff) {
        typedef ArrayWrapper1D<base_type> ArrayType;
        typedef std::shared_ptr<ArrayType> ArrayTypePtr;

        int arraySize = size * 2;

        cout << DEFAULT_BG_COLOR;
        const ::testing::TestInfo * const test_info =
                ::testing::UnitTest::GetInstance()->current_test_info();
        cout << "BEGINNING TEST: -------------------------------------- "
                << EMPHASIS_COLOR << test_info->test_case_name() << "." << test_info->name()
                << DEFAULT_FG_COLOR << " ---------------------------------------" << endl;

        cout << TITLE_COLOR << "\n\nCOMPARING SPI '" << EMPHASIS_COLOR << spi1 << TITLE_COLOR << "' WITH '"
                << EMPHASIS_COLOR << spi2 << TITLE_COLOR << "' with parameters\n"
                << "WIDTH: " << WIDTH_COLOR << size << TITLE_COLOR
                << "; Options: " << options << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ArrayTypePtr vecPtr = ArrayUtil::generateArray1D<base_type > (arraySize, -500, 500);
        // cout << "vecPtr = \n" << *vecPtr << endl;

        CLBuffer inBuffer1(vecPtr->getSize() * sizeof (base_type));
        CLBuffer outBuffer1(vecPtr->getSize() * sizeof (base_type));
        printSize("inBuffer1", inBuffer1.getSize());
        printSize("outBuffer2", outBuffer1.getSize());

        ASSERT_TRUE(inBuffer1.create());
        ASSERT_TRUE(outBuffer1.create());

        CLBuffer inBuffer2(vecPtr->getSize() * sizeof (base_type));
        printSize("inBuffer2", inBuffer1.getSize());

        CLBuffer outBuffer2(vecPtr->getSize() * sizeof (base_type));
        printSize("outBuffer2", outBuffer2.getSize());

        ASSERT_TRUE(inBuffer2.create());
        ASSERT_TRUE(outBuffer2.create());

        ASSERT_TRUE(inBuffer1.write(vecPtr->data()));
        ASSERT_TRUE(inBuffer2.write(vecPtr->data()));

        CLFFTOutOfPlaceAbstractFactory& abstractFactory1 = CLFFTOutOfPlaceAbstractFactory::getInstance();

        CLFFTOutOfPlaceFactoryPtr factoryPtr1 = abstractFactory1.getFactoryFor(spi1);
        ASSERT_TRUE((bool) factoryPtr1) << "ERROR: Cannot find factory for spi: '"
                << spi1 << "'!!\n";

        CLFFTOutOfPlaceFactoryPtr factoryPtr2 = abstractFactory1.getFactoryFor(spi2);
        ASSERT_TRUE((bool) factoryPtr2) << "ERROR: Cannot find factory for spi: '"
                << spi1 << "'!!\n";

        if (factoryPtr1->areOptionsSupported(options) == false) {
            cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                    << spi1 << "'! (Options: " << options << ")\n\n";

            ASSERT_TRUE(false);
            return;
        }

        if (factoryPtr2->areOptionsSupported(options) == false) {
            cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                    << spi2 << "'! (Options: " << options << ")\n\n";

            ASSERT_TRUE(false);
            return;
        }

        CLFFTOutOfPlacePtr clFFTPtr1 = factoryPtr1->create(options);
        ASSERT_TRUE((bool) clFFTPtr1);

        CLFFTOutOfPlacePtr clFFTPtr2 = factoryPtr2->create(options);
        ASSERT_TRUE((bool) clFFTPtr2);

        Profiler pInit;
        pInit.start();
        ASSERT_TRUE(clFFTPtr1->init(size));
        pInit.stop();
        cout << FADED_COLOR << "\nInit '" << spi1 << "' took "
                << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                << " seconds." << endl;

        pInit.reset();
        pInit.start();
        ASSERT_TRUE(clFFTPtr2->init(size));
        pInit.stop();
        cout << FADED_COLOR << "Init '" << spi2 << "' took "
                << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                << " seconds.\n" << endl;
        cout << DEFAULT_FG_COLOR;

        Profiler profiler1;
        Profiler profiler2;

        clFFTPtr1->fft(inBuffer1, outBuffer1,profiler1);
        clFFTPtr2->fft(inBuffer2, outBuffer2, profiler2);

        printProfilingInfo(spi1, profiler1);
        printProfilingInfo(spi2, profiler2);

        Array1D<base_type> outArr1(arraySize);
        Array1D<base_type> outArr2(arraySize);

        ASSERT_TRUE(outBuffer1.read(outArr1.data()));
        ASSERT_TRUE(outBuffer2.read(outArr2.data()));

        base_type totalDiff;
        base_type meanDiff;
        bool eq = ArrayUtil::equalsApproximately(outArr1, outArr2, maxDiff, &totalDiff, &meanDiff);

        cout << "\nmaxDiff \t= " << maxDiff << endl;
        cout << "meanDiff \t= " << EMPHASIS_COLOR << meanDiff << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ASSERT_TRUE(eq);
    }

    template<typename base_type>
        static void testInterleavedVsFlat(const char* spi, int size,
                CLAOption options,
                base_type maxDiff) {
            typedef ArrayWrapper1D<base_type> ArrayType;
            typedef std::shared_ptr<ArrayType> ArrayTypePtr;

            int arraySize = 2 * size;

            cout << DEFAULT_BG_COLOR;
            const ::testing::TestInfo * const test_info =
                    ::testing::UnitTest::GetInstance()->current_test_info();
            cout << "BEGINNING TEST: -------------------------------------- "
                    << EMPHASIS_COLOR << test_info->test_case_name() << "." << test_info->name()
                    << DEFAULT_FG_COLOR << " ---------------------------------------" << endl;


            cout << TITLE_COLOR << "\n\nComparing INTERLEAVED VS FLAT for SPI '" << EMPHASIS_COLOR
                    << spi << TITLE_COLOR << "' with parameters\n"
                    << "WIDTH: " << WIDTH_COLOR << size << TITLE_COLOR
                    << "; Options: " << options << "\n\n";
            cout << DEFAULT_FG_COLOR;

            ArrayTypePtr interVecPtr = ArrayUtil::generateArray1D<base_type > (arraySize, -500, 500, 5);
            Array1D<base_type> flatVec(arraySize);

            ArrayUtil::flattenComplexArray<base_type > (interVecPtr->data(),
                    flatVec.data(), size);

            CLBuffer inBuffer1(interVecPtr->getSize() * sizeof (base_type));
            printSize("inBuffer1", inBuffer1.getSize());
            ASSERT_TRUE(inBuffer1.create());

            CLBuffer inBuffer2(interVecPtr->getSize() * sizeof (base_type));
            printSize("inBuffer2", inBuffer1.getSize());
            ASSERT_TRUE(inBuffer2.create());

            //-----
            CLBuffer outBuffer1(interVecPtr->getSize() * sizeof (base_type));
            printSize("outBuffer1", outBuffer1.getSize());

            CLBuffer outBuffer2(interVecPtr->getSize() * sizeof (base_type));
            printSize("outBuffer2", outBuffer2.getSize());

            ASSERT_TRUE(outBuffer1.create());
            ASSERT_TRUE(outBuffer2.create());
            //------

            ASSERT_TRUE(inBuffer1.write(interVecPtr->data()));
            ASSERT_TRUE(inBuffer2.write(flatVec.data()));

            CLFFTOutOfPlaceAbstractFactory& abstractFactory1 = CLFFTOutOfPlaceAbstractFactory::getInstance();

            CLFFTOutOfPlaceFactoryPtr factoryPtr1 = abstractFactory1.getFactoryFor(spi);
            ASSERT_TRUE((bool) factoryPtr1) << "ERROR: Cannot find factory for spi: '"
                    << spi << "'!!\n";

            if (factoryPtr1->areOptionsSupported(options) == false) {
                cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                        << spi << "'! (Options: " << options << ")\n\n";

                ASSERT_TRUE(false);
                return;
            }

            CLAOption flatOptions = ICLAlgorithm::OPTION_ARR_FLAT | options;

            if (factoryPtr1->areOptionsSupported(flatOptions) == false) {
                cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                        << spi << "'! (Options: " << flatOptions << ")\n\n";

                ASSERT_TRUE(false);
                return;
            }

            CLFFTOutOfPlacePtr clFFTPtr1 = factoryPtr1->create(options);
            ASSERT_TRUE((bool) clFFTPtr1);

            CLFFTOutOfPlacePtr clFFTPtr2 = factoryPtr1->create(flatOptions);
            ASSERT_TRUE((bool) clFFTPtr2);

            Profiler pInit;
            pInit.start();
            ASSERT_TRUE(clFFTPtr1->init(size));
            pInit.stop();
            cout << FADED_COLOR << "\nInit '" << spi << "' took "
                    << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                    << " seconds." << endl;

            pInit.reset();
            pInit.start();
            ASSERT_TRUE(clFFTPtr2->init(size));
            pInit.stop();
            cout << FADED_COLOR << "Init '" << spi << "' took "
                    << StringUtil::format("%.6f", pInit.getTotalTimeSec())
                    << " seconds.\n" << endl;
            cout << DEFAULT_FG_COLOR;

            Profiler profiler1;
            Profiler profiler2;

            clFFTPtr1->fft(inBuffer1, outBuffer1,profiler1);
            clFFTPtr2->fft(inBuffer2, outBuffer2, profiler2);

            printProfilingInfo(spi, profiler1);
            printProfilingInfo(spi, profiler2);

            Array1D<base_type> outArr1(arraySize);
            Array1D<base_type> outArr2Temp(arraySize);
            Array1D<base_type> outArr2(arraySize);

            ASSERT_TRUE(outBuffer1.read(outArr1.data()));
            ASSERT_TRUE(outBuffer2.read(outArr2Temp.data()));

            ArrayUtil::interleaveComplexArray<base_type > (outArr2Temp.data(),
                    outArr2.data(), size);
            //  cout << "outArr1 = " << outArr1 << endl;
            //  cout << "\noutArr2 = " << outArr2 << endl;

            //ASSERT_TRUE(ArrayUtil::equals(outArr1, outArr2));

            base_type totalDiff;
            base_type meanDiff;
            bool eq = ArrayUtil::equalsApproximately(outArr1, outArr2, maxDiff, &totalDiff, &meanDiff);

            cout << "\nmaxDiff \t= " << maxDiff << endl;
            cout << "meanDiff \t= " << EMPHASIS_COLOR << meanDiff << "\n\n";
            cout << DEFAULT_FG_COLOR;

            ASSERT_TRUE(eq);
        }
};

TEST(FFTOutOfPlaceTest, CreateNoCLTest) {

	const int complexSize = 256;

	typedef double base_type;
	typedef ArrayWrapper1D<base_type> ArrayType;
	typedef std::shared_ptr<ArrayType> ArrayTypePtr;

	ArrayTypePtr vecPtr = ArrayUtil::generateArray1D<base_type>(complexSize * 2,
			-50, 50);

	//    cout << "\n\ninPtr = \n" << *vecPtr << endl;

	CLBuffer inBuffer(vecPtr->getSize() * sizeof(base_type));
	CLBuffer outBuffer(vecPtr->getSize() * sizeof(base_type));

	ASSERT_TRUE(inBuffer.create());
	ASSERT_TRUE(outBuffer.create());

	ASSERT_TRUE(inBuffer.write(vecPtr->data()));

	CLFFTOutOfPlaceAbstractFactory& factory =
			CLFFTOutOfPlaceAbstractFactory::getInstance();

	CLFFTOutOfPlacePtr clFFTOutOfPlacePtr = factory.createNoCL(
			ICLAlgorithm::OPTION_TYPE_DOUBLE | ICLAlgorithm::OPTION_ARR_FLAT);

	ASSERT_TRUE((bool) clFFTOutOfPlacePtr);

	ASSERT_TRUE(clFFTOutOfPlacePtr->init( complexSize));
	Profiler profiler;
	clFFTOutOfPlacePtr->fft(inBuffer, outBuffer,profiler);
	Array1D<base_type> outArr(complexSize * 2);

	ASSERT_TRUE(outBuffer.read(outArr.data()));

	// cout << "vecPtr = \n" << outArr << endl;
}

TEST(FFTOutOfPlaceTest, CompareNoCLINvsOOPf) {

	int size = ConfigReader::getInstance().getUint("size", 1024);
	string spi1 = ConfigReader::getInstance().getString("spi1", REVCLA_SPI_NO_CL);
	string spi2 = ConfigReader::getInstance().getString("spi2", REVCLA_SPI_NO_CL);

	FFTOutOfPlaceTest::testCompareIPvsOOP<float>(spi1.c_str(), spi2.c_str(), size,
			CLAOption(), 0.01);
}

TEST(FFTOutOfPlaceTest, CompareNoCLINvsOOPd) {

	int size = ConfigReader::getInstance().getUint("size", 1024);
	string spi1 = ConfigReader::getInstance().getString("spi1", REVCLA_SPI_NO_CL);
	string spi2 = ConfigReader::getInstance().getString("spi2", REVCLA_SPI_NO_CL);

	FFTOutOfPlaceTest::testCompareIPvsOOP<double>(spi1.c_str(), spi2.c_str(), size,
			ICLAlgorithm::OPTION_TYPE_DOUBLE, 0.001);
}

TEST(FFTOutOfPlaceTest, CompareD) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	string spi1 = ConfigReader::getInstance().getString("spi1", REVCLA_SPI_NO_CL);
	string spi2 = ConfigReader::getInstance().getString("spi2", REVCLA_SPI_NO_CL);

	FFTOutOfPlaceTest::testCompare<double>(spi1.c_str(), spi2.c_str(), size,
			ICLAlgorithm::OPTION_TYPE_DOUBLE, 0.001);
}

TEST(FFTOutOfPlaceTest, InverseNoCLd) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	FFTOutOfPlaceTest::testInverse<double>("nocl", size,
			ICLAlgorithm::OPTION_TYPE_DOUBLE, 0.001);
}

TEST(FFTOutOfPlaceTest, InverseNoCLf) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	FFTOutOfPlaceTest::testInverse<float>(REVCLA_SPI_NO_CL, size, CLAOption(),
			0.001);
}

TEST(FFTOutOfPlaceTest, InverseApplef) {
	int size = ConfigReader::getInstance().getUint("size", 1024);
	FFTOutOfPlaceTest::testInverse<float>(REVCLA_SPI_APPLE, size, CLAOption(), 0.1,
			true);
}

TEST(FFTOutOfPlaceTest, CompareAppleVsNoCLf) {
	int size = ConfigReader::getInstance().getUint("size", 1024);

	FFTOutOfPlaceTest::testCompare<float>(REVCLA_SPI_APPLE, REVCLA_SPI_NO_CL, size,
			CLAOption(), 0.4);
}

TEST(FFTOutOfPlaceTest, CompareAppleINvsOOPf) {

	int size = ConfigReader::getInstance().getUint("size", 1024);

	FFTOutOfPlaceTest::testCompareIPvsOOP<float>(REVCLA_SPI_APPLE, REVCLA_SPI_NO_CL,
			size, CLAOption(), 0.4);
}

TEST(FFTOutOfPlaceTest, CompareAppleVsNoCLFlatf) {
	int size = ConfigReader::getInstance().getUint("size", 1024);

	FFTOutOfPlaceTest::testCompare<float>(REVCLA_SPI_APPLE, REVCLA_SPI_NO_CL, size,
			ICLAlgorithm::OPTION_ARR_FLAT, 0.4);
}

TEST(FFTOutOfPlaceTest, InterleaveVsFlatNoCLd) {
	int size = ConfigReader::getInstance().getUint("size", 1024);

	FFTOutOfPlaceTest::testInterleavedVsFlat<double>(REVCLA_SPI_NO_CL, size,
			ICLAlgorithm::OPTION_TYPE_DOUBLE, 0.4);
}

TEST(FFTOutOfPlaceTest, InterleaveVsFlatNoCLf) {
	int size = ConfigReader::getInstance().getUint("size", 1024);

	FFTOutOfPlaceTest::testInterleavedVsFlat<float>(REVCLA_SPI_NO_CL, size,
			CLAOption(), 0.4);
}

TEST(FFTOutOfPlaceTest, InterleaveVsFlatApplef) {
	int size = ConfigReader::getInstance().getUint("size", 1024);

	FFTOutOfPlaceTest::testInterleavedVsFlat<float>(REVCLA_SPI_APPLE, size,
			CLAOption(), 0.4);
}
