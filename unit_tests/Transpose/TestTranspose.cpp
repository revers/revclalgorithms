

/*
 * TestTranspose.cpp
 *
 *  Created on: 03-09-2012
 *      Author: Revers
 */

#include <gtest/gtest.h>
#include <memory>
#include <iostream>
#include <cstdlib>

//#define REVCOMMON_NO_COLORIZED_OUT

#include <rev/common/RevErrorStream.h>
#include <rev/cl/RevCLContext.h>
#include <rev/cl/RevCLBuffer.h>
#include <rev/cla/api/RevCLTransposeAbstractFactory.h>
#include <rev/cla/api/RevICLTranspose.h>

#include <rev/common/RevCommonUtil.h>
#include <rev/common/RevDataTypes.h>
#include <rev/common/RevArray.h>
#include <rev/common/RevArrayUtil.h>
#include <rev/common/RevConfigReader.h>
#include <rev/common/test/RevColorUtil.h>
#include <rev/common/RevProfiler.h>

using namespace rev;
using namespace std;

#define DEFAULT_BG_COLOR rev::bg_white
#define DEFAULT_FG_COLOR rev::fg_black
#define TITLE_COLOR rev::fg_blue
#define EMPHASIS_COLOR rev::fg_red
#define TIME_COLOR rev::fg_lo_green
#define FADED_COLOR rev::fg_gray

class TransposeTest : public ::testing::Test {
protected:

    TransposeTest() {
    }

    virtual ~TransposeTest() {
    }

    virtual void SetUp() {
    }

    virtual void TearDown() {
    }

public:

    static void printSize(const char* name, int size) {
        cout << FADED_COLOR;

        cout << "size of " << name << " \t= " << CommonUtil::getSizeMB(size) << endl;

        cout << DEFAULT_FG_COLOR;
    }

    static void printProfilingInfo(const char* spiName, const Profiler& p) {
        cout << TIME_COLOR;

        cout << "Time for SPI: " << spiName << ":\t" << p.getTotalTimeMicro() << " micro seconds." << endl;

        cout << DEFAULT_FG_COLOR;
    }

    template<typename base_type>
    static void testCompareResults(const char* spi1, const char* spi2, int width, int height, CLAOption options) {
        typedef ArrayWrapper2D<base_type> ArrayType;
        typedef std::shared_ptr<ArrayType> ArrayTypePtr;

        cout << DEFAULT_BG_COLOR;
        const ::testing::TestInfo * const test_info =
                ::testing::UnitTest::GetInstance()->current_test_info();
        cout << "BEGINNING TEST: -------------------------------------- "
                << EMPHASIS_COLOR << test_info->test_case_name() << "." << test_info->name()
                << DEFAULT_FG_COLOR << " ---------------------------------------" << endl;



        cout << TITLE_COLOR << "\n\nCOMPARING Transpose SPI '" << EMPHASIS_COLOR << spi1 << TITLE_COLOR << "' WITH '"
                << EMPHASIS_COLOR << spi2 << TITLE_COLOR << "' with parameters\nWIDTH: " << width
                << "; HEIGHT: " << height << "; Options: " << options << "\n\n";
        cout << DEFAULT_FG_COLOR;

        ArrayTypePtr vecPtr = ArrayUtil::generateArray2D<base_type > (width, height, -500, 500);
        // cout << "vecPtr = \n" << *vecPtr << endl;


        CLBuffer inBuffer1(vecPtr->getSize() * sizeof (base_type));
        printSize("inBuffer1", inBuffer1.getSize());

        CLBuffer outBuffer1(vecPtr->getSize() * sizeof (base_type));
        printSize("outBuffer1", outBuffer1.getSize());

        ASSERT_TRUE(inBuffer1.create());
        ASSERT_TRUE(outBuffer1.create());

        CLBuffer inBuffer2(vecPtr->getSize() * sizeof (base_type));
        printSize("inBuffer2", inBuffer1.getSize());

        CLBuffer outBuffer2(vecPtr->getSize() * sizeof (base_type));
        printSize("outBuffer2", outBuffer2.getSize());

        ASSERT_TRUE(inBuffer2.create());
        ASSERT_TRUE(outBuffer2.create());

        ASSERT_TRUE(inBuffer1.write(vecPtr->data()));
        ASSERT_TRUE(inBuffer2.write(vecPtr->data()));

        CLTransposeAbstractFactory& abstractFactory = CLTransposeAbstractFactory::getInstance();

        CLTransposeFactoryPtr factoryPtr1 = abstractFactory.getFactoryFor(spi1);
        ASSERT_TRUE((bool) factoryPtr1) << "ERROR: Cannot find factory for spi: '"
                << spi1 << "'!!\n";

        CLTransposeFactoryPtr factoryPtr2 = abstractFactory.getFactoryFor(spi2);
        ASSERT_TRUE((bool) factoryPtr2) << "ERROR: Cannot find factory for spi: '"
                << spi1 << "'!!\n";

        if (factoryPtr1->areOptionsSupported(options) == false) {
            cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                    << spi1 << "'! (Options: " << options << ")\n\n";

            ASSERT_TRUE(false);
            return;
        }

        if (factoryPtr2->areOptionsSupported(options) == false) {
            cout << "\n\n!!! TEST STOPPED: Options not supported by SPI '"
                    << spi2 << "'! (Options: " << options << ")\n\n";

            ASSERT_TRUE(false);
            return;
        }

        CLTransposePtr clTransposePtr1 = factoryPtr1->create(options);
        ASSERT_TRUE((bool) clTransposePtr1);

        CLTransposePtr clTransposePtr2 = factoryPtr2->create(options);
        ASSERT_TRUE((bool) clTransposePtr2);

        ASSERT_TRUE(clTransposePtr1->init());
        ASSERT_TRUE(clTransposePtr2->init());

        Profiler profiler1;
        Profiler profiler2;

        clTransposePtr1->transpose(inBuffer1, outBuffer1, width, height, profiler1);
        clTransposePtr2->transpose(inBuffer2, outBuffer2, width, height, profiler2);

        printProfilingInfo(spi1, profiler1);
        printProfilingInfo(spi2, profiler2);

        Array2D<base_type> outArr1(width, height);
        Array2D<base_type> outArr2(width, height);

        ASSERT_TRUE(outBuffer1.read(outArr1.data()));
        ASSERT_TRUE(outBuffer2.read(outArr2.data()));

        ASSERT_TRUE(ArrayUtil::equals(outArr1, outArr2));
    }
};

TEST(TransposeTest, CompareTransposeVec1f) {

    int width = ConfigReader::getInstance().getUint("transpose.width", 256);
    int height = ConfigReader::getInstance().getUint("transpose.height", 256);
    string spi1 = ConfigReader::getInstance().getString("transpose.spi1", REVCLA_SPI_OTHER);
    string spi2 = ConfigReader::getInstance().getString("transpose.spi2", REVCLA_SPI_NO_CL);

    TransposeTest::testCompareResults<float>(spi1.c_str(), spi2.c_str(), width, height, ICLAlgorithm::OPTION_TYPE_FLOAT);
}

TEST(TransposeTest, CompareTransposeVec1ui) {

    int width = ConfigReader::getInstance().getUint("transpose.width", 256);
    int height = ConfigReader::getInstance().getUint("transpose.height", 256);
    string spi1 = ConfigReader::getInstance().getString("transpose.spi1", REVCLA_SPI_OTHER);
    string spi2 = ConfigReader::getInstance().getString("transpose.spi2", REVCLA_SPI_NO_CL);

    TransposeTest::testCompareResults<uint > (spi1.c_str(), spi2.c_str(), width, height, ICLAlgorithm::OPTION_TYPE_UINT);
}

TEST(TransposeTest, CompareTransposeVec1i) {

    int width = ConfigReader::getInstance().getUint("transpose.width", 256);
    int height = ConfigReader::getInstance().getUint("transpose.height", 256);
    string spi1 = ConfigReader::getInstance().getString("transpose.spi1", REVCLA_SPI_OTHER);
    string spi2 = ConfigReader::getInstance().getString("transpose.spi2", REVCLA_SPI_NO_CL);

    TransposeTest::testCompareResults<int>(spi1.c_str(), spi2.c_str(), width, height, ICLAlgorithm::OPTION_TYPE_INT);
}

TEST(TransposeTest, CompareTransposeVec1d) {

    int width = ConfigReader::getInstance().getUint("transpose.width", 256);
    int height = ConfigReader::getInstance().getUint("transpose.height", 256);
    string spi1 = ConfigReader::getInstance().getString("transpose.spi1", REVCLA_SPI_OTHER);
    string spi2 = ConfigReader::getInstance().getString("transpose.spi2", REVCLA_SPI_NO_CL);

    TransposeTest::testCompareResults<double>(spi1.c_str(), spi2.c_str(), width, height, ICLAlgorithm::OPTION_TYPE_DOUBLE);
}

TEST(TransposeTest, CompareTransposeVec2f) {

    int width = ConfigReader::getInstance().getUint("transpose.width", 256);
    int height = ConfigReader::getInstance().getUint("transpose.height", 256);
    string spi1 = ConfigReader::getInstance().getString("transpose.spi1", REVCLA_SPI_OTHER);
    string spi2 = ConfigReader::getInstance().getString("transpose.spi2", REVCLA_SPI_NO_CL);

    TransposeTest::testCompareResults<rev::vec2f > (spi1.c_str(), spi2.c_str(), width, height, ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_VECTOR_2);
}

TEST(TransposeTest, CompareTransposeVec2d) {

    int width = ConfigReader::getInstance().getUint("transpose.width", 256);
    int height = ConfigReader::getInstance().getUint("transpose.height", 256);
    string spi1 = ConfigReader::getInstance().getString("transpose.spi1", REVCLA_SPI_OTHER);
    string spi2 = ConfigReader::getInstance().getString("transpose.spi2", REVCLA_SPI_NO_CL);

    TransposeTest::testCompareResults<rev::vec2d > (spi1.c_str(), spi2.c_str(), width, height, ICLAlgorithm::OPTION_TYPE_DOUBLE | ICLAlgorithm::OPTION_VECTOR_2);
}

TEST(TransposeTest, CompareTransposeVec3f) {

    int width = ConfigReader::getInstance().getUint("transpose.width", 256);
    int height = ConfigReader::getInstance().getUint("transpose.height", 256);
    string spi1 = ConfigReader::getInstance().getString("transpose.spi1", REVCLA_SPI_OTHER);
    string spi2 = ConfigReader::getInstance().getString("transpose.spi2", REVCLA_SPI_NO_CL);

    TransposeTest::testCompareResults<rev::vec3f > (spi1.c_str(), spi2.c_str(), width, height, ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_VECTOR_3);
}

TEST(TransposeTest, CompareTransposeVec3d) {

    int width = ConfigReader::getInstance().getUint("transpose.width", 256);
    int height = ConfigReader::getInstance().getUint("transpose.height", 256);
    string spi1 = ConfigReader::getInstance().getString("transpose.spi1", REVCLA_SPI_OTHER);
    string spi2 = ConfigReader::getInstance().getString("transpose.spi2", REVCLA_SPI_NO_CL);

    TransposeTest::testCompareResults<rev::vec3d > (spi1.c_str(), spi2.c_str(), width, height, ICLAlgorithm::OPTION_TYPE_DOUBLE | ICLAlgorithm::OPTION_VECTOR_3);
}

TEST(TransposeTest, CompareTransposeVec4f) {

    int width = ConfigReader::getInstance().getUint("transpose.width", 256);
    int height = ConfigReader::getInstance().getUint("transpose.height", 256);
    string spi1 = ConfigReader::getInstance().getString("transpose.spi1", REVCLA_SPI_OTHER);
    string spi2 = ConfigReader::getInstance().getString("transpose.spi2", REVCLA_SPI_NO_CL);

    TransposeTest::testCompareResults<rev::vec4f > (spi1.c_str(), spi2.c_str(), width, height, ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_VECTOR_4);
}

TEST(TransposeTest, CompareTransposeVec4d) {

    int width = ConfigReader::getInstance().getUint("transpose.width", 256);
    int height = ConfigReader::getInstance().getUint("transpose.height", 256);
    string spi1 = ConfigReader::getInstance().getString("transpose.spi1", REVCLA_SPI_OTHER);
    string spi2 = ConfigReader::getInstance().getString("transpose.spi2", REVCLA_SPI_NO_CL);

    TransposeTest::testCompareResults<rev::vec4d > (spi1.c_str(), spi2.c_str(), width, height, ICLAlgorithm::OPTION_TYPE_DOUBLE | ICLAlgorithm::OPTION_VECTOR_4);
}

TEST(TransposeTest, CreateNoCLTest) {


    const int WIDTH = 256;
    const int HEIGHT = 256;

    typedef double base_type;
    typedef ArrayWrapper2D<base_type> ArrayType;
    typedef std::shared_ptr<ArrayType> ArrayTypePtr;

    ArrayTypePtr vecPtr = ArrayUtil::generateArray2D<base_type > (WIDTH, HEIGHT, -50, 50);
    // cout << "vecPtr = \n" << *vecPtr << endl;

    CLBuffer inBuffer(vecPtr->getSize() * sizeof (base_type));
    CLBuffer outBuffer(vecPtr->getSize() * sizeof (base_type));

    ASSERT_TRUE(inBuffer.create());

    ASSERT_TRUE(outBuffer.create());

    ASSERT_TRUE(inBuffer.write(vecPtr->data()));

    CLTransposeAbstractFactory& factory = CLTransposeAbstractFactory::getInstance();

    CLTransposePtr clTransposePtr = factory.createNoCL(ICLAlgorithm::OPTION_TYPE_DOUBLE);
    ASSERT_TRUE((bool) clTransposePtr);

    ASSERT_TRUE(clTransposePtr->init());

    clTransposePtr->transpose(inBuffer, outBuffer, WIDTH, HEIGHT);
    Array2D<base_type> outArr(WIDTH, HEIGHT);

    ASSERT_TRUE(outBuffer.read(outArr.data()));

    //  cout << "vecPtr = \n" << outArr << endl;
}

TEST(TransposeTest, CreateOtherTest) {

    const int WIDTH = 256;
    const int HEIGHT = 256;

    typedef double base_type;
    typedef ArrayWrapper2D<base_type> ArrayType;
    typedef std::shared_ptr<ArrayType> ArrayTypePtr;

    ArrayTypePtr vecPtr = ArrayUtil::generateArray2D<base_type > (WIDTH, HEIGHT, -50, 50);
    // cout << "vecPtr = \n" << *vecPtr << endl;

    CLBuffer inBuffer(vecPtr->getSize() * sizeof (base_type));
    CLBuffer outBuffer(vecPtr->getSize() * sizeof (base_type));

    ASSERT_TRUE(inBuffer.create());

    ASSERT_TRUE(outBuffer.create());

    ASSERT_TRUE(inBuffer.write(vecPtr->data()));

    CLTransposeAbstractFactory& factory = CLTransposeAbstractFactory::getInstance();

    CLTransposePtr clTransposePtr = factory.create(REVCLA_SPI_OTHER, ICLAlgorithm::OPTION_TYPE_DOUBLE);
    ASSERT_TRUE((bool) clTransposePtr);

    ASSERT_TRUE(clTransposePtr->init());

    clTransposePtr->transpose(inBuffer, outBuffer, WIDTH, HEIGHT);
    Array2D<base_type> outArr(WIDTH, HEIGHT);

    ASSERT_TRUE(outBuffer.read(outArr.data()));

    //cout << "vecPtr = \n" << outArr << endl;
}

