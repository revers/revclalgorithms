/* 
 * File:   XMLAlgorithm.cpp
 * Author: Revers
 * 
 * Created on 3 sierpień 2012, 21:36
 */

#include <rev/cla/xml/RevXMLAlgorithm.h>
#include <rev/cla/xml/RevXMLFlag.h>

using namespace rev;

REV_IMPLEMENT_XML_FACTORY(XMLAlgorithm, "algorithm")