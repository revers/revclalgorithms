/* 
 * File:   RevXMLAlgorithms.cpp
 * Author: Revers
 * 
 * Created on 3 sierpień 2012, 21:44
 */

#include <rev/cla/xml/RevXMLAlgorithms.h>
#include <rev/cla/xml/RevXMLAlgorithm.h>

using namespace rev; 

REV_IMPLEMENT_XML_FACTORY(XMLAlgorithms, "algorithms")