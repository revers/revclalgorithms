/* 
 * File:   RevCLAOption.cpp
 * Author: Kamil
 * 
 * Created on 21 sierpień 2012, 11:50
 */

#include <rev/cla/RevCLAOption.h>

using namespace rev;

CLAOption& CLAOption::reduce() {
	CLAOption* checkList = this->next;
	this->next = NULL;

	while (checkList) {
		CLAOption& checkCLAOption = *checkList;

		if (!checkCLAOption) {
			checkList = checkList->next;
			continue;
		}

		bool sameGroup = false;
		CLAOption* newList = this;
		CLAOption* last = newList;

		while (newList) {
			if (checkCLAOption.group == newList->group) {
				sameGroup = true;
				break;
			}

			newList = newList->next;
			if (newList) {
				last = newList;
			}
		}

		CLAOption* tmp = checkList;
		checkList = checkList->next;
		tmp->next = NULL;

		if (sameGroup) {
			delete tmp;
		} else {
			last->next = tmp;
		}
	}

	CLAOption& thisRef = *this;

	if (thisRef.isEmpty() && thisRef.next && !thisRef.next->isEmpty()) {
		thisRef.name = thisRef.next->name;
		thisRef.group = thisRef.next->group;

		CLAOption* tmp = thisRef.next;
		thisRef.next = thisRef.next->next;
		tmp->next = NULL;
		delete tmp;
	}

	return *this;
}

CLAOption& CLAOption::operator=(const CLAOption& other) {
	if (this != &other) {
		if (next) {
			delete next;
			next = NULL;
		}

		name = other.name;
		group = other.group;
		if (other.next) {
			next = new CLAOption(*other.next);
		}
	}
	return *this;
}

int CLAOption::getCount() const {
	CLAOption* oList = const_cast<CLAOption*>(this);

	int sz = 0;
	while (oList) {
		sz++;
		oList = oList->next;
	}

	return sz;
}

CLAOption* CLAOption::getNext() {
	return next;
}

CLAOption* CLAOption::getLast() {
	CLAOption* last = const_cast<CLAOption*>(this);

	while (true) {
		if (last->next) {
			last = last->next;
		} else {
			break;
		}
	}

	return last;
}

CLAOption::CLAOption(const CLAOption& other) {
	name = other.name;
	group = other.group;

	if (other.next) {
		next = new CLAOption(*other.next);
	} else {
		next = NULL;
	}
}

CLAOption::CLAOption(const CLAOption& current, const CLAOption& next) :
		name(current.name), group(current.group) {

	if (current.next) {
		this->next = new CLAOption(*current.next);

		CLAOption* last = getLast();
		last->next = new CLAOption(next);
	} else {
		this->next = new CLAOption(next);
	}
}

bool CLAOption::operator==(const CLAOption& other) const {
	if (next == NULL && other.next == NULL) {
		return name == other.name && group == other.group;
	} else if (next != NULL && other.next != NULL) {
		return name == other.name && group == other.group && *next == *other.next;
	}

	return false;
}

bool CLAOption::operator!=(const CLAOption& other) const {
	if (next == NULL && other.next == NULL) {
		return name != other.name || group != other.group;
	} else if (next != NULL && other.next != NULL) {
		return name != other.name || group != other.group || *next != *other.next;
	}

	return true;
}

bool CLAOption::containsSingleOption(const CLAOption& other) const {
	if(other.isEmpty()) {
		return true;
	}
	CLAOption* oList = const_cast<CLAOption*>(this);

	while (oList) {
		if (oList->name == other.name && oList->group == other.group) {
			return true;
		}
		oList = oList->next;
	}

	return false;
}

bool CLAOption::contains(const std::string& other) const {
	CLAOption* oList = const_cast<CLAOption*>(this);

	while (oList) {
		if (*oList == other) {
			return true;
		}
		oList = oList->next;
	}

	return false;
}

bool CLAOption::contains(const CLAOption& others) const {
	CLAOption* oList = const_cast<CLAOption*>(&others);

	while (oList) {
		if (!containsSingleOption(*oList)) {
			return false;
		}
		oList = oList->next;
	}

	return true;
}

CLAOption CLAOption::getFirstOfGroup(const std::string& group) const {
	CLAOption* oList = const_cast<CLAOption*>(this);

	while (oList) {
		if (group == oList->group) {
			return CLAOption(oList->name, oList->group);
		}

		oList = oList->next;
	}

	return CLAOption();
}
//
//std::ostream& rev::operator<<(std::ostream& out, const CLAOption& o) {
//	out << "CLAOption[name=" << o.name << ", group=" << o.group << "]";
//	if (o.next) {
//		out << ", " << *o.next;
//	}
//
//	return out;
//}
