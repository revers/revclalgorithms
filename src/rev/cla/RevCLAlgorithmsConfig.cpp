/* 
 * File:   RevCLAlgorithmsConfig.cpp
 * Author: Revers
 * 
 * Created on 14 sierpień 2012, 19:54
 */

#include <rev/cla/RevCLAlgorithmsConfig.h>
#include <rev/cla/xml/RevXMLAlgorithms.h>

#include <rev/common/RevAssert.h>

using namespace rev;

CLAlgorithmsConfig& CLAlgorithmsConfig::getInstance() {
    static CLAlgorithmsConfig config;
    return config;
}

bool CLAlgorithmsConfig::load(const char* filename) {
    XMLAlgorithms algorithms;
    if (!algorithms.loadFromFile(filename)) {
        return false;
    }

    spiMap.clear();

    for (XMLAlgorithm* algorithm : algorithms.getAlgorithmList()) {
        assertMsg(spiMap.find(algorithm->getName()) == spiMap.end(),
                "Duplicated algorithm '" << algorithm->getName() << "' in '"
                << filename << "' file!!");

        spiMap[algorithm->getName()] = algorithm->getSpi();
    }

    return true;
}

const char* CLAlgorithmsConfig::getSPIfor(const char* algorithm) {
    if (spiMap.find(algorithm) == spiMap.end()) {
        return NULL;
    }

    for (auto& pair : spiMap) {
        if (pair.first == algorithm) {
            return pair.second.c_str();
        }
    }

    return NULL;
}

