/*
 * RevCLFFT2DInPlaceAbstractFactory.cpp
 *
 *  Created on: 25-09-2012
 *      Author: Revers
 */

#include <rev/cla/api/RevCLFFT2DInPlaceAbstractFactory.h>
#include <rev/cla/RevCLAlgorithmsConfig.h>

#include <rev/cla/spi/apple/RevCLFFT2DInPlace.h>
#include <rev/cla/spi/nocl/RevCLFFT2DInPlace.h>

using namespace std;
using namespace rev;

typedef rev::nocl::CLFFT2DInPlaceFactory NoCLFFT2DInPlaceFactory;
typedef rev::apple::CLFFT2DInPlaceFactory AppleFFT2DInPlaceFactory;

CLFFT2DInPlaceAbstractFactory::CLFFT2DInPlaceAbstractFactory() :
        ICLAlgorithmAbstractFactory(REVCLA_FFT2D_IN_PLACE, REVCLA_FFT2D_IN_PLACE_DEFAULT_SPI) {

    noclFactoryPtr = CLFFT2DInPlaceFactoryPtr(new NoCLFFT2DInPlaceFactory());

    CLFFT2DInPlaceFactoryPtr aplPtr(new AppleFFT2DInPlaceFactory());
    addCLAlgorithmFactory(aplPtr);
}

CLFFT2DInPlaceAbstractFactory& CLFFT2DInPlaceAbstractFactory::getInstance() {
    static CLFFT2DInPlaceAbstractFactory instance;
    return instance;
}

