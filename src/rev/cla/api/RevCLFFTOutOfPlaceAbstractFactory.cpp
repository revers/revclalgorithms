/*
 * RevCLFFTOutOfPlaceAbstractFactory.cpp
 *
 *  Created on: 08-09-2012
 *      Author: Revers
 */

#include <rev/cla/api/RevCLFFTOutOfPlaceAbstractFactory.h>
#include <rev/cla/RevCLAlgorithmsConfig.h>

#include <rev/cla/spi/apple/RevCLFFTOutOfPlace.h>
#include <rev/cla/spi/nocl/RevCLFFTOutOfPlace.h>

using namespace std;
using namespace rev;

typedef rev::nocl::CLFFTOutOfPlaceFactory NoCLFFTOutOfPlaceFactory;
typedef rev::apple::CLFFTOutOfPlaceFactory AppleFFTOutOfPlaceFactory;

CLFFTOutOfPlaceAbstractFactory::CLFFTOutOfPlaceAbstractFactory() :
		ICLAlgorithmAbstractFactory(REVCLA_FFT_OUT_OF_PLACE,
				REVCLA_FFT_OUT_OF_PLACE_DEFAULT_SPI) {

	noclFactoryPtr = CLFFTOutOfPlaceFactoryPtr(new NoCLFFTOutOfPlaceFactory);

	CLFFTOutOfPlaceFactoryPtr aplPtr(new AppleFFTOutOfPlaceFactory);
	addCLAlgorithmFactory(aplPtr);
}

CLFFTOutOfPlaceAbstractFactory& CLFFTOutOfPlaceAbstractFactory::getInstance() {
	static CLFFTOutOfPlaceAbstractFactory instance;
	return instance;
}

