/*
 * RevCLFFTInPlaceAbstractFactory.cpp
 *
 *  Created on: 08-09-2012
 *      Author: Revers
 */

#include <rev/cla/api/RevCLFFTInPlaceAbstractFactory.h>
#include <rev/cla/RevCLAlgorithmsConfig.h>

#include <rev/cla/spi/apple/RevCLFFTInPlace.h>
#include <rev/cla/spi/nocl/RevCLFFTInPlace.h>

using namespace std;
using namespace rev;

typedef rev::nocl::CLFFTInPlaceFactory NoCLFFTInPlaceFactory;
typedef rev::apple::CLFFTInPlaceFactory AppleFFTInPlaceFactory;

CLFFTInPlaceAbstractFactory::CLFFTInPlaceAbstractFactory() :
		ICLAlgorithmAbstractFactory(REVCLA_FFT_IN_PLACE, REVCLA_FFT_IN_PLACE_DEFAULT_SPI) {

	noclFactoryPtr = CLFFTInPlaceFactoryPtr(new NoCLFFTInPlaceFactory);

	CLFFTInPlaceFactoryPtr aplPtr(new AppleFFTInPlaceFactory);
	addCLAlgorithmFactory(aplPtr);
}

CLFFTInPlaceAbstractFactory& CLFFTInPlaceAbstractFactory::getInstance() {
	static CLFFTInPlaceAbstractFactory instance;
	return instance;
}

