/* 
 * File:   RevCLTransposeAbstractFactory.cpp
 * Author: Revers
 * 
 * Created on 19 sierpień 2012, 21:21
 */

#include <rev/cla/api/RevCLTransposeAbstractFactory.h>
#include <rev/cla/RevCLAlgorithmsConfig.h>

#include <rev/cla/spi/nvidia/RevCLTranspose.h>
#include <rev/cla/spi/other/RevCLTranspose.h>
#include <rev/cla/spi/nocl/RevCLTranspose.h>

using namespace std;
using namespace rev;

typedef rev::nocl::CLTransposeFactory NoCLTransposeFactory;
typedef rev::other::CLTransposeFactory OtherTransposeFactory;
typedef rev::nvidia::CLTransposeFactory NVTransposeFactory;

CLTransposeAbstractFactory::CLTransposeAbstractFactory() :
		ICLAlgorithmAbstractFactory(REVCLA_TRANSPOSE, REVCLA_TRANSPOSE_DEFAULT_SPI) {

	noclFactoryPtr = CLTransposeFactoryPtr(new NoCLTransposeFactory);

	CLTransposeFactoryPtr otherPtr(new OtherTransposeFactory);
	addCLAlgorithmFactory(otherPtr);

	CLTransposeFactoryPtr nvPtr(new NVTransposeFactory);
	addCLAlgorithmFactory(nvPtr);
}

CLTransposeAbstractFactory& CLTransposeAbstractFactory::getInstance() {
	static CLTransposeAbstractFactory instance;
	return instance;
}
