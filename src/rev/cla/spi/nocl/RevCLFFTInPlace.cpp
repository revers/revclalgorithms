/*
 * RevCLFFtInPlace.cpp
 *
 *  Created on: 08-09-2012
 *      Author: Revers
 */

#include "RevCLFFTInPlace.h"
#include <rev/common/RevDataTypes.h>

using namespace rev;
using namespace rev::nocl;

bool CLFFTInPlaceFactory::areOptionsSupported(CLAOption o) {
	o.reduce();

	static const CLAOption SUPPORTED_OPTIONS = ICLAlgorithm::OPTION_TYPE_DOUBLE
			| ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_ARR_INTERLEAVED
			| ICLAlgorithm::OPTION_ARR_FLAT | ICLAlgorithm::OPTION_SCALED_TRUE
			| ICLAlgorithm::OPTION_SCALED_FALSE;

	return SUPPORTED_OPTIONS.contains(o);
}

/**
 * @Override
 */
CLFFTInPlaceFactory::algorithm_t_ptr CLFFTInPlaceFactory::create() {

	REVCLA_TRACE("Creating Default FFTInPlace Factory for SPI: " REVCLA_SPI_NO_CL);

	CLFFTInPlaceFactory::algorithm_t_ptr ptr(new CLFFTInPlace<float>(CLAOption()));
	REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");

	return ptr;
}

/**
 * @Override
 */
CLFFTInPlaceFactory::algorithm_t_ptr CLFFTInPlaceFactory::create(
		CLAOption options) {
	options.reduce();

	if (!areOptionsSupported(options)) {
		REVCLA_ERR_MSG("ERROR: Cannot cretae FFTInPlace "
		"Factory for SPI: " REVCLA_SPI_NO_CL " with options: " << options);

		return nullptr;
	}

	REVCLA_TRACE(
			"\nCreating FFTInPlace Factory for SPI: " REVCLA_SPI_NO_CL " with requested options: " << options);

	if (options.contains(ICLAlgorithm::OPTION_TYPE_DOUBLE)) {
		CLFFTInPlaceFactory::algorithm_t_ptr ptr(new CLFFTInPlace<double>(options));
		REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");

		return ptr;
	} else {
		CLFFTInPlaceFactory::algorithm_t_ptr ptr(new CLFFTInPlace<float>(options));
		REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");

		return ptr;
	}

	assertMsg(false, "We shouldn't be here... Unhandled option(s)!");

	return nullptr;
}

