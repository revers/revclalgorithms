/*
 * RevCLFFT2DInPlace.cpp
 *
 *  Created on: 26-09-2012
 *      Author: Kamil
 */

#include "RevCLFFT2DInPlace.h"
#include <rev/common/RevDataTypes.h>

using namespace rev;
using namespace rev::nocl;

bool CLFFT2DInPlaceFactory::areOptionsSupported(CLAOption o) {
	o.reduce();

	static const CLAOption SUPPORTED_OPTIONS = ICLAlgorithm::OPTION_TYPE_DOUBLE
			| ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_ARR_INTERLEAVED
			| ICLAlgorithm::OPTION_SCALED_TRUE | ICLAlgorithm::OPTION_SCALED_FALSE;

	return SUPPORTED_OPTIONS.contains(o);
}

/**
 * @Override
 */
CLFFT2DInPlaceFactory::algorithm_t_ptr CLFFT2DInPlaceFactory::create() {

	REVCLA_TRACE(
			"Creating Default FFT2DInPlace Factory for SPI: " REVCLA_SPI_NO_CL);

	CLFFT2DInPlaceFactory::algorithm_t_ptr ptr(
			new CLFFT2DInPlace<float>(CLAOption()));
	REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");

	return ptr;
}

/**
 * @Override
 */
CLFFT2DInPlaceFactory::algorithm_t_ptr CLFFT2DInPlaceFactory::create(
		CLAOption options) {
	options.reduce();

	if (!areOptionsSupported(options)) {
		REVCLA_ERR_MSG("ERROR: Cannot cretae FFT2DInPlace "
		"Factory for SPI: " REVCLA_SPI_NO_CL " with options: " << options);

		return nullptr;
	}

	REVCLA_TRACE(
			"\nCreating FFT2DInPlace Factory for SPI: " REVCLA_SPI_NO_CL " with requested options: " << options);

	if (options.contains(ICLAlgorithm::OPTION_TYPE_DOUBLE)) {
		CLFFT2DInPlaceFactory::algorithm_t_ptr ptr(
				new CLFFT2DInPlace<double>(options));
		REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");

		return ptr;
	} else {
		CLFFT2DInPlaceFactory::algorithm_t_ptr ptr(
				new CLFFT2DInPlace<float>(options));
		REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");

		return ptr;
	}

	assertMsg(false, "We shouldn't be here... Unhandled option(s)!");

	return nullptr;
}

