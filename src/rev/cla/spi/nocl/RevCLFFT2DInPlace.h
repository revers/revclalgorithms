/*
 * RevCLFFT2DInPlace.h
 *
 *  Created on: 26-09-2012
 *      Author: Kamil
 */

#ifndef REVCLFFT2DINPLACE_NOCL_H_
#define REVCLFFT2DINPLACE_NOCL_H_

#include <cmath>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/RevICLAlgorithmFactory.hpp>
#include <rev/cla/api/RevICLFFT2DInPlace.h>
#include <rev/cla/RevCLAOption.h>

#include <rev/common/RevAssert.h>
#include <rev/common/RevArrayUtil.h>

#include "RevBasicTypes.hpp"

namespace rev {

	namespace nocl {

		template<typename T>
		class REVCLA_API CLFFT2DInPlace: public rev::ICLFFT2DInPlace {
			CLFFT2DInPlace(const CLFFT2DInPlace& orig);
			CLFFT2DInPlace& operator=(const CLFFT2DInPlace& orig);
		public:
			typedef T data_type;
			//	bool flat = false;
			bool scale = true;

			CLFFT2DInPlace(CLAOption options) :
					ICLFFT2DInPlace(options) {
			}

			/**
			 * Override
			 */
			virtual bool init(int width, int height) {
				ICLFFT2DInPlace::width = width;
				ICLFFT2DInPlace::height = height;

				CLAOption options = getEnabledOptions();

//				if (options.contains(ICLAlgorithm::OPTION_ARR_FLAT)) {
//					flat = true;
//				}
//
				if (options.contains(ICLAlgorithm::OPTION_SCALED_FALSE)) {
					scale = false;
				}

				return true;
			}

			/**
			 * @Override
			 */
			const char* getSPI() {
				return REVCLA_SPI_NO_CL;
			}

			/**
			 * @Override
			 */
			void fft(rev::CLBuffer& inOutBuffer, bool inverse) {

				assertMsg(
						(ICLFFT2DInPlace::width != 0) && !(ICLFFT2DInPlace::width & (ICLFFT2DInPlace::width - 1)),
						"Error: Size '" << ICLFFT2DInPlace::width << "' is not power of 2!!");

				assertMsg(
						(ICLFFT2DInPlace::height != 0) && !(ICLFFT2DInPlace::height & (ICLFFT2DInPlace::height - 1)),
						"Error: Size '" << ICLFFT2DInPlace::height << "' is not power of 2!!");

				int arrayLength = ICLFFT2DInPlace::width
						* ICLFFT2DInPlace::height * 2;
				std::unique_ptr<data_type[]> srcBuf(new data_type[arrayLength]);

				alwaysAssert(
						inOutBuffer.read(srcBuf.get(), ICLFFT2DInPlace::bufferOffset, arrayLength * sizeof(data_type)));

//				if (flat) {
//					std::unique_ptr<data_type[]> interBuf(
//							new data_type[ICLFFT2DInPlace::width * 2]);
//
//					ArrayUtil::interleaveComplexArray<data_type>(srcBuf.get(),
//							interBuf.get(), ICLFFT2DInPlace::width);
//
//					if (inverse) {
//						ifft(interBuf.get(), ICLFFT2DInPlace::width,
//								ICLFFT2DInPlace::height);
//					} else {
//						fft(interBuf.get(), ICLFFT2DInPlace::width,
//								ICLFFT2DInPlace::height);
//					}
//
//					ArrayUtil::flattenComplexArray<data_type>(interBuf.get(),
//							srcBuf.get(), ICLFFT2DInPlace::width);
//
//					alwaysAssert(
//							inOutBuffer.write(srcBuf.get(), ICLFFT2DInPlace::bufferOffset, ICLFFT2DInPlace::width * 2 * sizeof(data_type)));
//
//				} else {

				if (inverse) {
					ifft(srcBuf.get(), ICLFFT2DInPlace::width,
							ICLFFT2DInPlace::height);
				} else {
					fft(srcBuf.get(), ICLFFT2DInPlace::width,
							ICLFFT2DInPlace::height);
				}
				alwaysAssert(
						inOutBuffer.write(srcBuf.get(), ICLFFT2DInPlace::bufferOffset, arrayLength * sizeof(data_type)));

//				}
			}

			/**
			 * @Override
			 */
			void fft(rev::CLBuffer& inOutBuffer, bool inverse,
					rev::Profiler& profiler) {

				assertMsg(
						(ICLFFT2DInPlace::width != 0) && !(ICLFFT2DInPlace::width & (ICLFFT2DInPlace::width - 1)),
						"Error: Size '" << ICLFFT2DInPlace::width << "' is not power of 2!!");

				assertMsg(
						(ICLFFT2DInPlace::height != 0) && !(ICLFFT2DInPlace::height & (ICLFFT2DInPlace::height - 1)),
						"Error: Size '" << ICLFFT2DInPlace::height << "' is not power of 2!!");

				int arrayLength = ICLFFT2DInPlace::width
						* ICLFFT2DInPlace::height * 2;
				std::unique_ptr<data_type[]> srcBuf(new data_type[arrayLength]);
				alwaysAssert(
						inOutBuffer.read(srcBuf.get(), ICLFFT2DInPlace::bufferOffset, arrayLength * sizeof(data_type)));

//				if (flat) {
//					std::unique_ptr<data_type[]> interBuf(new data_type[arraySize]);
//
//					ArrayUtil::interleaveComplexArray<data_type>(srcBuf.get(),
//							interBuf.get(), ICLFFT2DInPlace::width);
//
//					profiler.start();
//					if (inverse) {
//						ifft(interBuf.get(), ICLFFT2DInPlace::width,
//								ICLFFT2DInPlace::height);
//					} else {
//						fft(interBuf.get(), ICLFFT2DInPlace::width,
//								ICLFFT2DInPlace::height);
//					}
//					profiler.stop();
//
//					ArrayUtil::flattenComplexArray<data_type>(interBuf.get(),
//							srcBuf.get(), ICLFFT2DInPlace::width);
//
//					alwaysAssert(
//							inOutBuffer.write(srcBuf.get(), ICLFFT2DInPlace::bufferOffset, ICLFFT2DInPlace::width * 2 * sizeof(data_type)));
//
//				} else {

				profiler.start();
				if (inverse) {
					ifft(srcBuf.get(), ICLFFT2DInPlace::width,
							ICLFFT2DInPlace::height);
				} else {
					fft(srcBuf.get(), ICLFFT2DInPlace::width,
							ICLFFT2DInPlace::height);
				}
				profiler.stop();

				alwaysAssert(
						inOutBuffer.write(srcBuf.get(), ICLFFT2DInPlace::bufferOffset, arrayLength * sizeof(data_type)));

//				}
			}

		private:

			void fft(data_type* data, int xLen, int yLen) {

				for (int i = 0; i < yLen; i++) {
					data_type* dataPtr = data + i * xLen * 2;
					fft(dataPtr, xLen);
				}

				int bufferSize = xLen * yLen * 2 * sizeof(data_type);
				data_type* tempBuf = new data_type[bufferSize];

				transpose(data, tempBuf, xLen, yLen);

				for (int i = 0; i < xLen; i++) {
					data_type* dataPtr = tempBuf + i * yLen * 2;
					fft(dataPtr, yLen);
				}

				transpose(tempBuf, data, yLen, xLen);

				delete[] tempBuf;

				if (scale) {
					scaleData(data, xLen * yLen);
				}
			}

			void ifft(data_type* data, int xLen, int yLen) {
				for (int i = 0; i < yLen; i++) {
					data_type* dataPtr = data + i * xLen * 2;
					ifft(dataPtr, xLen);
				}

				int bufferSize = xLen * yLen * 2 * sizeof(data_type);
				data_type* tempBuf = new data_type[bufferSize];

				transpose(data, tempBuf, xLen, yLen);

				for (int i = 0; i < xLen; i++) {
					data_type* dataPtr = tempBuf + i * yLen * 2;
					ifft(dataPtr, yLen);
				}

				transpose(tempBuf, data, yLen, xLen);

				delete[] tempBuf;
			}

#define REAL(x) ((x) * 2)
#define IMAG(x) ((x) * 2 + 1)

			void transpose(data_type* dataIn, data_type* dataOut, int xLen,
					int yLen) {
				for (int j = 0; j < yLen; j++) {
					for (int i = 0; i < xLen; i++) {
						int indexIn = i + xLen * j;
						int indexOut = j + yLen * i;
						//  outData[index_out] = inData[index_in];

						dataOut[REAL(indexOut)] = dataIn[REAL(indexIn)];
						dataOut[IMAG(indexOut)] = dataIn[IMAG(indexIn)];
					}
				}
			}

			void fft(data_type* data, int nn) {
				int n, mmax, m, j, istep, i;
				data_type wtemp, wr, wpr, wpi, wi, theta;
				data_type tempr, tempi;

				// reverse-binary reindexing
				n = nn << 1;
				j = 1;
				for (i = 1; i < n; i += 2) {
					if (j > i) {
						std::swap(data[j - 1], data[i - 1]);
						std::swap(data[j], data[i]);
					}
					m = nn;
					while (m >= 2 && j > m) {
						j -= m;
						m >>= 1;
					}
					j += m;
				};

				// here begins the Danielson-Lanczos section
				mmax = 2;
				while (n > mmax) {
					istep = mmax << 1;
					theta = -(2.0 * M_PI / mmax);
					wtemp = sin(0.5 * theta);
					wpr = -2.0 * wtemp * wtemp;
					wpi = sin(theta);
					wr = 1.0;
					wi = 0.0;
					for (m = 1; m < mmax; m += 2) {
						for (i = m; i <= n; i += istep) {
							j = i + mmax;
							tempr = wr * data[j - 1] - wi * data[j];
							tempi = wr * data[j] + wi * data[j - 1];

							data[j - 1] = data[i - 1] - tempr;
							data[j] = data[i] - tempi;
							data[i - 1] += tempr;
							data[i] += tempi;
						}
						wtemp = wr;
						wr += wr * wpr - wi * wpi;
						wi += wi * wpr + wtemp * wpi;
					}
					mmax = istep;
				}
			}

			void ifft(data_type* data, int nn) {
				int n, mmax, m, j, istep, i;
				data_type wtemp, wr, wpr, wpi, wi, theta;
				data_type tempr, tempi;

				// reverse-binary reindexing
				n = nn << 1;
				j = 1;
				for (i = 1; i < n; i += 2) {
					if (j > i) {
						std::swap(data[j - 1], data[i - 1]);
						std::swap(data[j], data[i]);
					}
					m = nn;
					while (m >= 2 && j > m) {
						j -= m;
						m >>= 1;
					}
					j += m;
				};

				// here begins the Danielson-Lanczos section
				mmax = 2;
				while (n > mmax) {
					istep = mmax << 1;

					// -2.0 instead of 2.0 in fft
					theta = -(-2.0 * M_PI / mmax);
					wtemp = sin(0.5 * theta);
					wpr = -2.0 * wtemp * wtemp;
					wpi = sin(theta);
					wr = 1.0;
					wi = 0.0;
					for (m = 1; m < mmax; m += 2) {
						for (i = m; i <= n; i += istep) {
							j = i + mmax;
							tempr = wr * data[j - 1] - wi * data[j];
							tempi = wr * data[j] + wi * data[j - 1];

							data[j - 1] = data[i - 1] - tempr;
							data[j] = data[i] - tempi;
							data[i - 1] += tempr;
							data[i] += tempi;
						}
						wtemp = wr;
						wr += wr * wpr - wi * wpi;
						wi += wi * wpr + wtemp * wpi;
					}
					mmax = istep;
				}
			}

			void scaleData(data_type* data, int nn) {
				// scale:
				data_type val = 1.0 / nn;
				for (int index = 0; index < nn * 2; index++) {
					data[index] *= val;
				}
			}

			/**
			 * Override
			 */
			virtual CLAOption getDefaultOptions() {
				return ICLAlgorithm::OPTION_TYPE_FLOAT
						| ICLAlgorithm::OPTION_ARR_INTERLEAVED
						| ICLAlgorithm::OPTION_SCALED_FALSE;
			}
		};

		class REVCLA_API CLFFT2DInPlaceFactory: public rev::ICLAlgorithmFactory<
				rev::ICLFFT2DInPlace> {

			/**
			 * @Override
			 */
			const char* getSPI() {
				return REVCLA_SPI_NO_CL;
			}

			/**
			 * @Override
			 */
			bool areOptionsSupported(CLAOption options);

			/**
			 * @Override
			 */
			algorithm_t_ptr create();

			/**
			 * @Override
			 */
			algorithm_t_ptr create(CLAOption options);

		};
	}
}

#endif /* REVCLFFT2DINPLACE_NOCL_H_ */
