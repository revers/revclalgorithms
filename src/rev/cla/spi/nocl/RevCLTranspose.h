/* 
 * File:   RevCLTranspose.h
 * Author: Revers
 *
 * Created on 19 sierpień 2012, 21:52
 */

#ifndef REVCLTRANSPOSE_NOCL_H
#define	REVCLTRANSPOSE_NOCL_H

#include <rev/common/RevAssert.h>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/api/RevICLTranspose.h>
#include <rev/cla/RevICLAlgorithmFactory.hpp>
#include <rev/cla/RevCLAOption.h>

#include "RevBasicTypes.hpp"
#include <memory>

namespace rev {

	namespace nocl {

		template<typename T>
		class REVCLA_API CLTranspose: public rev::ICLTranspose {
			CLTranspose(const CLTranspose& orig);
			CLTranspose& operator=(const CLTranspose& orig);
		public:
			typedef T cell_type;
			const int cell_size = sizeof(cell_type);

			CLTranspose(CLAOption options) :
					ICLTranspose(options) {
			}

			/**
			 * @Override
			 */
			const char* getSPI() {
				return REVCLA_SPI_NO_CL;
			}

			/**
			 * @Override
			 */
			void transpose(rev::CLBuffer& src, rev::CLBuffer& dest, int width,
					int height, rev::Profiler& profiler) {

				assert(width * height * cell_size <= src.getSize());
				assert(width * height * cell_size <= dest.getSize());

				std::unique_ptr<cell_type[]> srcBuf(new cell_type[width * height]);
				std::unique_ptr<cell_type[]> destBuf(new cell_type[width * height]);

				alwaysAssert(src.read(srcBuf.get()));

				profiler.start();

				for (int j = 0; j < height; j++) {
					for (int i = 0; i < width; i++) {
						destBuf[i * height + j] = srcBuf[j * width + i];
					}
				}

				profiler.stop();

				alwaysAssert(dest.write(destBuf.get()));
			}

			/**
			 * @Override
			 */
			void transpose(rev::CLBuffer& src, rev::CLBuffer& dest, int width,
					int height, int localWidth, int localHeight) {

				assert(width * height * cell_size <= src.getSize());
				assert(width * height * cell_size <= dest.getSize());

				std::unique_ptr<cell_type[]> srcBuf(new cell_type[width * height]);
				std::unique_ptr<cell_type[]> destBuf(new cell_type[width * height]);

				alwaysAssert(src.read(srcBuf.get()));

				for (int j = 0; j < height; j++) {
					for (int i = 0; i < width; i++) {
						destBuf[i * height + j] = srcBuf[j * width + i];
					}
				}

				alwaysAssert(dest.write(destBuf.get()));
			}
		};

		class REVCLA_API CLTransposeFactory: public rev::ICLAlgorithmFactory<
				rev::ICLTranspose> {

			/**
			 * @Override
			 */
			const char* getSPI() {
				return REVCLA_SPI_NO_CL;
			}

			/**
			 * @Override
			 */
			bool areOptionsSupported(CLAOption options);

			/**
			 * @Override
			 */
			algorithm_t_ptr create();

			/**
			 * @Override
			 */
			algorithm_t_ptr create(CLAOption options);

		};
	}
}
#endif	/* REVCLTRANSPOSE_NOCL_H */

