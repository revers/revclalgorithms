/* 
 * File:   RevBasicTypes.hpp
 * Author: Revers
 *
 * Created on 20 sierpień 2012, 20:55
 */

#ifndef REVBASICTYPES_HPP
#define	REVBASICTYPES_HPP


namespace rev {
    namespace nocl {

        template<typename T>
        struct vec1 {
            T x;
        };
        
        template<typename T>
        struct vec2 {
            T x;
            T y;
        };
        
        template<typename T>
        struct vec3 {
            T x;
            T y;
            T z;
        };
        
        template<typename T>
        struct vec4 {
            T x;
            T y;
            T z;
            T w;
        };

        typedef vec1<float> vec1f;
        typedef vec2<float> vec2f;
        typedef vec3<float> vec3f;
        typedef vec4<float> vec4f;
        
        typedef vec1<double> vec1d;
        typedef vec2<double> vec2d;
        typedef vec3<double> vec3d;
        typedef vec4<double> vec4d;
    }
}

#endif	/* REVBASICTYPES_HPP */

