/* 
 * File:   RevCLTranspose.cpp
 * Author: Revers
 * 
 * Created on 19 sierpień 2012, 21:52
 */

#include "RevCLTranspose.h"

using namespace rev;
using namespace rev::nocl;

bool CLTransposeFactory::areOptionsSupported(CLAOption o) {
    o.reduce();

    static const CLAOption SUPPORTED_OPTIONS = ICLAlgorithm::OPTION_TYPE_DOUBLE
            | ICLAlgorithm::OPTION_TYPE_FLOAT
            | ICLAlgorithm::OPTION_TYPE_INT | ICLAlgorithm::OPTION_TYPE_UINT
            | ICLAlgorithm::OPTION_VECTOR_1 | ICLAlgorithm::OPTION_VECTOR_2
            | ICLAlgorithm::OPTION_VECTOR_3 | ICLAlgorithm::OPTION_VECTOR_4;

    return SUPPORTED_OPTIONS.contains(o);
}

/**
 * @Override
 */
CLTransposeFactory::algorithm_t_ptr CLTransposeFactory::create() {

    REVCLA_TRACE("Creating Default Transpose Factory for SPI: " REVCLA_SPI_NO_CL);

    CLTransposeFactory::algorithm_t_ptr ptr(new CLTranspose<vec1f > (CLAOption()));
    REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");

    return ptr;
}

/**
 * @Override
 */
CLTransposeFactory::algorithm_t_ptr CLTransposeFactory::create(CLAOption options) {
    options.reduce();

    if (!areOptionsSupported(options)) {
        REVCLA_ERR_MSG("ERROR: Cannot cretae Transpose "
                "Factory for SPI: " REVCLA_SPI_NO_CL " with options: " << options);

        return CLTransposeFactory::algorithm_t_ptr();
    }

    REVCLA_TRACE(
            "\nCreating Transpose Factory for SPI: " REVCLA_SPI_NO_CL " with requested options: " << options);

    if (options.contains(ICLAlgorithm::OPTION_TYPE_DOUBLE)) {
        if (options.contains(ICLAlgorithm::OPTION_VECTOR_2)) {
            CLTransposeFactory::algorithm_t_ptr ptr(
                    new CLTranspose<vec2d > (options));
            REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
            return ptr;
        } else if (options.contains(ICLAlgorithm::OPTION_VECTOR_3)) {
            CLTransposeFactory::algorithm_t_ptr ptr(
                    new CLTranspose<vec3d > (options));
            REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
            return ptr;
        } else if (options.contains(ICLAlgorithm::OPTION_VECTOR_4)) {
            CLTransposeFactory::algorithm_t_ptr ptr(
                    new CLTranspose<vec4d > (options));
            REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
            return ptr;
        } else {
            CLTransposeFactory::algorithm_t_ptr ptr(
                    new CLTranspose<vec1d > (options));
            REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
            return ptr;
        }
    } else {
        if (options.contains(ICLAlgorithm::OPTION_VECTOR_2)) {
            CLTransposeFactory::algorithm_t_ptr ptr(
                    new CLTranspose<vec2f > (options));
            REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
            return ptr;
        } else if (options.contains(ICLAlgorithm::OPTION_VECTOR_3)) {
            CLTransposeFactory::algorithm_t_ptr ptr(
                    new CLTranspose<vec3f > (options));
            REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
            return ptr;
        } else if (options.contains(ICLAlgorithm::OPTION_VECTOR_4)) {
            CLTransposeFactory::algorithm_t_ptr ptr(
                    new CLTranspose<vec4f > (options));
            REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
            return ptr;
        } else {
            CLTransposeFactory::algorithm_t_ptr ptr(
                    new CLTranspose<vec1f > (options));
            REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
            return ptr;
        }
    }

    assertMsg(false, "We shouldn't be here... Unhandled option(s)!");

    return CLTransposeFactory::algorithm_t_ptr();
}

