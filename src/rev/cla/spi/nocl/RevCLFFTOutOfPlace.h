/*
 * RevCLOutOfPlace.h
 *
 *  Created on: 08-09-2012
 *      Author: Revers
 */

#ifndef REVCLOUTOFPLACE_NOCL_H_
#define REVCLOUTOFPLACE_NOCL_H_

#include <cmath>
#include <cstdlib>
#include <cstdio>

#include <rev/common/RevAssert.h>
#include <rev/common/RevArrayUtil.h>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/api/RevICLFFTOutOfPlace.h>
#include <rev/cla/RevICLAlgorithmFactory.hpp>
#include <rev/cla/RevCLAOption.h>

#include "RevBasicTypes.hpp"
#include "fft.h"

namespace rev {

	namespace nocl {

		template<typename T>
		class REVCLA_API CLFFTOutOfPlace: public rev::ICLFFTOutOfPlace {
			CLFFTOutOfPlace(const CLFFTOutOfPlace& orig);
			CLFFTOutOfPlace& operator=(const CLFFTOutOfPlace& orig);
		public:
			typedef T data_type;
			bool flat = false;

			CLFFTOutOfPlace(CLAOption options) :
					ICLFFTOutOfPlace(options) {
			}

			/**
			 * Override
			 */
			virtual bool init(int width) {
				ICLFFTOutOfPlace::width = width;

				CLAOption options = getEnabledOptions();

				if (options.contains(ICLAlgorithm::OPTION_ARR_FLAT)) {
					flat = true;
				}

				return true;
			}

			/**
			 * @Override
			 */
			const char* getSPI() {
				return REVCLA_SPI_NO_CL;
			}

			/**
			 * @Override
			 */
			void fft(rev::CLBuffer& inBuffer, rev::CLBuffer& outBuffer, bool inverse) {

				assertMsg((ICLFFTOutOfPlace::width != 0) && !(ICLFFTOutOfPlace::width & (ICLFFTOutOfPlace::width - 1)),
						"Error: Size '" << ICLFFTOutOfPlace::width << "' is not power of 2!!");

				std::unique_ptr<data_type[]> srcBuf(new data_type[ICLFFTOutOfPlace::width * 2]);
				std::unique_ptr<data_type[]> outBuf(new data_type[ICLFFTOutOfPlace::width * 2]);

				alwaysAssert(inBuffer.read(srcBuf.get()));

				if (flat) {
					ArrayUtil::interleaveComplexArray<data_type>(srcBuf.get(),
							outBuf.get(), ICLFFTOutOfPlace::width);

					CFFT<data_type> cfft;
					if (inverse) {
						cfft.Inverse((const complex<data_type>*) outBuf.get(),
								(complex<data_type>*) srcBuf.get(), ICLFFTOutOfPlace::width);
					} else {
						cfft.Forward((const complex<data_type>*) outBuf.get(),
								(complex<data_type>*) srcBuf.get(), ICLFFTOutOfPlace::width);
					}

					ArrayUtil::flattenComplexArray<data_type>(srcBuf.get(),
							outBuf.get(), ICLFFTOutOfPlace::width);

					alwaysAssert(outBuffer.write(outBuf.get()));
				} else {

					CFFT<data_type> cfft;
					if (inverse) {
						cfft.Inverse((const complex<data_type>*) srcBuf.get(),
								(complex<data_type>*) outBuf.get(), ICLFFTOutOfPlace::width);
					} else {
						cfft.Forward((const complex<data_type>*) srcBuf.get(),
								(complex<data_type>*) outBuf.get(), ICLFFTOutOfPlace::width);
					}

					alwaysAssert(outBuffer.write(outBuf.get()));
				}
			}

			/**
			 * @Override
			 */
			void fft(rev::CLBuffer& inBuffer, rev::CLBuffer& outBuffer,
					bool inverse, rev::Profiler& profiler) {

				assertMsg((ICLFFTOutOfPlace::width != 0) && !(ICLFFTOutOfPlace::width & (ICLFFTOutOfPlace::width - 1)),
						"Error: Size '" << ICLFFTOutOfPlace::width << "' is not power of 2!!");

				std::unique_ptr<data_type[]> srcBuf(new data_type[ICLFFTOutOfPlace::width * 2]);
				std::unique_ptr<data_type[]> outBuf(new data_type[ICLFFTOutOfPlace::width * 2]);

				alwaysAssert(inBuffer.read(srcBuf.get()));

				if (flat) {
					ArrayUtil::interleaveComplexArray<data_type>(srcBuf.get(),
							outBuf.get(), ICLFFTOutOfPlace::width);

					CFFT<data_type> cfft;

					profiler.start();
					if (inverse) {
						cfft.Inverse((const complex<data_type>*) outBuf.get(),
								(complex<data_type>*) srcBuf.get(), ICLFFTOutOfPlace::width);
					} else {
						cfft.Forward((const complex<data_type>*) outBuf.get(),
								(complex<data_type>*) srcBuf.get(), ICLFFTOutOfPlace::width);
					}
					profiler.stop();

					ArrayUtil::flattenComplexArray<data_type>(srcBuf.get(),
							outBuf.get(), ICLFFTOutOfPlace::width);

					alwaysAssert(outBuffer.write(outBuf.get()));
				} else {

					CFFT<data_type> cfft;

					profiler.start();
					if (inverse) {
						cfft.Inverse((const complex<data_type>*) srcBuf.get(),
								(complex<data_type>*) outBuf.get(), ICLFFTOutOfPlace::width);
					} else {
						cfft.Forward((const complex<data_type>*) srcBuf.get(),
								(complex<data_type>*) outBuf.get(), ICLFFTOutOfPlace::width);
					}
					profiler.stop();

					alwaysAssert(outBuffer.write(outBuf.get()));
				}
			}
		};

		class REVCLA_API CLFFTOutOfPlaceFactory: public rev::ICLAlgorithmFactory<
				rev::ICLFFTOutOfPlace> {

			/**
			 * @Override
			 */
			const char* getSPI() {
				return REVCLA_SPI_NO_CL;
			}

			/**
			 * @Override
			 */
			bool areOptionsSupported(CLAOption options);

			/**
			 * @Override
			 */
			algorithm_t_ptr create();

			/**
			 * @Override
			 */
			algorithm_t_ptr create(CLAOption options);

		};
	}
}

#endif /* REVCLOUTOFPLACE_NOCL_H_ */
