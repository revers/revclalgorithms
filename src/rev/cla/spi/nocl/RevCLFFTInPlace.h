/*
 * RevCLFFtInPlace.h
 *
 *  Created on: 08-09-2012
 *      Author: Revers
 */

#ifndef REVCLFFTINPLACE_NOCL_H_
#define REVCLFFTINPLACE_NOCL_H_

#include <cmath>

#include <rev/common/RevAssert.h>
#include <rev/common/RevArrayUtil.h>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/RevICLAlgorithmFactory.hpp>
#include <rev/cla/api/RevICLFFTInPlace.h>
#include <rev/cla/RevCLAOption.h>

#include "RevBasicTypes.hpp"

namespace rev {

	namespace nocl {

		template<typename T>
		class REVCLA_API CLFFTInPlace: public rev::ICLFFTInPlace {
			CLFFTInPlace(const CLFFTInPlace& orig);
			CLFFTInPlace& operator=(const CLFFTInPlace& orig);
		public:
			typedef T data_type;
			bool flat = false;
			bool scale = true;

			CLFFTInPlace(CLAOption options) :
					ICLFFTInPlace(options) {
			}

			/**
			 * Override
			 */
			virtual bool init(int width) {
				ICLFFTInPlace::width = width;

				CLAOption options = getEnabledOptions();

				if (options.contains(ICLAlgorithm::OPTION_ARR_FLAT)) {
					flat = true;
				}

				if (options.contains(ICLAlgorithm::OPTION_SCALED_FALSE)) {
					scale = false;
				}

				return true;
			}

			/**
			 * @Override
			 */
			const char* getSPI() {
				return REVCLA_SPI_NO_CL;
			}

			/**
			 * @Override
			 */
			void fft(rev::CLBuffer& inOutBuffer, bool inverse) {

				assertMsg(
						(ICLFFTInPlace::width != 0) && !(ICLFFTInPlace::width & (ICLFFTInPlace::width - 1)),
						"Error: Size '" << ICLFFTInPlace::width << "' is not power of 2!!");

				std::unique_ptr<data_type[]> srcBuf(
						new data_type[ICLFFTInPlace::width * 2]);

				alwaysAssert(
						inOutBuffer.read(srcBuf.get(), ICLFFTInPlace::bufferOffset, ICLFFTInPlace::width * 2 * sizeof(data_type)));

				if (flat) {
					std::unique_ptr<data_type[]> interBuf(
							new data_type[ICLFFTInPlace::width * 2]);

					ArrayUtil::interleaveComplexArray<data_type>(srcBuf.get(),
							interBuf.get(), ICLFFTInPlace::width);

					if (inverse) {
						ifft(interBuf.get(), ICLFFTInPlace::width);
					} else {
						fft(interBuf.get(), ICLFFTInPlace::width);
					}

					ArrayUtil::flattenComplexArray<data_type>(interBuf.get(),
							srcBuf.get(), ICLFFTInPlace::width);

					alwaysAssert(
							inOutBuffer.write(srcBuf.get(), ICLFFTInPlace::bufferOffset, ICLFFTInPlace::width * 2 * sizeof(data_type)));

				} else {

					if (inverse) {
						ifft(srcBuf.get(), ICLFFTInPlace::width);
					} else {
						fft(srcBuf.get(), ICLFFTInPlace::width);
					}
					alwaysAssert(
							inOutBuffer.write(srcBuf.get(), ICLFFTInPlace::bufferOffset, ICLFFTInPlace::width * 2 * sizeof(data_type)));

				}
			}

			/**
			 * @Override
			 */
			void fft(rev::CLBuffer& inOutBuffer, bool inverse,
					rev::Profiler& profiler) {

				assertMsg(
						(ICLFFTInPlace::width != 0) && !(ICLFFTInPlace::width & (ICLFFTInPlace::width - 1)),
						"Error: Size '" << ICLFFTInPlace::width << "' is not power of 2!!");

				std::unique_ptr<data_type[]> srcBuf(
						new data_type[ICLFFTInPlace::width * 2]);
				alwaysAssert(
						inOutBuffer.read(srcBuf.get(), ICLFFTInPlace::bufferOffset, ICLFFTInPlace::width * 2 * sizeof(data_type)));

				if (flat) {
					std::unique_ptr<data_type[]> interBuf(
							new data_type[ICLFFTInPlace::width * 2]);

					ArrayUtil::interleaveComplexArray<data_type>(srcBuf.get(),
							interBuf.get(), ICLFFTInPlace::width);

					profiler.start();
					if (inverse) {
						ifft(interBuf.get(), ICLFFTInPlace::width);
					} else {
						fft(interBuf.get(), ICLFFTInPlace::width);
					}
					profiler.stop();

					ArrayUtil::flattenComplexArray<data_type>(interBuf.get(),
							srcBuf.get(), ICLFFTInPlace::width);

					alwaysAssert(
							inOutBuffer.write(srcBuf.get(), ICLFFTInPlace::bufferOffset, ICLFFTInPlace::width * 2 * sizeof(data_type)));

				} else {

					profiler.start();
					if (inverse) {
						ifft(srcBuf.get(), ICLFFTInPlace::width);
					} else {
						fft(srcBuf.get(), ICLFFTInPlace::width);
					}
					profiler.stop();

					alwaysAssert(
							inOutBuffer.write(srcBuf.get(), ICLFFTInPlace::bufferOffset, ICLFFTInPlace::width * 2 * sizeof(data_type)));

				}
			}

		private:

			void fft(data_type* data, int nn) {
				int n, mmax, m, j, istep, i;
				data_type wtemp, wr, wpr, wpi, wi, theta;
				data_type tempr, tempi;

				// reverse-binary reindexing
				n = nn << 1;
				j = 1;
				for (i = 1; i < n; i += 2) {
					if (j > i) {
						std::swap(data[j - 1], data[i - 1]);
						std::swap(data[j], data[i]);
					}
					m = nn;
					while (m >= 2 && j > m) {
						j -= m;
						m >>= 1;
					}
					j += m;
				};

				// here begins the Danielson-Lanczos section
				mmax = 2;
				while (n > mmax) {
					istep = mmax << 1;
					theta = -(2.0 * M_PI / mmax);
					wtemp = sin(0.5 * theta);
					wpr = -2.0 * wtemp * wtemp;
					wpi = sin(theta);
					wr = 1.0;
					wi = 0.0;
					for (m = 1; m < mmax; m += 2) {
						for (i = m; i <= n; i += istep) {
							j = i + mmax;
							tempr = wr * data[j - 1] - wi * data[j];
							tempi = wr * data[j] + wi * data[j - 1];

							data[j - 1] = data[i - 1] - tempr;
							data[j] = data[i] - tempi;
							data[i - 1] += tempr;
							data[i] += tempi;
						}
						wtemp = wr;
						wr += wr * wpr - wi * wpi;
						wi += wi * wpr + wtemp * wpi;
					}
					mmax = istep;
				}
			}

			void ifft(data_type* data, int nn) {
				int n, mmax, m, j, istep, i;
				data_type wtemp, wr, wpr, wpi, wi, theta;
				data_type tempr, tempi;

				// reverse-binary reindexing
				n = nn << 1;
				j = 1;
				for (i = 1; i < n; i += 2) {
					if (j > i) {
						std::swap(data[j - 1], data[i - 1]);
						std::swap(data[j], data[i]);
					}
					m = nn;
					while (m >= 2 && j > m) {
						j -= m;
						m >>= 1;
					}
					j += m;
				};

				// here begins the Danielson-Lanczos section
				mmax = 2;
				while (n > mmax) {
					istep = mmax << 1;

					// -2.0 instead of 2.0 in fft
					theta = -(-2.0 * M_PI / mmax);
					wtemp = sin(0.5 * theta);
					wpr = -2.0 * wtemp * wtemp;
					wpi = sin(theta);
					wr = 1.0;
					wi = 0.0;
					for (m = 1; m < mmax; m += 2) {
						for (i = m; i <= n; i += istep) {
							j = i + mmax;
							tempr = wr * data[j - 1] - wi * data[j];
							tempi = wr * data[j] + wi * data[j - 1];

							data[j - 1] = data[i - 1] - tempr;
							data[j] = data[i] - tempi;
							data[i - 1] += tempr;
							data[i] += tempi;
						}
						wtemp = wr;
						wr += wr * wpr - wi * wpi;
						wi += wi * wpr + wtemp * wpi;
					}
					mmax = istep;
				}

				if (scale) {
					// scale:
					data_type val = 1.0 / nn;
					for (int index = 0; index < nn * 2; index++) {
						data[index] *= val;
					}
				}
			}

		};

		class REVCLA_API CLFFTInPlaceFactory: public rev::ICLAlgorithmFactory<
				rev::ICLFFTInPlace> {

			/**
			 * @Override
			 */
			const char* getSPI() {
				return REVCLA_SPI_NO_CL;
			}

			/**
			 * @Override
			 */
			bool areOptionsSupported(CLAOption options);

			/**
			 * @Override
			 */
			algorithm_t_ptr create();

			/**
			 * @Override
			 */
			algorithm_t_ptr create(CLAOption options);

		};
	}
}
#endif /* REVCLFFTINPLACE_NOCL_H_ */
