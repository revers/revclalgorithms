/*
 * RevCLOutOfPlace.cpp
 *
 *  Created on: 08-09-2012
 *      Author: Revers
 */

/*
 * RevCLFFTOutOfPlace.cpp
 *
 *  Created on: 08-09-2012
 *      Author: Revers
 */

#include "RevCLFFTOutOfPlace.h"
#include <rev/common/RevDataTypes.h>

using namespace rev;
using namespace rev::nocl;

bool CLFFTOutOfPlaceFactory::areOptionsSupported(CLAOption o) {
	o.reduce();

	static const CLAOption SUPPORTED_OPTIONS = ICLAlgorithm::OPTION_TYPE_DOUBLE
			| ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_ARR_INTERLEAVED
			| ICLAlgorithm::OPTION_ARR_FLAT | ICLAlgorithm::OPTION_SCALED_TRUE;

	return SUPPORTED_OPTIONS.contains(o);
}

/**
 * @Override
 */
CLFFTOutOfPlaceFactory::algorithm_t_ptr CLFFTOutOfPlaceFactory::create() {

	REVCLA_TRACE(
			"Creating Default FFTOutOfPlace Factory for SPI: " REVCLA_SPI_NO_CL);

	CLFFTOutOfPlaceFactory::algorithm_t_ptr ptr(
			new CLFFTOutOfPlace<float>(CLAOption()));
	REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");

	return ptr;
}

/**
 * @Override
 */
CLFFTOutOfPlaceFactory::algorithm_t_ptr CLFFTOutOfPlaceFactory::create(
		CLAOption options) {
	options.reduce();

	if (!areOptionsSupported(options)) {
		REVCLA_ERR_MSG("ERROR: Cannot cretae FFTOutOfPlace "
		"Factory for SPI: " REVCLA_SPI_NO_CL " with options: " << options);

		return nullptr;
	}

	REVCLA_TRACE(
			"\nCreating FFTOutOfPlace Factory for SPI: " REVCLA_SPI_NO_CL " with requested options: " << options);

	if (options.contains(ICLAlgorithm::OPTION_TYPE_DOUBLE)) {
		CLFFTOutOfPlaceFactory::algorithm_t_ptr ptr(
				new CLFFTOutOfPlace<double>(options));
		REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");

		return ptr;
	} else {
		CLFFTOutOfPlaceFactory::algorithm_t_ptr ptr(
				new CLFFTOutOfPlace<float>(options));
		REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");

		return ptr;
	}

	assertMsg(false, "We shouldn't be here... Unhandled option(s)!");

	return nullptr;
}

