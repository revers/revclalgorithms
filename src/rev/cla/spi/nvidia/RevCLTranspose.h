/* 
 * File:   RevCLTranspose.h
 * Author: Revers
 *
 * Created on 19 sierpień 2012, 21:52
 */

#ifndef REVCLTRANSPOSE_NVIDIA_H
#define	REVCLTRANSPOSE_NVIDIA_H

#include <string>

#include <rev/common/RevAssert.h>
#include <rev/cl/RevCLProgram.h>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/api/RevICLTranspose.h>
#include <rev/cla/RevICLAlgorithmFactory.hpp>
#include <rev/cla/RevCLAOption.h>

#include "CL_Transpose.h"

namespace rev {

	namespace nvidia {

		/**
		 * __kernel void transpose(__global data_t *odata, __global data_t *idata,
		 *  int width, int height, __local data_t* block, uint blockDim) {
		 */
		namespace TransposeKernel {

			enum {
				ARG_OUT_DATA,
				ARG_IN_DATA,
				ARG_WIDTH,
				ARG_HEIGHT,
				ARG_BLOCK,
				ARG_BLOCK_DIM
			};
		}

		class REVCLA_API CLTranspose: public rev::ICLTranspose {
			int localMemSize = 0;

			CLTranspose(const CLTranspose& orig);
			CLTranspose& operator=(const CLTranspose& orig);
			rev::CLProgram program;
			cl::Kernel transposeKernel;
		public:
			int cellSize = 0;

			CLTranspose(const CLAOption& options) :
					ICLTranspose(options) {
			}

			/**
			 * @Override
			 */
			bool init();

			/**
			 * @Override
			 */
			const char* getSPI() {
				return REVCLA_SPI_NVIDIA;
			}

			/**
			 * @Override
			 */
			virtual void transpose(rev::CLBuffer& src, rev::CLBuffer& dest,
					int width, int height, int localWidth,
					int localHeight);

			/**
			 * @Override
			 */
			virtual int getMinimalLocalMemorySize() {
				return localMemSize;
			}
		};

		class REVCLA_API CLTransposeFactory: public rev::ICLAlgorithmFactory<
				rev::ICLTranspose> {

			/**
			 * @Override
			 */
			const char* getSPI() {
				return REVCLA_SPI_NVIDIA;
			}

			/**
			 * @Override
			 */
			bool areOptionsSupported(CLAOption options);

			/**
			 * @Override
			 */
			algorithm_t_ptr create();

			/**
			 * @Override
			 */
			algorithm_t_ptr create(CLAOption options);

		};
	}
}
#endif	/* REVCLTRANSPOSE_NVIDIA_H */

