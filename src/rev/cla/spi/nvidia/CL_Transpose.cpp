/* 
 * WARNING: Autogenerated from file 'rev/spi/nvidia/cl_files/Transpose.cl'. Do not edit!
 */
#include "CL_Transpose.h"

const char* rev::nvidia::clTransposeSource = 
    "#ifdef FAKE_INCLUDE\n"
    "#include <FAKE_INCLUDE/opencl_fake.hxx>\n"
    "#endif\n"
    "\n"
    "#ifdef DOUBLE_TYPE\n"
    "#pragma OPENCL EXTENSION cl_khr_fp64 : enable\n"
    "#pragma OPENCL EXTENSION cl_amd_fp64 : enable\n"
    "#ifdef VECTOR_4\n"
    "typedef double4 data_t;\n"
    "#elif defined(VECTOR_3)\n"
    "typedef double3 data_t;\n"
    "#elif defined(VECTOR_2)\n"
    "typedef double2 data_t;\n"
    "#else\n"
    "typedef double data_t;\n"
    "#endif\n"
    "#else \n"
    "#ifdef VECTOR_4\n"
    "typedef float4 data_t;\n"
    "#elif defined(VECTOR_3)\n"
    "typedef float3 data_t;\n"
    "#elif defined(VECTOR_2)\n"
    "typedef float2 data_t;\n"
    "#else\n"
    "typedef float data_t;\n"
    "#endif\n"
    "#endif\n"
    "\n"
    "// This kernel is optimized to ensure all global reads and writes are coalesced,\n"
    "// and to avoid bank conflicts in shared memory.  This kernel is up to 11x faster\n"
    "// than the naive kernel below.  Note that the shared memory array is sized to \n"
    "// (blockDim+1)*blockDim.  This pads each row of the 2D block in shared memory \n"
    "// so that bank conflicts do not occur when threads address the array column-wise.\n"
    "\n"
    "__kernel void transpose(__global data_t *odata, __global data_t *idata, int width, int height, __local data_t* block, uint blockDim) {\n"
    "    // read the matrix tile into shared memory\n"
    "    unsigned int xIndex = get_global_id(0);\n"
    "    unsigned int yIndex = get_global_id(1);\n"
    "\n"
    "    if ((xIndex < width) && (yIndex < height)) {\n"
    "        unsigned int index_in = yIndex * width + xIndex;\n"
    "        block[get_local_id(1)*(blockDim + 1) + get_local_id(0)] = idata[index_in];\n"
    "    }\n"
    "\n"
    "    barrier(CLK_LOCAL_MEM_FENCE);\n"
    "\n"
    "    // write the transposed matrix tile to global memory\n"
    "    xIndex = get_group_id(1) * blockDim + get_local_id(0);\n"
    "    yIndex = get_group_id(0) * blockDim + get_local_id(1);\n"
    "    if ((xIndex < height) && (yIndex < width)) {\n"
    "        unsigned int index_out = yIndex * height + xIndex;\n"
    "        odata[index_out] = block[get_local_id(0)*(blockDim + 1) + get_local_id(1)];\n"
    "    }\n"
    "}\n"
;
