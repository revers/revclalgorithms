#ifdef FAKE_INCLUDE
#include <FAKE_INCLUDE/opencl_fake.hxx>
#endif

#ifdef DOUBLE_TYPE
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#pragma OPENCL EXTENSION cl_amd_fp64 : enable
#ifdef VECTOR_4
typedef double4 data_t;
#elif defined(VECTOR_3)
typedef double3 data_t;
#elif defined(VECTOR_2)
typedef double2 data_t;
#else
typedef double data_t;
#endif
#else 
#ifdef VECTOR_4
typedef float4 data_t;
#elif defined(VECTOR_3)
typedef float3 data_t;
#elif defined(VECTOR_2)
typedef float2 data_t;
#else
typedef float data_t;
#endif
#endif

// This kernel is optimized to ensure all global reads and writes are coalesced,
// and to avoid bank conflicts in shared memory.  This kernel is up to 11x faster
// than the naive kernel below.  Note that the shared memory array is sized to 
// (blockDim+1)*blockDim.  This pads each row of the 2D block in shared memory 
// so that bank conflicts do not occur when threads address the array column-wise.

__kernel void transpose(__global data_t *odata, __global data_t *idata, int width,
		int height, __local data_t* block, uint blockDim) {
	// read the matrix tile into shared memory
	unsigned int xIndex = get_global_id(0);
	unsigned int yIndex = get_global_id(1);

	if ((xIndex < width) && (yIndex < height)) {
		unsigned int index_in = yIndex * width + xIndex;
		block[get_local_id(1) * (blockDim + 1) + get_local_id(0)] = idata[index_in];
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	// write the transposed matrix tile to global memory
	xIndex = get_group_id(1) * blockDim + get_local_id(0);
	yIndex = get_group_id(0) * blockDim + get_local_id(1);
	if ((xIndex < height) && (yIndex < width)) {
		unsigned int index_out = yIndex * height + xIndex;
		odata[index_out] =
				block[get_local_id(0) * (blockDim + 1) + get_local_id(1)];
	}
}
