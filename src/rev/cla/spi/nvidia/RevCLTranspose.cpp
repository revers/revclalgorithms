/* 
 * File:   RevCLTranspose.cpp
 * Author: Revers
 * 
 * Created on 19 sierpień 2012, 21:52
 */

#include "RevCLTranspose.h"

using namespace std;
using namespace rev;
using namespace rev::nvidia;

#define REV_NV_BLOCK_DIM 16

/**
 * @Override
 */
bool CLTranspose::init() {

    CLAOption enabledOpt = getEnabledOptions();

    std::string kernelFlags;
    CLAOption type = enabledOpt.getFirstOfGroup(ICLAlgorithm::GROUP_TYPES);
    assert(type.isEmpty() == false);

    CLAOption vectorType = enabledOpt.getFirstOfGroup(
            ICLAlgorithm::GROUP_VECTORS);
    assert(vectorType.isEmpty() == false);

    kernelFlags = string("-D") + type.getName();
    kernelFlags += string(" -D") + vectorType.getName();

    int typeSize = ICLAlgorithm::getTypeSize(type);
    int vecSize = ICLAlgorithm::getVectorSize(vectorType);
    cellSize = typeSize * vecSize;

#ifdef REVCLA_USE_FAST_MATH
    string programFlags = "-cl-fast-relaxed-math";
#else
    string programFlags = "";
#endif
    program.setFlags(programFlags + " " + kernelFlags);

    REVCLA_TRACE(
            "Compiling Transpose.cl (" << getSPI() << ") file with flags: " << program.getFlags());

    if (!program.compileFromString(clTransposeSource)) {
        REVCLA_ERR_MSG(
                "Failed to compile Transpose CL code for SPI: " << getSPI());
        return false;
    }

    cl_int err;
    transposeKernel = cl::Kernel(program(), "transpose", &err);
    clAssert(err);

    if (err != CL_SUCCESS) {
        return false;
    }

    localMemSize = cellSize * REV_NV_BLOCK_DIM * (REV_NV_BLOCK_DIM + 1);
    err = transposeKernel.setArg(TransposeKernel::ARG_BLOCK, localMemSize, NULL);
    clAssert(err);

    err = transposeKernel.setArg(TransposeKernel::ARG_BLOCK_DIM, REV_NV_BLOCK_DIM);
    clAssert(err);

    return true;
}

/**
 * @Override
 */
void CLTranspose::transpose(rev::CLBuffer& src, rev::CLBuffer& dest, int width,
        int height, int localWidth, int localHeight) {

    assertMsg(width * height * cellSize <= src.getSize(),
            "ASSERTION ERROR: " << rev::a2s(R(width), R(height), R(cellSize), R(src.getSize())));
    assertMsg(width * height * cellSize <= dest.getSize(),
            "ASSERTION ERROR: " << rev::a2s(R(width), R(height), R(cellSize), R(dest.getSize())));

    cl_int err;
    err = transposeKernel.setArg(TransposeKernel::ARG_IN_DATA, src());
    clAssert(err);
    err = transposeKernel.setArg(TransposeKernel::ARG_OUT_DATA, dest());
    clAssert(err);
    err = transposeKernel.setArg(TransposeKernel::ARG_WIDTH, width);
    clAssert(err);
    err = transposeKernel.setArg(TransposeKernel::ARG_HEIGHT, height);
    clAssert(err);

    assertMsgNoExit(width % localWidth == 0 && height % localWidth == 0 &&
            width >= localWidth && height >= localHeight,
            "ASSERTION ERROR: " << rev::a2s(R(width), R(height), R(localWidth), R(localHeight)));

    cl::CommandQueue& queue = CLContext::get().commandQueue();
    err = queue.enqueueNDRangeKernel(transposeKernel, cl::NullRange,
            cl::NDRange(width, height), cl::NDRange(localWidth, localHeight), NULL,
            NULL);

    clAssert(err);
}

bool CLTransposeFactory::areOptionsSupported(CLAOption o) {
    o.reduce();

    static const CLAOption SUPPORTED_OPTIONS = ICLAlgorithm::OPTION_TYPE_DOUBLE
            | ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_TYPE_INT
            | ICLAlgorithm::OPTION_TYPE_UINT | ICLAlgorithm::OPTION_VECTOR_1
            | ICLAlgorithm::OPTION_VECTOR_2
            | ICLAlgorithm::OPTION_VECTOR_4;

    return SUPPORTED_OPTIONS.contains(o);
}

/**
 * @Override
 */
CLTransposeFactory::algorithm_t_ptr CLTransposeFactory::create() {

    REVCLA_TRACE("Creating Default Transpose Factory for SPI: " << getSPI());

    CLTransposeFactory::algorithm_t_ptr ptr(new CLTranspose(CLAOption()));
    REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
    return ptr;
}

/**
 * @Override
 */
CLTransposeFactory::algorithm_t_ptr CLTransposeFactory::create(CLAOption options) {
    options.reduce();

    if (!areOptionsSupported(options)) {
        REVCLA_ERR_MSG("ERROR: Cannot cretae Transpose "
                "Factory for SPI: " << getSPI() << " with requested options: " << options);

        return nullptr;
    }

    REVCLA_TRACE(
            "\nCreating Transpose Factory for SPI: " << getSPI() << " with options: " << options);

    CLTransposeFactory::algorithm_t_ptr ptr(new CLTranspose(options));
    REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
    return ptr;
}

// ---------------------------------------------------------------------------------------------------------

