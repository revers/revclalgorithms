#ifdef FAKE_INCLUDE
#include <FAKE_INCLUDE/opencl_fake.hxx>
#endif

#ifdef DOUBLE_TYPE
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#pragma OPENCL EXTENSION cl_amd_fp64 : enable
#ifdef VECTOR_4
typedef double4 data_t;
#elif defined(VECTOR_3)
typedef double3 data_t;
#elif defined(VECTOR_2)
typedef double2 data_t;
#else
typedef double data_t;
#endif
#else 
#ifdef VECTOR_4
typedef float4 data_t;
#elif defined(VECTOR_3)
typedef float3 data_t;
#elif defined(VECTOR_2)
typedef float2 data_t;
#else
typedef float data_t;
#endif
#endif

__kernel void transpose(
        __global data_t* inData,
        __global data_t* outData, 
        uint width, uint height) {
    
    uint xIndex = get_global_id(0);
    uint yIndex = get_global_id(1);

    if (xIndex < width && yIndex < height) {
        uint index_in = xIndex + width * yIndex;
        uint index_out = yIndex + height * xIndex;
        outData[index_out] = inData[index_in];
    }
}