/*
 * RevCLFFTInPlace.cpp
 *
 *  Created on: 14-09-2012
 *      Author: Revers
 */

#include <iostream>

#include "RevCLFFTInPlace.h"
#include "fft_internal.h"

using namespace std;

using namespace rev;
using namespace rev::apple;

/**
 * @Override
 */
bool CLFFTInPlace::init(int width,
        clFFT_DataFormat format /* = clFFT_InterleavedComplexFormat */) {

    baseTypeSize = sizeof(float);

    dataFormat = format;
    batchSize = 1;
    dim = clFFT_1D;
    n = {(unsigned int) width, 1, 1};

    rev::CLContext& ctx = rev::CLContext::get();
    gMemSize = ctx.globalMemorySize();
    gMemSize /= (1024 * 1024);

    if (!checkMemRequirements(n, batchSize, testType, gMemSize)) {
        return false;
    }

    cl_int err;
    plan = clFFT_CreatePlan(ctx.context()(), n, dim, dataFormat, &err, 0);
    clAssert(err);

    if (plan == NULL || err != CL_SUCCESS) {
        return false;
    }
//    else {
//        clFFT_DumpPlan(plan, stdout);
//    }

    return true;
}

bool CLFFTInPlace::checkMemRequirements(clFFT_Dim3 n, int batchSize,
        clFFT_TestType testType, cl_ulong gMemSize) {
    cl_ulong memReq = (testType == clFFT_OUT_OF_PLACE) ? 3 : 2;
    memReq *= n.x * n.y * n.z * sizeof(clFFT_Complex) * batchSize;
    memReq = memReq / 1024 / 1024;

    if (memReq >= gMemSize) {
        assertMsg(memReq >= gMemSize,
                "ERROR: memory not sufficient: " << rev::a2s(R(memReq), R(gMemSize), R(n), R(testType), R(batchSize)));

        return false;
    }

    return true;
}

bool CLFFTInPlaceFactory::areOptionsSupported(CLAOption o) {
    o.reduce();

    static const CLAOption SUPPORTED_OPTIONS = ICLAlgorithm::OPTION_TYPE_FLOAT
            | ICLAlgorithm::OPTION_ARR_INTERLEAVED | ICLAlgorithm::OPTION_ARR_FLAT
            | ICLAlgorithm::OPTION_SCALED_FALSE;

    return SUPPORTED_OPTIONS.contains(o);
}

/**
 * @Override
 */
CLFFTInPlaceFactory::algorithm_t_ptr CLFFTInPlaceFactory::create() {

    REVCLA_TRACE("Creating Default FFTInPlace Factory for SPI: " << getSPI());

    CLFFTInPlaceFactory::algorithm_t_ptr ptr(new CLFFTInPlace(CLAOption()));
    REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
    return ptr;
}

/**
 * @Override
 */
CLFFTInPlaceFactory::algorithm_t_ptr CLFFTInPlaceFactory::create(
        CLAOption options) {
    options.reduce();

    if (!areOptionsSupported(options)) {
        REVCLA_ERR_MSG("ERROR: Cannot cretae FFTInPlace "
        "Factory for SPI: " << getSPI() << " with requested options: " << options);

        return nullptr;
    }

    REVCLA_TRACE(
            "\nCreating FFTInPlace Factory for SPI: " << getSPI() << " with options: " << options);

    CLFFTInPlaceFactory::algorithm_t_ptr ptr(new CLFFTInPlace(options));
    REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
    return ptr;
}

void CLFFTInPlace::fft(rev::CLBuffer& inOutBuffer, bool inverse) {

    assertMsg(
            (ICLFFTInPlace::width != 0) && !(ICLFFTInPlace::width & (ICLFFTInPlace::width - 1)),
            "Error: Size '" << ICLFFTInPlace::width << "' is not power of 2!!");

    ((cl_fft_plan *) plan)->buffer_offset = ICLFFTInPlace::bufferOffset;

    if (dataFormat == clFFT_InterleavedComplexFormat) {
        cl_mem mem = inOutBuffer.getBuffer()();
        cl_int err = clFFT_ExecuteInterleaved(
                rev::CLContext::get().commandQueue()(), plan, batchSize,
                inverse ? clFFT_Inverse : clFFT_Forward, mem, mem, 0, NULL, NULL);
        clAssert(err);
    } else { // dataFormat == clFFT_SplitComplexFormat
        cl_mem mem = inOutBuffer.getBuffer()();

        int halfSize = baseTypeSize * ICLFFTInPlace::width;
        cl_buffer_region reRegion = { 0, (size_t) halfSize };
        cl_buffer_region imRegion = { (size_t) halfSize, (size_t) halfSize };

        cl_int err;
        cl_mem data_in_real = clCreateSubBuffer(mem, CL_MEM_READ_WRITE,
                CL_BUFFER_CREATE_TYPE_REGION, &reRegion, &err);
        clAssert(err);

        cl_mem data_in_imag = clCreateSubBuffer(mem, CL_MEM_READ_WRITE,
                CL_BUFFER_CREATE_TYPE_REGION, &imRegion, &err);
        clAssert(err);

        cl_mem data_out_real = data_in_real;
        cl_mem data_out_imag = data_in_imag;

        err = clFFT_ExecutePlannar(rev::CLContext::get().commandQueue()(), plan,
                batchSize, inverse ? clFFT_Inverse : clFFT_Forward, data_in_real,
                data_in_imag, data_out_real, data_out_imag, 0, NULL, NULL);
        clAssert(err);
    }
}

