/*
 * clFFTCommon.h
 *
 *  Created on: 15-09-2012
 *      Author: Revers
 */

#ifndef CLFFTCOMMON_H_
#define CLFFTCOMMON_H_

#include <ostream>
#include "clFFT.h"

namespace rev {
	namespace apple {

		typedef enum {
			clFFT_OUT_OF_PLACE, clFFT_IN_PLACE,
		} clFFT_TestType;

		typedef struct {
			double real;
			double imag;
		} clFFT_ComplexDouble;

		typedef struct {
			double *real;
			double *imag;
		} clFFT_SplitComplexDouble;

		typedef unsigned long long ulong;

		std::ostream& operator<<(std::ostream& out, const clFFT_TestType& val);

		std::ostream& operator<<(std::ostream& out, const clFFT_Dim3& val);

	} /* namespace apple */
} /* namespace rev */
#endif /* CLFFTCOMMON_H_ */
