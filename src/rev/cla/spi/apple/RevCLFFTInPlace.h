/*
 * RevCLFFTInPlace.h
 *
 *  Created on: 14-09-2012
 *      Author: Revers
 */

#ifndef REVCLFFTINPLACE_APPLE_H_
#define REVCLFFTINPLACE_APPLE_H_

#include <string>
#include <ostream>

#include <rev/common/RevAssert.h>
#include <rev/cl/RevCLProgram.h>
#include <rev/cl/RevCLContext.h>
#include <rev/cl/RevCLBuffer.h>
#include <rev/cl/RevCLAssert.h>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/api/RevICLFFTInPlace.h>
#include <rev/cla/RevICLAlgorithmFactory.hpp>
#include <rev/cla/RevCLAOption.h>

#include "clFFT.h"
#include "clFFTCommon.h"

namespace rev {
    namespace apple {

        class REVCLA_API CLFFTInPlace: public rev::ICLFFTInPlace {
            int localMemSize = 0;

            cl_ulong gMemSize;
            //clFFT_Direction dir; // = clFFT_Forward;
            //int numIter = 1;
            clFFT_Dim3 n; // = {1024, 1, 1};
            int batchSize; // = 1;
            clFFT_DataFormat dataFormat; // = clFFT_SplitComplexFormat;
            clFFT_Dimension dim; // = clFFT_1D;
            clFFT_TestType testType = clFFT_IN_PLACE;
            clFFT_Plan plan = NULL;

            CLFFTInPlace(const CLFFTInPlace& orig);
            CLFFTInPlace& operator=(const CLFFTInPlace& orig);

            int baseTypeSize = 0;
        public:

            CLFFTInPlace(const CLAOption& options) :
                    ICLFFTInPlace(options) {
            }

            ~CLFFTInPlace() {
                if (plan) {
                    clFFT_DestroyPlan(plan);
                }
            }
            /**
             * @Override
             */
            bool init(int width) {
                ICLFFTInPlace::width = width;
                CLAOption options = getEnabledOptions();
                if (options.contains(ICLAlgorithm::OPTION_ARR_FLAT)) {
                    return init(width, clFFT_SplitComplexFormat);
                } else { // ICLAlgorithm::OPTION_ARR_INTERLEAVED
                    return init(width, clFFT_InterleavedComplexFormat);
                }
            }

            /**
             * @Override
             */
            const char* getSPI() {
                return REVCLA_SPI_APPLE;
            }

            /**
             * @Override
             */
            virtual int getMinimalLocalMemorySize() {
                return localMemSize;
            }

            /**
             * @Override
             */
            void fft(rev::CLBuffer& inOutBuffer, bool inverse);

            /**
             * Override
             */
            virtual CLAOption getDefaultOptions() {
                return ICLAlgorithm::OPTION_TYPE_FLOAT
                        | ICLAlgorithm::OPTION_ARR_INTERLEAVED
                        | ICLAlgorithm::OPTION_SCALED_FALSE;
            }

        private:
            bool init(int width, clFFT_DataFormat format =
                    clFFT_InterleavedComplexFormat);

            bool checkMemRequirements(clFFT_Dim3 n, int batchSize,
                    clFFT_TestType testType, cl_ulong gMemSize);

        };

        class REVCLA_API CLFFTInPlaceFactory: public rev::ICLAlgorithmFactory<
                rev::ICLFFTInPlace> {

            /**
             * @Override
             */
            const char* getSPI() {
                return REVCLA_SPI_APPLE;
            }

            /**
             * @Override
             */
            bool areOptionsSupported(CLAOption options);

            /**
             * @Override
             */
            algorithm_t_ptr create();

            /**
             * @Override
             */
            algorithm_t_ptr create(CLAOption options);

        };

    } /* namespace apple */
} /* namespace rev */
#endif /* REVCLFFTINPLACE_APPLE_H_ */
