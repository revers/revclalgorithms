/*
 * RevCLFFT2DInPlace.cpp
 *
 *  Created on: 25-09-2012
 *      Author: Revers
 */

#include <iostream>

#include "RevCLFFT2DInPlace.h"
#include "fft_internal.h"

using namespace std;

using namespace rev;
using namespace rev::apple;

/**
 * @Override
 */
bool CLFFT2DInPlace::init(int width, int height,
        clFFT_DataFormat format /* = clFFT_InterleavedComplexFormat */) {

    // for now only interleaved format is supported.
    assert(format == clFFT_InterleavedComplexFormat);

    baseTypeSize = sizeof(float);

    dataFormat = format;
    batchSize = 1;
    dim = clFFT_2D;
    n = {(unsigned int) width, (unsigned int) height, 1};

    rev::CLContext& ctx = rev::CLContext::get();
    gMemSize = ctx.globalMemorySize();
    gMemSize /= (1024 * 1024);

    if (!checkMemRequirements(n, batchSize, testType, gMemSize)) {
        return false;
    }

    cl_int err;
    plan = clFFT_CreatePlan(ctx.context()(), n, dim, dataFormat, &err, 0);
    clAssert(err);

    if (plan == NULL || err != CL_SUCCESS) {
        return false;
    }
//    else {
//        clFFT_DumpPlan(plan, stdout);
//    }

    return true;
}

bool CLFFT2DInPlace::checkMemRequirements(clFFT_Dim3 n, int batchSize,
        clFFT_TestType testType, cl_ulong gMemSize) {
    cl_ulong memReq = (testType == clFFT_OUT_OF_PLACE) ? 3 : 2;
    memReq *= n.x * n.y * n.z * sizeof(clFFT_Complex) * batchSize;
    memReq = memReq / 1024 / 1024;

    if (memReq >= gMemSize) {
        assertMsg(memReq >= gMemSize,
                "ERROR: memory not sufficient: " << rev::a2s(R(memReq), R(gMemSize), R(n), R(testType), R(batchSize)));

        return false;
    }

    return true;
}

bool CLFFT2DInPlaceFactory::areOptionsSupported(CLAOption o) {
    o.reduce();

    static const CLAOption SUPPORTED_OPTIONS = ICLAlgorithm::OPTION_TYPE_FLOAT
            | ICLAlgorithm::OPTION_ARR_INTERLEAVED
            | ICLAlgorithm::OPTION_SCALED_FALSE;

    return SUPPORTED_OPTIONS.contains(o);
}

/**
 * @Override
 */
CLFFT2DInPlaceFactory::algorithm_t_ptr CLFFT2DInPlaceFactory::create() {

    REVCLA_TRACE("Creating Default FFT2DInPlace Factory for SPI: " << getSPI());

    CLFFT2DInPlaceFactory::algorithm_t_ptr ptr(new CLFFT2DInPlace(CLAOption()));
    REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
    return ptr;
}

/**
 * @Override
 */
CLFFT2DInPlaceFactory::algorithm_t_ptr CLFFT2DInPlaceFactory::create(
        CLAOption options) {
    options.reduce();

    if (!areOptionsSupported(options)) {
        REVCLA_ERR_MSG("ERROR: Cannot cretae FFT2DInPlace "
        "Factory for SPI: " << getSPI() << " with requested options: " << options);

        return nullptr;
    }

    REVCLA_TRACE(
            "\nCreating FFT2DInPlace Factory for SPI: " << getSPI() << " with options: " << options);

    CLFFT2DInPlaceFactory::algorithm_t_ptr ptr(new CLFFT2DInPlace(options));
    REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
    return ptr;
}

void CLFFT2DInPlace::fft(rev::CLBuffer& inOutBuffer, bool inverse) {

    assertMsg( (width != 0) && !(width & (width - 1)),
            "Error: Size '" << width << "' is not power of 2!!");
    assertMsg( (height != 0) && !(height & (height - 1)),
            "Error: Size '" << height << "' is not power of 2!!");

    ((cl_fft_plan *) plan)->buffer_offset = bufferOffset;

//  if (dataFormat == clFFT_InterleavedComplexFormat) {
    cl_mem mem = inOutBuffer.getBuffer()();
    cl_int err = clFFT_ExecuteInterleaved(rev::CLContext::get().commandQueue()(),
            plan, batchSize, inverse ? clFFT_Inverse : clFFT_Forward, mem, mem, 0,
            NULL, NULL);
    clAssert(err);
//  } else { // dataFormat == clFFT_SplitComplexFormat
//      cl_mem mem = inOutBuffer.getBuffer()();
//
//      int halfSize = baseTypeSize * width;
//      cl_buffer_region reRegion = { 0, halfSize };
//      cl_buffer_region imRegion = { halfSize, halfSize };
//
//      cl_int err;
//      cl_mem data_in_real = clCreateSubBuffer(mem, CL_MEM_READ_WRITE,
//              CL_BUFFER_CREATE_TYPE_REGION, &reRegion, &err);
//      clAssert(err);
//
//      cl_mem data_in_imag = clCreateSubBuffer(mem, CL_MEM_READ_WRITE,
//              CL_BUFFER_CREATE_TYPE_REGION, &imRegion, &err);
//      clAssert(err);
//
//      cl_mem data_out_real = data_in_real;
//      cl_mem data_out_imag = data_in_imag;
//
//      err = clFFT_ExecutePlannar(rev::CLContext::get().commandQueue()(), plan,
//              batchSize, inverse ? clFFT_Inverse : clFFT_Forward, data_in_real,
//              data_in_imag, data_out_real, data_out_imag, 0, NULL, NULL);
//      clAssert(err);
//  }
}
