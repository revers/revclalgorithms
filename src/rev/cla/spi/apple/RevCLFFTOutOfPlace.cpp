/*
 * RevCLFFTOutOfPlace.cpp
 *
 *  Created on: 15-09-2012
 *      Author: Revers
 */

#include "RevCLFFTOutOfPlace.h"

using namespace rev;
using namespace rev::apple;

/**
 * @Override
 */
bool CLFFTOutOfPlace::init(int width,
        clFFT_DataFormat format /* = clFFT_InterleavedComplexFormat */) {
    baseTypeSize = sizeof(float);
    dataFormat = format;
    batchSize = 1;
    dim = clFFT_1D;
    n = { (size_t) width, 1, 1};

    rev::CLContext& ctx = rev::CLContext::get();
    gMemSize = ctx.globalMemorySize();
    gMemSize /= (1024 * 1024);

    if (!checkMemRequirements(n, batchSize, testType, gMemSize)) {
        return false;
    }

    cl_int err;
    plan = clFFT_CreatePlan(ctx.context()(), n, dim, dataFormat, &err, 0);
    clAssert(err);

    if (plan == NULL || err != CL_SUCCESS) {
        return false;
    } //else {
      //	clFFT_DumpPlan(plan, stdout);
      //}

    return true;
}

bool CLFFTOutOfPlace::checkMemRequirements(clFFT_Dim3 n, int batchSize,
        clFFT_TestType testType, cl_ulong gMemSize) {
    cl_ulong memReq = (testType == clFFT_OUT_OF_PLACE) ? 3 : 2;
    memReq *= n.x * n.y * n.z * sizeof(clFFT_Complex) * batchSize;
    memReq = memReq / 1024 / 1024;

    if (memReq >= gMemSize) {
        assertMsg(memReq >= gMemSize,
                "ERROR: memory not sufficient: " << rev::a2s(R(memReq), R(gMemSize), R(n), R(testType), R(batchSize)));

        return false;
    }

    return true;
}

bool CLFFTOutOfPlaceFactory::areOptionsSupported(CLAOption o) {
    o.reduce();

    static const CLAOption SUPPORTED_OPTIONS = ICLAlgorithm::OPTION_TYPE_FLOAT
            | ICLAlgorithm::OPTION_ARR_INTERLEAVED | ICLAlgorithm::OPTION_ARR_FLAT
            | ICLAlgorithm::OPTION_SCALED_FALSE;

    return SUPPORTED_OPTIONS.contains(o);
}

/**
 * @Override
 */
CLFFTOutOfPlaceFactory::algorithm_t_ptr CLFFTOutOfPlaceFactory::create() {

    REVCLA_TRACE("Creating Default FFTOutOfPlace Factory for SPI: " << getSPI());

    CLFFTOutOfPlaceFactory::algorithm_t_ptr ptr(new CLFFTOutOfPlace(CLAOption()));
    REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
    return ptr;
}

/**
 * @Override
 */
CLFFTOutOfPlaceFactory::algorithm_t_ptr CLFFTOutOfPlaceFactory::create(
        CLAOption options) {
    options.reduce();

    if (!areOptionsSupported(options)) {
        REVCLA_ERR_MSG("ERROR: Cannot cretae FFTOutOfPlace "
        "Factory for SPI: " << getSPI() << " with requested options: " << options);

        return nullptr;
    }

    REVCLA_TRACE(
            "\nCreating FFTOutOfPlace Factory for SPI: " << getSPI() << " with options: " << options);

    CLFFTOutOfPlaceFactory::algorithm_t_ptr ptr(new CLFFTOutOfPlace(options));
    REVCLA_TRACE("\tUSED OPTIONS: " << ptr->getEnabledOptions() << "\n");
    return ptr;
}

void CLFFTOutOfPlace::fft(rev::CLBuffer& inBuffer, rev::CLBuffer& outBuffer,
        bool inverse) {

    assertMsg(
            (ICLFFTOutOfPlace::width != 0) && !(ICLFFTOutOfPlace::width & (ICLFFTOutOfPlace::width - 1)),
            "Error: Size '" << ICLFFTOutOfPlace::width << "' is not power of 2!!");

    if (dataFormat == clFFT_InterleavedComplexFormat) {
        cl_mem inMem = inBuffer.getBuffer()();
        cl_mem outMem = outBuffer.getBuffer()();

        cl_int err = clFFT_ExecuteInterleaved(
                rev::CLContext::get().commandQueue()(), plan, batchSize,
                inverse ? clFFT_Inverse : clFFT_Forward, inMem, outMem, 0, NULL,
                NULL);
        clAssert(err);
    } else { // dataFormat == clFFT_SplitComplexFormat
        cl_mem inMem = inBuffer.getBuffer()();
        cl_mem outMem = outBuffer.getBuffer()();

        int halfSize = baseTypeSize * ICLFFTOutOfPlace::width;
        cl_buffer_region reRegion = { 0, (size_t) halfSize };
        cl_buffer_region imRegion = { (size_t) halfSize, (size_t) halfSize };

        cl_int err;
        cl_mem data_in_real = clCreateSubBuffer(inMem, CL_MEM_READ_WRITE,
                CL_BUFFER_CREATE_TYPE_REGION, &reRegion, &err);
        clAssert(err);

        cl_mem data_in_imag = clCreateSubBuffer(inMem, CL_MEM_READ_WRITE,
                CL_BUFFER_CREATE_TYPE_REGION, &imRegion, &err);
        clAssert(err);

        cl_mem data_out_real = clCreateSubBuffer(outMem, CL_MEM_READ_WRITE,
                CL_BUFFER_CREATE_TYPE_REGION, &reRegion, &err);
        clAssert(err);

        cl_mem data_out_imag = clCreateSubBuffer(outMem, CL_MEM_READ_WRITE,
                CL_BUFFER_CREATE_TYPE_REGION, &imRegion, &err);
        clAssert(err);

        err = clFFT_ExecutePlannar(rev::CLContext::get().commandQueue()(), plan,
                batchSize, inverse ? clFFT_Inverse : clFFT_Forward, data_in_real,
                data_in_imag, data_out_real, data_out_imag, 0, NULL, NULL);

        clAssert(err);
    }

}
