/* 
 * File:   RevICLAlgorithm.cpp
 * Author: Revers
 * 
 * Created on 20 sierpień 2012, 21:46
 */

#include <rev/cla/RevICLAlgorithm.h>

using namespace rev;

const std::string ICLAlgorithm::GROUP_TYPES("TYPES");
const std::string ICLAlgorithm::GROUP_VECTORS("VECTORS");
const std::string ICLAlgorithm::GROUP_ARRANGMENT("ARRANGMENT");
const std::string ICLAlgorithm::GROUP_SCALED("SCALED");

const CLAOption ICLAlgorithm::OPTION_SCALED_TRUE("SCALED_TRUE", ICLAlgorithm::GROUP_SCALED);
const CLAOption ICLAlgorithm::OPTION_SCALED_FALSE("SCALED_FALSE", ICLAlgorithm::GROUP_SCALED);

const CLAOption ICLAlgorithm::OPTION_TYPE_UINT("UINT_TYPE", ICLAlgorithm::GROUP_TYPES);
const CLAOption ICLAlgorithm::OPTION_TYPE_INT("INT_TYPE", ICLAlgorithm::GROUP_TYPES);
const CLAOption ICLAlgorithm::OPTION_TYPE_FLOAT("FLOAT_TYPE", ICLAlgorithm::GROUP_TYPES);
const CLAOption ICLAlgorithm::OPTION_TYPE_DOUBLE("DOUBLE_TYPE", ICLAlgorithm::GROUP_TYPES);

const CLAOption ICLAlgorithm::OPTION_VECTOR_1("VECTOR_1", ICLAlgorithm::GROUP_VECTORS);
const CLAOption ICLAlgorithm::OPTION_VECTOR_2("VECTOR_2", ICLAlgorithm::GROUP_VECTORS);
const CLAOption ICLAlgorithm::OPTION_VECTOR_3("VECTOR_3", ICLAlgorithm::GROUP_VECTORS);
const CLAOption ICLAlgorithm::OPTION_VECTOR_4("VECTOR_4", ICLAlgorithm::GROUP_VECTORS);

const CLAOption ICLAlgorithm::OPTION_ARR_INTERLEAVED("ARR_INTERLEAVED", ICLAlgorithm::GROUP_ARRANGMENT);
const CLAOption ICLAlgorithm::OPTION_ARR_FLAT("ARR_FLAT", ICLAlgorithm::GROUP_ARRANGMENT);
