/* 
 * File:   XMLFlag.h
 * Author: Revers
 *
 * Created on 3 sierpień 2012, 21:10
 */

#ifndef REVXMLFLAG_H
#define	REVXMLFLAG_H

#include <string>
#include <ostream>

#include <rev/cla/RevCLAConfig.h>
#include <rev/common/xml/RevXMLElement.h>

namespace rev {
    
    class REVCLA_API XMLFlag : public rev::XMLElement {
        typedef unsigned int flag_t;
        
        std::string name;
        flag_t value;
    public:

        REV_DECLARE_XML_FACTORY(XMLFlag, "flag")

        XMLFlag() {
            value = 0;
            initXMLElement();
        }

        XMLFlag(const char* name_, flag_t value_)
        : name(name_), value(value_) {
            initXMLElement();
        }

        /**
         * @Override
         */
        void mapXMLTagsToMembers() {
            mapAttribute(&value, "value");
            mapAttribute(&name, "name");
        }

        const std::string& getName() const {
            return name;
        }

        void setName(const char* name) {
            this->name = name;
        }

        flag_t getValue() const {
            return value;
        }

        void setValue(flag_t value) {
            this->value = value;
        }

        friend std::ostream& operator<<(std::ostream &out, const XMLFlag& xml) {
            out << "XMLFlag[name=\"" << xml.name << "\", value=" << xml.value << "]";

            return out;
        }
    };
}




#endif	/* REVXMLFLAG_H */

