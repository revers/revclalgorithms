/* 
 * File:   XMLAlgorithm.h
 * Author: Revers
 *
 * Created on 3 sierpień 2012, 21:36
 */

#ifndef REVXMLALGORITHM_H
#define	REVXMLALGORITHM_H

#include <string>
#include <list>
#include <ostream>

#include <rev/cla/RevCLAConfig.h>
#include <rev/common/xml/RevXMLElement.h>

#include "RevXMLFlag.h"

namespace rev {

    class REVCLA_API XMLAlgorithm : public rev::XMLElement {
        typedef std::list< rev::XMLFlag* > FlagList;

        std::string name;
        std::string spi;

        FlagList flagList;
    public:
        
        REV_DECLARE_XML_FACTORY(XMLAlgorithm, "algorithm")
        
        /**
         * @Override
         */
        void mapXMLTagsToMembers() {
            mapAttribute(&name, "name");
            mapAttribute(&spi, "spi");
            mapElement(&flagList, XMLFlag::getStaticElementName());
        }

        XMLAlgorithm() {
            initXMLElement();
        }
        
        XMLAlgorithm(const char* name_, const char* spi_)
        : name(name_), spi(spi_) {
            initXMLElement();
        }

        ~XMLAlgorithm() {
            for (auto fPtr : flagList) {
                delete fPtr;
            }
        }

        const std::string& getName() const {
            return name;
        }

        void setName(const char* name) {
            this->name = name;
        }

        const std::string& getSpi() const {
            return spi;
        }

        void setSpi(const char* spi) {
            this->spi = spi;
        }

        FlagList& getFlagList() {
            return flagList;
        }

        void addFlag(rev::XMLFlag* flag) {
            flagList.push_back(flag);
        }
        
        friend std::ostream& operator<<(std::ostream &out, const XMLAlgorithm& xml) {
            out << "XMLAlgorithm[name=\"" << xml.name << "\", spi=\"" << xml.spi
                    << "\", flagList=[";
            
            if(!xml.flagList.empty()) {
                for(auto flag : xml.flagList) {
                    out << *flag << ", ";
                }
            }
            out << "] ]";            
            return out;
        }
    };
}
#endif	/* REVXMLALGORITHM_H */

