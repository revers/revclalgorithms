/* 
 * File:   RevXMLAlgorithms.h
 * Author: Revers
 *
 * Created on 3 sierpień 2012, 21:44
 */

#ifndef REVXMLALGORITHMS_H
#define	REVXMLALGORITHMS_H

#include <list>

#include <rev/cla/RevCLAConfig.h>
#include <rev/common/xml/RevXMLElement.h>

#include "RevXMLAlgorithm.h"

namespace rev {

    class REVCLA_API XMLAlgorithms : public rev::XMLElement {
        typedef std::list< rev::XMLAlgorithm* > AlgorithmList;

        AlgorithmList algorithmList;
    public:
        
        REV_DECLARE_XML_FACTORY(XMLAlgorithms, "algorithms")
        
        /**
         * @Override
         */
        void mapXMLTagsToMembers() {
            mapElement(&algorithmList, XMLAlgorithm::getStaticElementName());
        }

        XMLAlgorithms() {
            initXMLElement();
        }

        ~XMLAlgorithms() {
            for (auto aPtr : algorithmList) {
                delete aPtr;
            }
        }

        AlgorithmList& getAlgorithmList() {
            return algorithmList;
        }

        void addAlgorithm(rev::XMLAlgorithm* algo) {
            algorithmList.push_back(algo);
        }

        friend std::ostream& operator<<(std::ostream &out, const XMLAlgorithms& xml) {
            out << "XMLAlgorithms[algorithmList=[";

            if (!xml.algorithmList.empty()) {
                
                for (auto a : xml.algorithmList) {
                    out << *a << ", ";
                }
            }

            out << "] ]";
            return out;
        }
    };
}

#endif	/* REVXMLALGORITHMS_H */

