/* 
 * File:   RevICLAlgorithm.h
 * Author: Revers
 *
 * Created on 20 sierpień 2012, 21:46
 *
 */

#ifndef REVICLALGORITHM_H
#define	REVICLALGORITHM_H

#include <iostream>
#include <string>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/RevCLAOption.h>

#include <rev/common/RevAssert.h>

namespace rev {

    class REVCLA_API ICLAlgorithm {
    private:
        CLAOption enabledOptions;
    public:
        static const std::string GROUP_TYPES;
        static const std::string GROUP_VECTORS;
        /**
         * Components arrangment
         */
        static const std::string GROUP_ARRANGMENT;
        /**
         * Is FFT Scaled?
         */
        static const std::string GROUP_SCALED;
        /**
         * Inverse 1D FFT scaled. (Or forward 2D FFT scaled).
         */
        static const CLAOption OPTION_SCALED_TRUE;

        /**
         * Inverse 1D FFT not scaled. (Or forward 2D FFT not scaled).
         */
        static const CLAOption OPTION_SCALED_FALSE;

        static const CLAOption OPTION_TYPE_UINT;
        static const CLAOption OPTION_TYPE_INT;
        static const CLAOption OPTION_TYPE_FLOAT;
        static const CLAOption OPTION_TYPE_DOUBLE;

        static const CLAOption OPTION_VECTOR_1;
        static const CLAOption OPTION_VECTOR_2;
        static const CLAOption OPTION_VECTOR_3;
        static const CLAOption OPTION_VECTOR_4;

        /**
         * Option representing interleaved data arrangement,
         *  for exemaple for complex numbers it would be:
         *
         *	[x0.re, x0.im, x1.re, x1.im, ..., xn.re, xn.im].
         */
        static const CLAOption OPTION_ARR_INTERLEAVED;

        /**
         * Option representing flat data arrangement,
         * for exemaple for complex numbers it would be:
         * [ x0.re, x1.re, ..., xn.re,
         *   x0.im, x1.im, ..., xn.im ].
         */
        static const CLAOption OPTION_ARR_FLAT;

    public:

        ICLAlgorithm() {
        }

        ICLAlgorithm(CLAOption enabledOptions_) :
                enabledOptions(enabledOptions_) {
        }

        virtual ~ICLAlgorithm() {
        }

    public:
        virtual const char* getName() = 0;
        virtual const char* getSPI() = 0;
        virtual CLAOption getDefaultOptions() = 0;

        virtual CLAOption getEnabledOptions() {
            return (enabledOptions | getDefaultOptions()).reduce();
        }

        /**
         * Additional memory size used by the algorithm.
         * This method is valid only after init().
         */
        virtual int getAdditionalGlobalMemorySize() {
            return 0;
        }

        /**
         * Minimal local memory size needed by the algorithm.
         * This method is valid only after init().
         */
        virtual int getMinimalLocalMemorySize() {
            return 0;
        }

    public:
        static int getVectorSize(const CLAOption& vecOption) {
            assertMsg(vecOption.getGroup() == GROUP_VECTORS,
                    "Wrong group: " << R(vecOption));

            if (vecOption.getName() == OPTION_VECTOR_1.getName()) {
                return 1;
            } else if (vecOption.getName() == OPTION_VECTOR_2.getName()) {
                return 2;
            } else if (vecOption.getName() == OPTION_VECTOR_3.getName()) {
                return 3;
            } else if (vecOption.getName() == OPTION_VECTOR_4.getName()) {
                return 4;
            }

            assertMsg(false, "Unknown vector's type: " << R(vecOption));

            return 0;
        }

        static int getTypeSize(const CLAOption& typeOption) {
            assertMsg(typeOption.getGroup() == GROUP_TYPES,
                    "Wrong group: " << R(typeOption));

            if (typeOption.getName() == OPTION_TYPE_UINT.getName()) {
                return sizeof(unsigned int);
            } else if (typeOption.getName() == OPTION_TYPE_INT.getName()) {
                return sizeof(int);
            } else if (typeOption.getName() == OPTION_TYPE_FLOAT.getName()) {
                return sizeof(float);
            } else if (typeOption.getName() == OPTION_TYPE_DOUBLE.getName()) {
                return sizeof(double);
            }

            assertMsg(false, "Unknown type's name: " << R(typeOption));

            return 0;
        }
    };
} // end namespace rev

#endif	/* REVICLALGORITHM_H */

