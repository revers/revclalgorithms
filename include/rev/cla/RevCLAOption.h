/* 
 * File:   RevCLAOption.h
 * Author: Kamil
 *
 * Created on 21 sierpień 2012, 10:57
 */

#ifndef REVCLAOPTION_H
#define	REVCLAOPTION_H

#include <rev/cla/RevCLAConfig.h>
#include <ostream>
#include <string>

namespace rev {

	class REVCLA_API CLAOption {
		std::string name;
		std::string group;
		CLAOption* next;

		CLAOption(const CLAOption& current, const CLAOption& next);

		bool containsSingleOption(const CLAOption& other) const;
	public:

		CLAOption() :
				name(""), group(""), next(NULL) {
		}

		CLAOption(const std::string& name_, const std::string& group_) :
				name(name_), group(group_), next(NULL) {
		}

		CLAOption(const CLAOption& other);

		~CLAOption() {
			if (next) {
				delete next;
			}
		}

		const std::string& getName() const {
			return name;
		}

		const std::string& getGroup() const {
			return group;
		}

		/**
		 * Removes empty options (if it is not the only one [eg. next = NULL]).
		 * Removes options with duplicated groups. Group set must be unique.
		 */
		CLAOption& reduce();

		int getCount() const;

		bool contains(const std::string& other) const;

		bool contains(const CLAOption& others) const;

		bool hasNext() const {
			return next != NULL;
		}

		CLAOption* getNext();

		CLAOption* getLast();

		bool isEmpty() const {
			return name == "";
		}

		CLAOption getFirstOfGroup(const std::string& group) const;

		CLAOption operator|(const CLAOption& o) const {
			return CLAOption(*this, o);
		}

		CLAOption& operator=(const CLAOption& other);

		bool operator!() const {
			return name == "";
		}

		operator bool() const {
			return name != "";
		}

		bool operator==(const CLAOption& other) const;

		bool operator!=(const CLAOption& other) const;

		bool operator==(const std::string& name) const {
			return this->name == name;
		}

		bool operator!=(const std::string& name) const {
			return this->name != name;
		}

		bool operator==(const char* name) const {
			return this->name == name;
		}

		bool operator!=(const char* name) const {
			return this->name != name;
		}

		friend std::ostream& operator<<(std::ostream& out, const CLAOption& o) {
			if (o.isEmpty()) {
				out << "CLAOption[EMPTY]";
			} else {
				out << "CLAOption[name=" << o.name << ", group=" << o.group << "]";
			}
			if (o.next) {
				out << ", " << *o.next;
			}

			return out;
		}
	};

//std::ostream& operator<<(std::ostream& out, const CLAOption& o);
}

#endif	/* REVCLAOPTION_H */

