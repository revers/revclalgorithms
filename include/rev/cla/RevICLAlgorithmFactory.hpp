/* 
 * File:   RevICLAlgorithmFactory.hpp
 * Author: Revers
 *
 * Created on 19 sierpień 2012, 20:54
 */

#ifndef REVICLALGORITHMFACTORY_HPP
#define	REVICLALGORITHMFACTORY_HPP

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/RevICLAlgorithm.h>
#include <rev/cla/RevCLAOption.h>
#include <memory>

namespace rev {

    template<typename T>
    class ICLAlgorithmFactory {
    public:
        typedef T algorithm_t;
        typedef std::shared_ptr< algorithm_t > algorithm_t_ptr;
        
        virtual const char* getSPI() = 0;
        virtual bool areOptionsSupported(CLAOption o) = 0;
        
        virtual algorithm_t_ptr create() = 0;
        virtual algorithm_t_ptr create(CLAOption options) = 0;
        
        virtual ~ICLAlgorithmFactory() {}
    };
}
#endif	/* REVICLALGORITHMFACTORY_HPP */

