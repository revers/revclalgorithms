/* 
 * File:   RevCLAlgorithmsConfig.h
 * Author: Revers
 *
 * Created on 14 sierpień 2012, 19:54
 */

#ifndef REVCLALGORITHMSCONFIG_H
#define	REVCLALGORITHMSCONFIG_H

#include <rev/cla/RevCLAConfig.h>
#include <string>
#include <map>
#include <list>

namespace rev {

    class REVCLA_API CLAlgorithmsConfig {
        typedef std::map< std::string, std::string > SpiMap;
        SpiMap spiMap;

        CLAlgorithmsConfig() {
        }
        CLAlgorithmsConfig(const CLAlgorithmsConfig& orig);

    public:

        static CLAlgorithmsConfig& getInstance();

        bool load(const char* filename);

        /**
         * @param algorithms
         * @return NULL if there is no default spi for the algorithm.
         */
        const char* getSPIfor(const char* algorithm);
    };
}

#endif	/* REVCLALGORITHMSCONFIG_H */

