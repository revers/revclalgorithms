/*
 * RevICLAlgorithmAbstractFactory.hpp
 *
 *  Created on: 07-09-2012
 *      Author: Kamil
 */

#ifndef REVICLALGORITHMABSTRACTFACTORY_HPP_
#define REVICLALGORITHMABSTRACTFACTORY_HPP_

#include <vector>
#include <memory>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/RevCLAOption.h>
#include <rev/cla/RevICLAlgorithmFactory.hpp>
#include <rev/cla/RevCLAlgorithmsConfig.h>

namespace rev {

	template<typename T>
	class ICLAlgorithmAbstractFactory {
	public:
		typedef T AlgorithmType;
		typedef std::shared_ptr<AlgorithmType> CLAlgorithmPtr;

		typedef rev::ICLAlgorithmFactory<AlgorithmType> CLAlgorithmFactory;
		typedef std::shared_ptr<CLAlgorithmFactory> CLAlgorithmFactoryPtr;
		typedef std::vector<CLAlgorithmFactoryPtr> CLAlgorithmFactoryVector;
		typedef std::shared_ptr<CLAlgorithmFactoryVector> CLAlgorithmFactoryVectorPtr;

	private:
		ICLAlgorithmAbstractFactory(const ICLAlgorithmAbstractFactory& orig);
	protected:
		const char* algorithmName;
		const char* defaultSpi;

		CLAlgorithmFactoryVector algorithmFactoryVec;
		CLAlgorithmFactoryPtr noclFactoryPtr;

		ICLAlgorithmAbstractFactory(const char* algorithmName,
				const char* defaultSpi) :
				algorithmName(algorithmName), defaultSpi(defaultSpi) {
		}

		virtual ~ICLAlgorithmAbstractFactory() {
		}

		void addCLAlgorithmFactory(CLAlgorithmFactoryPtr factoryPtr) {
			algorithmFactoryVec.push_back(factoryPtr);
		}

		void setNoCLFactory(CLAlgorithmFactoryPtr noclFactoryPtr) {
			this->noclFactoryPtr = noclFactoryPtr;
		}
	public:
		CLAlgorithmPtr createDefault() {
		    CLAlgorithmsConfig& config = CLAlgorithmsConfig::getInstance();
		    const char* spi = config.getSPIfor(algorithmName);

		    if (spi) {
		        return create(spi);
		    }

		    return create(defaultSpi);
		}

		CLAlgorithmPtr createDefault(CLAOption options) {

		    CLAlgorithmsConfig& config = CLAlgorithmsConfig::getInstance();
		    const char* spi = config.getSPIfor(algorithmName);

		    if (spi) {
		        return create(spi, options);
		    }

		    return create(defaultSpi, options);
		}

		CLAlgorithmPtr create(const char* spi) {

		    std::string spiStr(spi);

		    if (spiStr == REVCLA_SPI_NO_CL) {
		        return createNoCL();
		    }

		    for (CLAlgorithmFactoryPtr factoryPtr : algorithmFactoryVec) {
		        if (spiStr == factoryPtr->getSPI()) {
		            return factoryPtr->create();
		        }
		    }

		    return nullptr;
		}

		CLAlgorithmPtr create(const char* spi, CLAOption options) {

		    std::string spiStr(spi);
		    if (spiStr == REVCLA_SPI_NO_CL) {
		        return createNoCL(options);
		    }

		    for (CLAlgorithmFactoryPtr factoryPtr : algorithmFactoryVec) {
		        if (spiStr == factoryPtr->getSPI()) {
		            if (factoryPtr->areOptionsSupported(options)) {
		                return factoryPtr->create(options);
		            } else {
		                return CLAlgorithmPtr();
		            }
		        }
		    }

		    return nullptr;
		}

		CLAlgorithmPtr create(CLAOption options, bool includeNoCL = false) {

			CLAlgorithmPtr ptr = createDefault(options);
			if(ptr) {
				return ptr;
			}

			 for (CLAlgorithmFactoryPtr factoryPtr : algorithmFactoryVec) {
				if (factoryPtr->areOptionsSupported(options)) {
					return factoryPtr->create(options);
				}
		    }

			if(includeNoCL) {
				return createNoCL(options);
			}

			return nullptr;
		}

		CLAlgorithmPtr createNoCL() {
			return noclFactoryPtr->create();
		}

		CLAlgorithmPtr createNoCL(CLAOption options) {
			return noclFactoryPtr->create(options);
		}

		CLAlgorithmFactoryPtr getFactoryFor(const char* spi) {

		    std::string spiStr(spi);

		    if (spiStr == REVCLA_SPI_NO_CL) {
		        return noclFactoryPtr;
		    }

		    for (CLAlgorithmFactoryPtr factoryPtr : algorithmFactoryVec) {
		        if (spiStr == factoryPtr->getSPI()) {
		            return factoryPtr;
		        }
		    }

		    return nullptr;
		}

		const CLAlgorithmFactoryVector& getAllFactories() {
			return algorithmFactoryVec;
		}

		CLAlgorithmFactoryVectorPtr getAllFactoriesFor(CLAOption options) {
		    CLAlgorithmFactoryVectorPtr vec(new CLAlgorithmFactoryVector());

		    for (CLAlgorithmFactoryPtr factoryPtr : algorithmFactoryVec) {
		        if (factoryPtr->areOptionsSupported(options)) {
		            vec->push_back(factoryPtr);
		        }
		    }

		    return vec;
		}
	};
}

#endif /* REVICLALGORITHMABSTRACTFACTORY_HPP_ */
