/*
 * RevCLFFT2DInPlaceAbstractFactory.h
 *
 *  Created on: 25-09-2012
 *      Author: Revers
 */

#ifndef REVCLFFT2DINPLACEABSTRACTFACTORY_H_
#define REVCLFFT2DINPLACEABSTRACTFACTORY_H_


#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/api/RevICLFFT2DInPlace.h>
#include <rev/cla/RevICLAlgorithmAbstractFactory.hpp>
#include <rev/cla/RevICLAlgorithmFactory.hpp>
#include <rev/cla/RevCLAOption.h>

namespace rev {
    typedef rev::ICLAlgorithmFactory<rev::ICLFFT2DInPlace> CLFFT2DInPlaceFactory;
    typedef std::shared_ptr<CLFFT2DInPlaceFactory> CLFFT2DInPlaceFactoryPtr;
    typedef std::vector<CLFFT2DInPlaceFactoryPtr> CLFFT2DInPlaceFactoryVector;
    typedef std::shared_ptr<CLFFT2DInPlaceFactoryVector> CLFFT2DInPlaceFactoryVectorPtr;
    typedef std::shared_ptr<rev::ICLFFT2DInPlace> CLFFT2DInPlacePtr;

    class REVCLA_API CLFFT2DInPlaceAbstractFactory: public ICLAlgorithmAbstractFactory<
            rev::ICLFFT2DInPlace> {

        CLFFT2DInPlaceAbstractFactory();

        CLFFT2DInPlaceAbstractFactory(const CLFFT2DInPlaceAbstractFactory& orig);

        ~CLFFT2DInPlaceAbstractFactory() {
        }

    public:

        static CLFFT2DInPlaceAbstractFactory& getInstance();
    };
}

#endif /* REVCLFFT2DINPLACEABSTRACTFACTORY_H_ */
