/*
 * RevCLFFTInPlaceAbstractFactory.h
 *
 *  Created on: 08-09-2012
 *      Author: Revers
 */

#ifndef REVCLFFTINPLACEABSTRACTFACTORY_H_
#define REVCLFFTINPLACEABSTRACTFACTORY_H_

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/api/RevICLFFTInPlace.h>
#include <rev/cla/RevICLAlgorithmAbstractFactory.hpp>
#include <rev/cla/RevICLAlgorithmFactory.hpp>
#include <rev/cla/RevCLAOption.h>

namespace rev {
	typedef rev::ICLAlgorithmFactory<rev::ICLFFTInPlace> CLFFTInPlaceFactory;
	typedef std::shared_ptr<CLFFTInPlaceFactory> CLFFTInPlaceFactoryPtr;
	typedef std::vector<CLFFTInPlaceFactoryPtr> CLFFTInPlaceFactoryVector;
	typedef std::shared_ptr<CLFFTInPlaceFactoryVector> CLFFTInPlaceFactoryVectorPtr;
	typedef std::shared_ptr<rev::ICLFFTInPlace> CLFFTInPlacePtr;

	class REVCLA_API CLFFTInPlaceAbstractFactory: public ICLAlgorithmAbstractFactory<
			rev::ICLFFTInPlace> {

		CLFFTInPlaceAbstractFactory();

		CLFFTInPlaceAbstractFactory(const CLFFTInPlaceAbstractFactory& orig);

		~CLFFTInPlaceAbstractFactory() {
		}

	public:

		static CLFFTInPlaceAbstractFactory& getInstance();
	};
}

#endif /* REVCLFFTINPLACEABSTRACTFACTORY_H_ */
