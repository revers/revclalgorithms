/*
 * RevICLFFT2DInPlace.h
 *
 *  Created on: 25-09-2012
 *      Author: Revers
 */

#ifndef REVICLFFT2DINPLACE_H_
#define REVICLFFT2DINPLACE_H_

#include <rev/common/RevProfiler.h>

#include <rev/cl/RevCLBuffer.h>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/RevICLAlgorithm.h>
#include <rev/cla/RevCLAOption.h>

#define REVCLA_FFT2D_IN_PLACE_DEFAULT_SPI REVCLA_SPI_APPLE
#define REVCLA_FFT2D_IN_PLACE "FFT2DInPlace"

namespace rev {

    class REVCLA_API ICLFFT2DInPlace: public ICLAlgorithm {
    protected:
        int width = 0;
        int height = 0;
        int bufferOffset = 0;
    public:

        ICLFFT2DInPlace() {
        }

        ICLFFT2DInPlace(CLAOption enabledOptions_) :
                ICLAlgorithm(enabledOptions_) {
        }

        virtual bool init(int width, int height) {
            this->width = width;
            this->height = height;
            return true;
        }

        /**
         * @Override
         */
        const char* getName() {
            return REVCLA_FFT2D_IN_PLACE;
        }

        /**
         * Override
         */
        virtual CLAOption getDefaultOptions() {
            return ICLAlgorithm::OPTION_TYPE_FLOAT
                    | ICLAlgorithm::OPTION_ARR_INTERLEAVED
                    | ICLAlgorithm::OPTION_SCALED_TRUE;
        }

        /**
         * size must be power of 2.
         */
        virtual void fft(rev::CLBuffer& inOutBuffer, bool inverse) = 0;


        virtual void fft(rev::CLBuffer& inOutBuffer, bool inverse,
                rev::Profiler& profiler) {
            profiler.start();
            fft(inOutBuffer, inverse);
            profiler.stop();
        }

        /**
         * Not inverse.
         */
        inline void fft(rev::CLBuffer& inOutBuffer, rev::Profiler& profiler) {
            fft(inOutBuffer, false, profiler);
        }

        /**
         * Not inverse.
         */
        inline void fft(rev::CLBuffer& inOutBuffer) {
            fft(inOutBuffer, false);
        }

        int getWidth() const {
            return width;
        }

        void setWidth(int width = 0) {
            this->width = width;
        }

        int getBufferOffset() const {
            return bufferOffset;
        }

        void setBufferOffset(int bufferOffset) {
            this->bufferOffset = bufferOffset;
        }
    };
} // end namespace rev

#endif /* REVICLFFT2DINPLACE_H_ */
