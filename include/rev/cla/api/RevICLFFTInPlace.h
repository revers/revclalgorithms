/*
 * RevICLICLFFTInPlace.h
 *
 *  Created on: 08-09-2012
 *      Author: Revers
 */

#ifndef REVICLFFTINPLACE_H_
#define REVICLFFTINPLACE_H_

#include <rev/common/RevProfiler.h>
#include <rev/cl/RevCLBuffer.h>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/RevICLAlgorithm.h>
#include <rev/cla/RevCLAOption.h>

#define REVCLA_FFT_IN_PLACE_DEFAULT_SPI REVCLA_SPI_APPLE
#define REVCLA_FFT_IN_PLACE "FFTInPlace"

namespace rev {

    class REVCLA_API ICLFFTInPlace: public ICLAlgorithm {
    protected:
        int width = 0;
        int bufferOffset = 0;
    public:

        ICLFFTInPlace() {
        }

        ICLFFTInPlace(CLAOption enabledOptions_) :
                ICLAlgorithm(enabledOptions_) {
        }

        virtual bool init(int width) {
            this->width = width;
            return true;
        }

        /**
         * @Override
         */
        const char* getName() {
            return REVCLA_FFT_IN_PLACE;
        }

        /**
         * Override
         */
        virtual CLAOption getDefaultOptions() {
            return ICLAlgorithm::OPTION_TYPE_FLOAT
                    | ICLAlgorithm::OPTION_ARR_INTERLEAVED
                    | ICLAlgorithm::OPTION_SCALED_TRUE;
        }

        /**
         * size must be power of 2.
         */
        virtual void fft(rev::CLBuffer& inOutBuffer, bool inverse) = 0;


        virtual void fft(rev::CLBuffer& inOutBuffer, bool inverse,
                rev::Profiler& profiler) {
            profiler.start();
            fft(inOutBuffer, inverse);
            profiler.stop();
        }

        /**
         * Not inverse.
         */
        inline void fft(rev::CLBuffer& inOutBuffer, rev::Profiler& profiler) {
            fft(inOutBuffer, false, profiler);
        }

        /**
         * Not inverse.
         */
        inline void fft(rev::CLBuffer& inOutBuffer) {
            fft(inOutBuffer, false);
        }

        int getWidth() const {
            return width;
        }

        void setWidth(int width = 0) {
            this->width = width;
        }

        int getBufferOffset() const {
            return bufferOffset;
        }

        void setBufferOffset(int bufferOffset) {
            this->bufferOffset = bufferOffset;
        }
    };
} // end namespace rev

#endif /* REVICLFFTINPLACE_H_ */
