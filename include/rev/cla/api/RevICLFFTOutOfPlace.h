/*
 * RevICLFFTOutOfPlace.h
 *
 *  Created on: 08-09-2012
 *      Author: Revers
 */

#ifndef REVICLFFTOUTOFPLACE_H_
#define REVICLFFTOUTOFPLACE_H_

#include <rev/common/RevProfiler.h>
#include <rev/cl/RevCLBuffer.h>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/RevICLAlgorithm.h>
#include <rev/cla/RevCLAOption.h>

#define REVCLA_FFT_OUT_OF_PLACE_DEFAULT_SPI REVCLA_SPI_APPLE
#define REVCLA_FFT_OUT_OF_PLACE "FFTOutOfPlace"

namespace rev {

	class REVCLA_API ICLFFTOutOfPlace: public ICLAlgorithm {
	protected:
		int width = 0;
	public:

		ICLFFTOutOfPlace() {
		}

		ICLFFTOutOfPlace(CLAOption enabledOptions_) :
				ICLAlgorithm(enabledOptions_) {
		}

		virtual bool init(int width) {
			this->width = width;
			return true;
		}

		/**
		 * @Override
		 */
		const char* getName() {
			return REVCLA_FFT_OUT_OF_PLACE;
		}

		/**
		 * Override
		 */
		virtual CLAOption getDefaultOptions() {
			return ICLAlgorithm::OPTION_TYPE_FLOAT
					| ICLAlgorithm::OPTION_ARR_INTERLEAVED
					| ICLAlgorithm::OPTION_SCALED_TRUE;
		}

		/**
		 * size must be power of 2.
		 */
		virtual void fft(rev::CLBuffer& inOutBuffer, rev::CLBuffer& outBuffer,
				bool inverse) = 0;

		virtual void fft(rev::CLBuffer& inOutBuffer, rev::CLBuffer& outBuffer,
				bool inverse, rev::Profiler& profiler) {
			profiler.start();
			fft(inOutBuffer, outBuffer, inverse);
			profiler.stop();
		}

		/**
		 * Not inverse.
		 */
		inline void fft(rev::CLBuffer& inOutBuffer, rev::CLBuffer& outBuffer,
				rev::Profiler& profiler) {
			fft(inOutBuffer, outBuffer, false, profiler);
		}

		/**
		 * Not inverse.
		 */
		inline void fft(rev::CLBuffer& inOutBuffer, rev::CLBuffer& outBuffer) {
			fft(inOutBuffer, outBuffer, false);
		}

		int getWidth() const {
			return width;
		}

		void setWidth(int width = 0) {
			this->width = width;
		}
	};
} // end namespace rev

#endif /* REVICLFFTOUTOFPLACE_H_ */
