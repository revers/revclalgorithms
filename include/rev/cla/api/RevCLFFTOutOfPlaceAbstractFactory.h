/*
 * RevCLFFTOutOfPlaceAbstractFactory.h
 *
 *  Created on: 08-09-2012
 *      Author: Revers
 */

#ifndef REVCLFFTOUTOFPLACEABSTRACTFACTORY_H_
#define REVCLFFTOUTOFPLACEABSTRACTFACTORY_H_

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/api/RevICLFFTOutOfPlace.h>
#include <rev/cla/RevICLAlgorithmAbstractFactory.hpp>
#include <rev/cla/RevICLAlgorithmFactory.hpp>
#include <rev/cla/RevCLAOption.h>

namespace rev {
	typedef rev::ICLAlgorithmFactory<rev::ICLFFTOutOfPlace> CLFFTOutOfPlaceFactory;
	typedef std::shared_ptr<CLFFTOutOfPlaceFactory> CLFFTOutOfPlaceFactoryPtr;
	typedef std::vector<CLFFTOutOfPlaceFactoryPtr> CLFFTOutOfPlaceFactoryVector;
	typedef std::shared_ptr<CLFFTOutOfPlaceFactoryVector> CLFFTOutOfPlaceFactoryVectorPtr;
	typedef std::shared_ptr<rev::ICLFFTOutOfPlace> CLFFTOutOfPlacePtr;

	class REVCLA_API CLFFTOutOfPlaceAbstractFactory: public ICLAlgorithmAbstractFactory<
			rev::ICLFFTOutOfPlace> {

		CLFFTOutOfPlaceAbstractFactory();

		CLFFTOutOfPlaceAbstractFactory(const CLFFTOutOfPlaceAbstractFactory& orig);

		~CLFFTOutOfPlaceAbstractFactory() {
		}

	public:
		static CLFFTOutOfPlaceAbstractFactory& getInstance();
	};
}

#endif /* REVCLFFTOUTOFPLACEABSTRACTFACTORY_H_ */
