/* 
 * File:   RevICLTranspose.h
 * Author: Revers
 *
 * Created on 19 sierpień 2012, 22:07
 */

#ifndef REVICLTRANSPOSE_H
#define	REVICLTRANSPOSE_H

#include <rev/common/RevProfiler.h>
#include <rev/cl/RevCLBuffer.h>

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/RevICLAlgorithm.h>
#include <rev/cla/RevCLAOption.h>

#define REVCLA_TRANSPOSE_DEFAULT_SPI REVCLA_SPI_OTHER
#define REVCLA_TRANSPOSE "Transpose"

#define REVCLA_TRANSPOSE_LOCAL_SIZE_X 16
#define REVCLA_TRANSPOSE_LOCAL_SIZE_Y 16

namespace rev {

	class REVCLA_API ICLTranspose: public ICLAlgorithm {
	public:

		ICLTranspose() {
		}

		ICLTranspose(CLAOption enabledOptions_) :
				ICLAlgorithm(enabledOptions_) {
		}

		virtual bool init() {
			return true;
		}

		/**
		 * @Override
		 */
		const char* getName() {
			return REVCLA_TRANSPOSE;
		}

		/**
		 * Override
		 */
		virtual CLAOption getDefaultOptions() {
			return ICLAlgorithm::OPTION_TYPE_FLOAT | ICLAlgorithm::OPTION_VECTOR_1;
		}

		virtual void transpose(rev::CLBuffer& src, rev::CLBuffer& dest,
				int width, int height, rev::Profiler& profiler) {

			transpose(src, dest, width, height, REVCLA_TRANSPOSE_LOCAL_SIZE_X,
					REVCLA_TRANSPOSE_LOCAL_SIZE_Y, profiler);
		}

		virtual void transpose(rev::CLBuffer& src, rev::CLBuffer& dest,
				int width, int height, int localWidth, int localHeight,
				rev::Profiler& profiler) {

			profiler.start();
			transpose(src, dest, width, height, localWidth, localHeight);
			profiler.stop();
		}

		inline void transpose(rev::CLBuffer& src, rev::CLBuffer& dest, int width,
				int height) {
			transpose(src, dest, width, height, REVCLA_TRANSPOSE_LOCAL_SIZE_X,
					REVCLA_TRANSPOSE_LOCAL_SIZE_Y);
		}

		virtual void transpose(rev::CLBuffer& src, rev::CLBuffer& dest,
				int width, int height, int localWidth,
				int localHeight) = 0;
	};
}

#endif	/* REVICLTRANSPOSE_H */

