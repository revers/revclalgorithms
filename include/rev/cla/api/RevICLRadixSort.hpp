/* 
 * File:   RevICLRadixSort.hpp
 * Author: Revers
 *
 * Created on 19 sierpień 2012, 21:32
 */

#ifndef REVICLRADIXSORT_HPP
#define	REVICLRADIXSORT_HPP

#include <rev/RevCLAConfig.h>
#include <rev/RevICLAlgorithm.hpp>

namespace rev {

    class REVCLA_API ICLRadixSort : public ICLAlgorithm {
    public:
        
        /**
         * @Override
         */
        const char* getName() {
            return "RadixSort";
        }
    };
}

#endif	/* REVICLRADIXSORT_HPP */

