/* 
 * File:   RevCLTransposeAbstractFactory.h
 * Author: Revers
 *
 * Created on 19 sierpień 2012, 21:21
 */
#ifndef REVCLTRANSPOSEABSTRACTFACTORY_H
#define REVCLTRANSPOSEABSTRACTFACTORY_H

#include <rev/cla/RevCLAConfig.h>
#include <rev/cla/api/RevICLTranspose.h>
#include <rev/cla/RevICLAlgorithmAbstractFactory.hpp>
#include <rev/cla/RevICLAlgorithmFactory.hpp>
#include <rev/cla/RevCLAOption.h>

namespace rev {
    typedef rev::ICLAlgorithmFactory< rev::ICLTranspose > CLTransposeFactory;
    typedef std::shared_ptr< CLTransposeFactory > CLTransposeFactoryPtr;
    typedef std::vector< CLTransposeFactoryPtr > CLTransposeFactoryVector;
    typedef std::shared_ptr< CLTransposeFactoryVector > CLTransposeFactoryVectorPtr;
    typedef std::shared_ptr< rev::ICLTranspose > CLTransposePtr;

    class REVCLA_API CLTransposeAbstractFactory : public ICLAlgorithmAbstractFactory<rev::ICLTranspose> {

        CLTransposeAbstractFactory();

        CLTransposeAbstractFactory(const CLTransposeAbstractFactory& orig);

        ~CLTransposeAbstractFactory() {
        }

    public:

        static CLTransposeAbstractFactory& getInstance();
    };
}
#endif	/* REVCLTRANSPOSEABSTRACTFACTORY_H */

