#ifndef REVCLA_CONFIG_H
#define	REVCLA_CONFIG_H

#define REVCLA_VERSION_MAJOR 0
#define REVCLA_VERSION_MINOR 1 

#ifndef REVCLA_STATIC
/* #undef REVCLA_STATIC */
#endif

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(_WIN64)
#define REVCLA_WINDOWS
#endif

#if defined(REVCLA_WINDOWS)
#   define REVCLA_CDECL_CALL    __cdecl
#   define REVCLA_STD_CALL      __stdcall
#   define REVCLA_CALL          REVCLA_CDECL_CALL
#   define REVCLA_EXPORT_API    __declspec(dllexport)
#   define REVCLA_IMPORT_API    __declspec(dllimport)
#else
#   define REVCLA_CDECL_CALL
#   define REVCLA_STD_CALL
#   define REVCLA_CALL
#   define REVCLA_EXPORT_API
#   define REVCLA_IMPORT_API
#endif

#if defined REVCLA_EXPORTS
#   define REVCLA_API REVCLA_EXPORT_API
#elif defined REVCLA_STATIC
#   define REVCLA_API
#   if defined(_MSC_VER) && !defined(REVCLA_NO_LIB_PRAGMA)
#       ifdef _WIN64
#           pragma comment(lib, "RevCLAlgorithmsStatic64")
#       else
#           pragma comment(lib, "RevCLAlgorithmsStatic")
#       endif
#   endif
#else
#   define REVCLA_API REVCLA_IMPORT_API
#   if defined(_MSC_VER) && !defined(REVCLA_NO_LIB_PRAGMA)
#       ifdef _WIN64
#           pragma comment(lib, "RevCLAlgorithms64")
#       else
#           pragma comment(lib, "RevCLAlgorithms")
#       endif
#   endif
#endif

#define REVCLA_SPI_NVIDIA "nvidia"
#define REVCLA_SPI_ATI "ati"
#define REVCLA_SPI_APPLE "apple"
#define REVCLA_SPI_NO_CL "nocl"
#define REVCLA_SPI_OTHER "other"

#ifdef NDEBUG
#define REVCLA_TRACE(x) ((void)0)
#define REVCLA_ERR_MSG(x) REV_ERROR_MSG(x)

#else

#include <iostream>
#define REVCLA_TRACE(x) std::cout << x << std::endl
#include <rev/common/RevAssert.h>
#define REVCLA_ERR_MSG(x) assertMsg(false, x)
#endif

#define REVCLA_USE_FAST_MATH

#endif /* REVCLA_CONFIG_H */
